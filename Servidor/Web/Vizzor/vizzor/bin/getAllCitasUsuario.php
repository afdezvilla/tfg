<?php

require_once 'funciones_bd.php';
$db = new funciones_BD();

$usuario = $_POST['usuario'];

$cita = $db->getAllCitasUsuario($usuario);
if ($cita) {
    $response["error"] = FALSE;
    $response["cita"] = $cita ;
    echo json_encode($response);

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "JSON Error occured in Registartion";
    echo json_encode($response);
}
?>