En la presente carpeta, Servidor, se encuentran aquellos ficheros y recursos que han
formado parte del desarrollo del proyecto presente en el servidor.

	API REST: Contiene la API REST realizada para establecer la comunicación entre
la aplicación móvil y la página web con el servidor remoto.

	Web: Contiene la página web y todos los archivos necesarios para su correcto 
funcionamiento.
