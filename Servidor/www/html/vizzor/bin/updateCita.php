<?php

require_once 'funciones_bd.php';
$db = new funciones_BD();

$usuario = $_POST['usuario'];
$especie = $_POST['especie'];
$latitud = $_POST['latitud'];
$longitud = $_POST['longitud'];
$paraje = $_POST['paraje'];
$municipio = $_POST['municipio'];
$numero = $_POST['numero'];
$edad = $_POST['edad'];
$sexo = $_POST['sexo'];
$interes = $_POST['interes'];
$observaciones = $_POST['observaciones'];
$privacidad = $_POST['privacidad'];
$filtro = $_POST['filtro'];
$fecha = $_POST['fecha'];
$imagen = $_POST['imagen'];
$token = $_POST['token'];
$tokenUsuario = $_POST['tokenUsuario'];


$cita = $db->updateCita($usuario, $especie, $latitud, $longitud, $paraje, $municipio, $numero, $edad, $sexo, $interes, $observaciones, $privacidad, $filtro, $fecha, $imagen, $token, $tokenUsuario);
if ($cita) {
    $response["error"] = FALSE;
    $response["cita"] = $cita ;
    echo json_encode($response);

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "JSON Error occured in Registartion";
    echo json_encode($response);
}




?>