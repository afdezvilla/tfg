<?php
 
class funciones_BD {
 
    private $conn;

    // constructor
    function __construct() {
        require_once 'connectbd.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {
        
    }
 
    public function insertCita($usuario, $especie, $latitud, $longitud, $paraje, $municipio, $numero, $edad, $sexo, $interes, $observaciones, $privacidad, $filtro, $fecha, $imagen, $token, $tokenUsuario) {
        $stmt = $this->conn->prepare("SELECT * FROM autenticacion WHERE token = :tokenUsuario");
        $stmt->bindParam(':tokenUsuario', $tokenUsuario);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $stmt = $this->conn->prepare("SELECT * FROM citas WHERE token = :token");
            $stmt->bindParam(':token', $token);
            $stmt->execute();
            if(!$stmt->rowCount()){
                $uuid = uniqid('', true);

                $stmt = $this->conn->prepare("INSERT INTO citas(unique_id, usuario, especie, latitud, longitud, paraje, municipio, numero, edad, sexo, interes, observaciones, privacidad, filtro, fecha, imagen,token, created_at) 
                    VALUES(:uuid,:usuario,:especie,:latitud,:longitud,:paraje,:municipio,:numero,:edad,:sexo,:interes,:observaciones,:privacidad,:filtro,:fecha,:imagen,:token,NOW())");

                $stmt->bindParam(':uuid', $uuid);
                $stmt->bindParam(':usuario', $usuario);
                $stmt->bindParam(':especie', $especie);
                $stmt->bindParam(':latitud', $latitud);
                $stmt->bindParam(':longitud', $longitud);
                $stmt->bindParam(':paraje', $paraje);
                $stmt->bindParam(':municipio', $municipio);
                $stmt->bindParam(':numero', $numero);
                $stmt->bindParam(':edad', $edad);
                $stmt->bindParam(':sexo', $sexo);
                $stmt->bindParam(':interes', $interes);
                $stmt->bindParam(':observaciones', $observaciones);
                $stmt->bindParam(':privacidad', $privacidad);
                $stmt->bindParam(':filtro', $filtro);
                $stmt->bindParam(':fecha', $fecha);
                $stmt->bindParam(':imagen', $imagen);
                $stmt->bindParam(':token', $token);

                $stmt->execute();
                if ($stmt) {
                    return true;
                } else {
                    return false;
                }
            } else {
                 return false;
            }
        } elseif(!$stmt->rowCount()){
            return false;
        }


          
    }

    public function updateCita($usuario, $especie, $latitud, $longitud, $paraje, $municipio, $numero, $edad, $sexo, $interes, $observaciones, $privacidad, $filtro, $fecha, $imagen, $token, $tokenUsuario) {
        $stmt = $this->conn->prepare("SELECT * FROM autenticacion WHERE token = :tokenUsuario");
        $stmt->bindParam(':tokenUsuario', $tokenUsuario);
        $stmt->execute();
        if ($stmt->rowCount()) {

            $stmt = $this->conn->prepare("UPDATE citas SET usuario=:usuario, especie=:especie, latitud=:latitud, longitud=:longitud, paraje=:paraje, municipio=:municipio, numero=:numero, edad=:edad, sexo=:sexo, interes=:interes, observaciones=:observaciones, privacidad=:privacidad, filtro=:filtro, fecha=:fecha, imagen=:imagen, updated_at=NOW() WHERE token = :token");

            $stmt->bindParam(':usuario', $usuario);
            $stmt->bindParam(':especie', $especie);
            $stmt->bindParam(':latitud', $latitud);
            $stmt->bindParam(':longitud', $longitud);
            $stmt->bindParam(':paraje', $paraje);
            $stmt->bindParam(':municipio', $municipio);
            $stmt->bindParam(':numero', $numero);
            $stmt->bindParam(':edad', $edad);
            $stmt->bindParam(':sexo', $sexo);
            $stmt->bindParam(':interes', $interes);
            $stmt->bindParam(':observaciones', $observaciones);
            $stmt->bindParam(':privacidad', $privacidad);
            $stmt->bindParam(':filtro', $filtro);
            $stmt->bindParam(':fecha', $fecha);
            $stmt->bindParam(':imagen', $imagen);
            $stmt->bindParam(':token', $token);

            $stmt->execute();
            if ($stmt) {
                return true;
            } else {
                return false;
            }
        } elseif(!$stmt->rowCount()){
            return false;
        }
    }

    public function deleteCita($token, $tokenUsuario) {
        $stmt = $this->conn->prepare("SELECT * FROM autenticacion WHERE token = :tokenUsuario");
        $stmt->bindParam(':tokenUsuario', $tokenUsuario);
        $stmt->execute();
        if ($stmt->rowCount()) {

            $stmt = $this->conn->prepare("DELETE FROM citas WHERE token = :token");
            $stmt->bindParam(':token', $token);

            $stmt->execute();
            if ($stmt) {
                return true;
            } else {
                return false;
            }
        } elseif(!$stmt->rowCount()){
            return false;
        }
    }

    public function getAllCitas(){
        $stmt = $this->conn->prepare("SELECT * FROM citas WHERE privacidad ='Pública' ORDER BY uid DESC");
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row_all = $stmt->fetchall(PDO::FETCH_ASSOC);
            return $row_all;
        } elseif(!$stmt->rowCount()){
          return false;
        }
    }

    public function getAllCitasUsuario($usuario){
        $stmt = $this->conn->prepare("SELECT * FROM citas WHERE usuario = :usuario ORDER BY uid DESC");
        $stmt->bindParam(':usuario', $usuario);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row_all = $stmt->fetchall(PDO::FETCH_ASSOC);
            return $row_all;
        } elseif(!$stmt->rowCount()){
          return false;
        }
    }

    public function getCita($uid) {
        $stmt = $this->conn->prepare("SELECT * FROM citas WHERE uid = :uid");
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
        } elseif(!$stmt->rowCount()){
          return false;
        }
    }

   public function insertParaje($nombre, $latitud, $longitud, $municipio, $radio, $interes, $imagen){
        $stmt = $this->conn->prepare("INSERT INTO parajes(nombre, latitud, longitud, municipio, radio, interes, imagen) 
            VALUES(:nombre,:latitud,:longitud,:municipio,:radio,:interes,:imagen)");

        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':latitud', $latitud);
        $stmt->bindParam(':longitud', $longitud);
        $stmt->bindParam(':municipio', $municipio);
        $stmt->bindParam(':radio', $radio);
        $stmt->bindParam(':interes', $interes);
        $stmt->bindParam(':imagen', $imagen);

        $stmt->execute();
        if ($stmt) {
            $uid = $this->conn->lastInsertId(); // last inserted id
            $stmt = $this->conn->prepare("SELECT * FROM parajes WHERE uid = :uid");
            $stmt->bindParam(':uid', $uid);
            $stmt->execute();
            if ($stmt->rowCount()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            } elseif(!$stmt->rowCount()){
                return false;
            }
        } else {
            return false;
        }
    }

    public function getAllParajes(){
        $stmt = $this->conn->prepare("SELECT * FROM parajes ORDER BY nombre");
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row_all = $stmt->fetchall(PDO::FETCH_ASSOC);
            return $row_all;
        } elseif(!$stmt->rowCount()){
          return false;
        }
    }

    public function getParaje($uid) {
        $stmt = $this->conn->prepare("SELECT * FROM parajes WHERE uid = :uid");
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
        } elseif(!$stmt->rowCount()){
          return false;
        }
    }

    public function insertAuth($email, $token){
        $stmt = $this->conn->prepare("SELECT * FROM autenticacion WHERE email = :email");
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        if ($stmt->rowCount()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
        } elseif(!$stmt->rowCount()){
            $stmt = $this->conn->prepare("INSERT INTO autenticacion(email, token) 
                VALUES(:email,:token)");

            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':token', $token);

            $stmt->execute();

            if ($stmt) {
                return true;
            } else {
                return false;
            }
        }
    }



}	
 
?>
