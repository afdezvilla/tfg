<?php

require_once 'funciones_bd.php';
$db = new funciones_BD();

$paraje = $db->insertParaje("Aiguas Tortas y Lago de San Mauricio","42.577222","0.947778","Lérida","10000","Este paraje se encuentra al norte de la provincia de Lleida, formado por más de 200 lagos, entre los que se encuentra el lago de Sant Maurici, que está situado aproximadamente a 2000 metros de altitud y de origen glacial. Además de los numerosos lagos se puede encontrar grandes riscos, llamados como Els Encantats, que hacen de éste uno de los paisajes más bonitos de la comarca. ","Aiguestortes.png");

$paraje = $db->insertParaje("Doñana","37.02","-6.44","Huelva, Sevilla, Cádiz","10000","Este paraje es un conjunto de ecosistemas con una biodiversidad única en el planeta. Formado por llanuras, marismas, lagunas, pinares, dunas, playas y acantilados que hacen del lugar un marco para el disfrute de la naturaleza. Destaca la marisma, el cual es un lugar de paso y cría de multitud de aves europeas y africanas. La zona, además, es un refugio de animales en peligro de extinción. ","Donana.png");

$paraje = $db->insertParaje("Teide","28.25","-16.6167","Tenerife","10000","Este paraje está formado por unos relives que han sido resultado de los restos de la lava del volcán, únicos en todo el planeta. A parte del entorno también hay que destacar una flora impresionante. El tajinaste rojo, que puede superar los dos metros y medio de altura, o la violeta del Teide son dos plantas que sólo crecen aquí, la mayoría de ellas, a más de 2.000 metros de altitud.","Teide.png");

$paraje = $db->insertParaje("Caldera de Taburiente","28.753928","-1.7884719","La Palma","10000","Este paraje se encuentra formado zonas boscosas del pino canario, cedros o jaras y por multitud de especies protegidas. El espacio natural mantiene un estado de conservación del medioambiente y de su ecosistema, albergando gran cantidad de fauna, sobre todo de aves.","Caldera.png");

$paraje = $db->insertParaje("Picos de Europa","43.21","-4.84","Asturias, León, Cantabria","10000","Este paraje representa aquellos ecosistemas basados en los bosques atlánticos. Producto de la erosión glaciar con la presencia de numerosos lagos. La fauna de la zona es bastante rica, en el que podemos destacar especies como los corzos, los lobos o el oso pardo.","Picos.png");

$paraje = $db->insertParaje("Ordesa y Monte Perdido","42.671667","0.055556","Huesca","10000","Este paraje se encuentra en el centro de la cordillera pirenaica, formado por grandes contrastes. La extrema aridez de las zonas altas contrasta con los verdes valles cubiertos por bosques y prados. En dicho enclave viven cerca de 1.400 especies vegetales, como hayedos, abedules, pinos y tinos, y entre su fauna destacan especies de aves como el quebrantahuesos, el urogallo o el buitre leonado.","Ordesa.png");

$paraje = $db->insertParaje("Tablas de Daimiel","39.15","-3.666667","Ciudad Real","10000","Este paraje es el mayor representante del ecosistema denominado tablas fluviales. La belleza de este lugar reside en parte en la gran fauna que se cobija entre sus aguas, como las garzas imperiales, las cigüeñelas reales, los patos cuchara o las nutrias. Existen propuestas específicas para la observación de aves con un monitor, totalmente recomendables para quienes quieran iniciarse en el mundo de la ornitología.","Tablas.png");

$paraje = $db->insertParaje("Timanfaya","29.011444","-13.779944","Lanzarote","10000","Este paraje se encuentra marcado por las erupciones. Este parque es el resultado de las erupciones volcánicas que modificaron la fisionomía de esta parte de la isla y que, hasta entonces, se dedicaba únicamente a la agricultura.","Timanfaya.png");

$paraje = $db->insertParaje("Garajonay","28.12625","-17.237222","La Gomera","10000","Este paraje destaca por su singularidad. La niebla procedente del océano cae sobre sus bosques dándoles una constante humedad haciéndose parecer una selva. Los valles donde encontramos estos bosques verdes se mezclan con unos inmensos salientes de roca.","Garajonay.png");

$paraje = $db->insertParaje("Archipiélago de Cabrera","39.158333","2.966667","Islas Baleares","10000","Este paraje se trata del mejor ejemplo de un ecosistema insular no alterado por la mano del hombre. En el parque encontramos importantes colonias de aves marinas, especies endémicas y uno de los mejores fondos marinos del litoral español. También se puede albergar una gran cantidad de acantilados, calas, cuevas y playas de arena blanca que hacen del disfrute de un paisaje sorprendente.","Cabrera.png");

$paraje = $db->insertParaje("Cabañeros","39.4099407","-4.5069323","Ciudad Real, Toledo","10000","Este paraje es uno de los rincones protegidos más valiosos de los Montes de Toledo, rodeado de fauna ibérica amenazada y con fósiles de más de 500 millones de años de antigüedad. Sus sierras y macizos cubiertos de bosque y matorral mediterráneo dan cobijo hoy a gran variedad de aves y mamíferos entre los que destaca el águila imperial ibérica, el buitre negro, el corzo o el jabalí.","Cabaneros.png");

$paraje = $db->insertParaje("Sierra Nevada","37.1","-3.09","Granada, Almería","10000","Este paraje se encuentra formado por una zona boscosa de pinos, encinas y robles. La vegetación cada vez se vuelve más frondosa en este espacio natural. Existen puntos de partida de diferentes rutas de senderismo y también zonas como miradores para ver el paisaje y poder disfrutar de la naturaleza en vivo.","Sierra.png");

$paraje = $db->insertParaje("Islas Atlánticas de Galicia","42.380556","-8.933333","Pontevedra, La Coruña","10000","Este paraje formado por vertiginosos acantilados embellece el paisaje atlántico. Matorrales, dunas, playas y fondos marinos de riqueza excepcional conforman este paraje único custodiado por los cañones de los antiguos buques hundidos en sus aguas. Sin duda alguna, el enclave más bello y conocido de este parque son las Islas Cíes, un oasis de naturaleza.","Galicia.png");

$paraje = $db->insertParaje("Monfragüe","39.82","-5.97","Cáceres","10000","Este paraje representa uno de los enclaves más extensos de bosque y matorral mediterráneo. Se trata de un refugio único para todo tipo de aves, algunas de ellas en peligro de extinción, como el águila imperial ibérica, el buitre leonado o la cigüeña negra.","Monfrague.png");

$paraje = $db->insertParaje("Sierra de Guadarrama","40.783333","-3.983333","Madrid, Segovia","10000","Este paraje es un macizo montañoso con una orografía que impone sus paredes rocosas y cúmulos de matorrales que crecen en las alturas. Pinares, enebrales y sistemas naturales de origen glaciar configuran el paisaje que parece esculpido en granito. El pechiazul y el roquero rojo son dos tipos de aves que comparten el espacio aéreo con algunas de las especies más amenazadas por la extinción del planeta, como la cigüeña negra, el águila imperial o el buitre negro.","Guadarrama.png");


if ($paraje) {
    $response["error"] = FALSE;
    $response["paraje"] = $paraje ;
    echo json_encode($response);

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "JSON Error occured in Registartion";
    echo json_encode($response);
}
?>