package alvaro.vizzor.helper;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import alvaro.vizzor.R;
import alvaro.vizzor.SplashActivity;
import alvaro.vizzor.model.Usuario;
import alvaro.vizzor.sql.DAOLocal;

public class Util {

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

    public static void enableEditText(EditText editText) {
        editText.setFocusable(true);
        editText.setEnabled(true);
        editText.setCursorVisible(true);
    }

    public static void textWatcher(final EditText txt, final Context context){
        txt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if(s.toString() == null || s.length() == 0) {
                    txt.setError(context.getResources().getString(R.string.errorVacio));
                }else{
                    txt.setError(null);
                }
            }
        });
    }

    public static void textWatcherEspecies(final EditText txt, final String[] especies, final Context context){
        txt.addTextChangedListener(new TextWatcher() {

            final android.os.Handler handler = new android.os.Handler();
            Runnable runnable;
            Boolean encontrado;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacks(runnable);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                encontrado = false;
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        for (String temp : especies) {
                            if (s.toString().equals(temp)) {
                                txt.setError(null);
                                encontrado = true;
                                break;
                            //} else {
                                //txt.setError(context.getResources().getString(R.string.errorEspecie));
                            }
                        }
                        if(!encontrado){
                            txt.setError(context.getResources().getString(R.string.errorEspecie));
                        }
                    }
                };
                handler.postDelayed(runnable, 500);
            }
        });
    }

    public static void textWatcherMunicipio(final EditText txt, final String[] municipios, final Context context){
        txt.addTextChangedListener(new TextWatcher() {

            final android.os.Handler handler = new android.os.Handler();
            Runnable runnable;
            Boolean encontrado;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.removeCallbacks(runnable);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                encontrado = false;
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        for (String temp : municipios) {
                            if (s.toString().equals(temp)) {
                                txt.setError(null);
                                encontrado = true;
                                break;
                            //} else {
                               // txt.setError(context.getResources().getString(R.string.errorMunicipio));
                            }
                        }
                        if(!encontrado){
                            txt.setError(context.getResources().getString(R.string.errorMunicipio));
                        }
                    }
                };
                handler.postDelayed(runnable, 500);
            }
        });
    }

    public static void textNoNull(final EditText txt, final Context context){
        if (txt.getText().toString().trim().equalsIgnoreCase("")) {
            txt.setError(context.getResources().getString(R.string.errorVacio));
        }
    }

    public static void textError(final EditText txt, final Context context){
        txt.setError(context.getResources().getString(R.string.errorLocalizacion));
    }

    public static String getMes (Context context, int mes){
        String mesString;

        switch(mes){
            case 0:
            {
                mesString = context.getResources().getString(R.string.enero);
                break;
            }
            case 1:
            {
                mesString = context.getResources().getString(R.string.febrero);
                break;
            }
            case 2:
            {
                mesString = context.getResources().getString(R.string.marzo);
                break;
            }
            case 3:
            {
                mesString = context.getResources().getString(R.string.abril);
                break;
            }
            case 4:
            {
                mesString = context.getResources().getString(R.string.mayo);
                break;
            }
            case 5:
            {
                mesString = context.getResources().getString(R.string.junio);
                break;
            }
            case 6:
            {
                mesString = context.getResources().getString(R.string.julio);
                break;
            }
            case 7:
            {
                mesString = context.getResources().getString(R.string.agosto);
                break;
            }
            case 8:
            {
                mesString = context.getResources().getString(R.string.septiembre);
                break;
            }
            case 9:
            {
                mesString = context.getResources().getString(R.string.octubre);
                break;
            }
            case 10:
            {
                mesString = context.getResources().getString(R.string.noviembre);
                break;
            }
            case 11:
            {
                mesString = context.getResources().getString(R.string.diciembre);
                break;
            }
            default:
            {
                mesString="Error";
                break;
            }
        }
        return mesString;
    }

    public static void sharedUser(Context context, String usuarioEmail ){
        SharedPreferences prefs = context.getSharedPreferences("Vizzor",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("usuarioEmail", usuarioEmail);
        Log.e("Util", "sharedUser= " + usuarioEmail);
        editor.apply();

    }

    public static void checkSingletonUser(Context context){
        SharedPreferences prefs = context.getSharedPreferences("Vizzor",Context.MODE_PRIVATE);
        String usuarioEmail = prefs.getString("usuarioEmail", "null");
        Usuario user = DAOLocal.getUser(usuarioEmail, context);
        Log.e("Util", "checkSingletonUser= " + user.toString());
        Singleton.getInstance().setUsuario(user);
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest)
                hexString.append(Integer.toHexString(0xFF & aMessageDigest));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String convertCoordenadas(double latitude, double longitude) {
        StringBuilder builder = new StringBuilder();

        if (latitude < 0) {
            builder.append("S ");
        } else {
            builder.append("N ");
        }

        String latitudeDegrees = Location.convert(Math.abs(latitude), Location.FORMAT_SECONDS);
        String[] latitudeSplit = latitudeDegrees.split(":");
        builder.append(latitudeSplit[0]);
        builder.append("°");
        builder.append(latitudeSplit[1]);
        builder.append("'");
        builder.append(latitudeSplit[2]);
        builder.append("\"");

        builder.append(" ");

        if (longitude < 0) {
            builder.append("W ");
        } else {
            builder.append("E ");
        }

        String longitudeDegrees = Location.convert(Math.abs(longitude), Location.FORMAT_SECONDS);
        String[] longitudeSplit = longitudeDegrees.split(":");
        builder.append(longitudeSplit[0]);
        builder.append("°");
        builder.append(longitudeSplit[1]);
        builder.append("'");
        builder.append(longitudeSplit[2]);
        builder.append("\"");

        return builder.toString();
    }

    public static void logout(final Context context, final Activity activity){
        GoogleSignInClient mGoogleSignInClient;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);

        mGoogleSignInClient.signOut()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent login = new Intent(context, SplashActivity.class);
                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(login);
                    }
                });

    }
}
