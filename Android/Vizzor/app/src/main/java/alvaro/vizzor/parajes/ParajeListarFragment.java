package alvaro.vizzor.parajes;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import java.util.ArrayList;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.model.Paraje;


public class ParajeListarFragment extends Fragment {

    private SwipeRefreshLayout refreshLayout;
    private ParajeAdaptadorLista adaptador;
    private RecyclerView recView;

    private ArrayList<Paraje> filtro;

    private SearchView search;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_paraje_listar, container, false);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);

        recView = (RecyclerView) view.findViewById(R.id.paraje_listar_recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recView.setLayoutManager(linearLayoutManager);

        filtro = new ArrayList<>();

        adaptador = new ParajeAdaptadorLista(getActivity(), Singleton.getInstance().getParajes());

        recView.setAdapter(adaptador);

        search = (SearchView) view.findViewById(R.id.search);
        search.setIconified(false);
        search.clearFocus();

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshLayout.setRefreshing(false);
                    }
                }
        );

        adaptador = new ParajeAdaptadorLista(getActivity(), Singleton.getInstance().getParajes());
        recView.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();
                filtro.clear();
                for (int i = 0; i < Singleton.getInstance().getParajes().size(); i++) {

                    final String text = Singleton.getInstance().getParajes().get(i).toString().toLowerCase();
                    if (text.contains(query)) {
                        filtro.add(Singleton.getInstance().getParajes().get(i));
                    }
                }

                adaptador = new ParajeAdaptadorLista(getActivity(), filtro);
                recView.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
                return true;
            }
        });

        return view;
    }

}