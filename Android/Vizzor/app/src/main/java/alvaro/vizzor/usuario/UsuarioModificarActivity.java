package alvaro.vizzor.usuario;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.model.Usuario;
import alvaro.vizzor.sql.DAOLocal;

import static alvaro.vizzor.helper.Config.PX_H;
import static alvaro.vizzor.helper.Config.PX_W;
import static alvaro.vizzor.helper.Config.RC_CAMERA;
import static alvaro.vizzor.helper.Config.RC_GALLERY;

public class UsuarioModificarActivity extends AppCompatActivity {
    private Context context;

    private Usuario user;

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;

    private EditText txtNombre;
    private EditText txtDomicilio;
    private EditText txtEmail;
    private EditText txtTelefono;
    private EditText txtIdentificador;
    private Button btnGuardar;

    private TextView txtSeleccionarImagen;
    private ImageView ivImagen;

    private Bitmap imagenFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_modificar);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("usuario_modificar", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Log.e("usuario_modificar", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");


        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(true);

        collapsingToolbar.setTitle("Modificar Usuario");

        user = new Usuario();

        txtNombre =(EditText)findViewById(R.id.txtNombre);
        txtDomicilio =(EditText)findViewById(R.id.txtDomicilio);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtTelefono =(EditText)findViewById(R.id.txtTelefono);
        txtIdentificador = (EditText)findViewById(R.id.txtIdentificador);
        txtSeleccionarImagen = (TextView) findViewById(R.id.txtSeleccionarImagen);
        ivImagen = (ImageView) findViewById(R.id.ivImagen);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);

        txtNombre.setText(Singleton.getInstance().getUsuario().getNombre());
        txtDomicilio.setText(Singleton.getInstance().getUsuario().getDomicilio());
        txtEmail.setText(Singleton.getInstance().getUsuario().getEmail());
        Util.disableEditText(txtEmail);
        txtTelefono.setText(Singleton.getInstance().getUsuario().getTelefono());
        txtIdentificador.setText(Singleton.getInstance().getUsuario().getIdentificador());

        imagenFinal = Singleton.getInstance().getUsuario().getImagen();

        Glide.with(context)
                .load(imagenFinal)
                .apply(new RequestOptions().override(247, 200).fitCenterTransform())
                .into(ivImagen);

        Util.textWatcher(txtNombre, context);

        Util.textNoNull(txtDomicilio, context);
        Util.textWatcher(txtDomicilio, context);

        Util.textNoNull(txtTelefono, context);
        Util.textWatcher(txtTelefono, context);

        Util.textNoNull(txtIdentificador, context);
        Util.textWatcher(txtIdentificador, context);

        txtSeleccionarImagen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final CharSequence[] items = { getString(R.string.hacer_foto), getString(R.string.seleccionar_foto), getString(R.string.cancelar) };
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UsuarioModificarActivity.this);
                builder.setTitle(getString(R.string.anadir_foto));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (items[item].equals(getString(R.string.hacer_foto))) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, RC_CAMERA);

                        } else if (items[item].equals(getString(R.string.seleccionar_foto))) {
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, getString(R.string.seleccionar_fichero)),RC_GALLERY);
                        } else if (items[item].equals(getString(R.string.cancelar))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }

        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (DAOLocal.checkUser(txtEmail.getText().toString().trim(), context)) {
                    if(checkRegistro()) {
                        user.setNombre(txtNombre.getText().toString());
                        user.setDomicilio(txtDomicilio.getText().toString());
                        user.setEmail(txtEmail.getText().toString());
                        user.setTelefono(txtTelefono.getText().toString());
                        user.setIdentificador(txtIdentificador.getText().toString());
                        user.setImagen(imagenFinal);
                        DAOLocal.updateUser(user, context);

                        Singleton.getInstance().setUsuario(user);
                        Intent modificarIntent = new Intent(context, UsuarioDetallesActivity.class);
                        startActivity(modificarIntent);
                        finish();
                    }
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_GALLERY) {
                onSelectFromGalleryResult(data);
            }else if(requestCode == RC_CAMERA){
                onCaptureImageResult(data);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera/",
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;

        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imagenFinal = Util.getResizedBitmap(thumbnail, PX_W, PX_H);
        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .apply(new RequestOptions().override(247, 200).fitCenterTransform())
                .into(ivImagen);
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap thumbnail = null;
        try {
            Uri selectedImageUri = data.getData();
            thumbnail = MediaStore.Images.Media.getBitmap(context.getContentResolver(), selectedImageUri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 50, bytes);
            byte[] byteArrayCompress = bytes.toByteArray();
            thumbnail = BitmapFactory.decodeByteArray(byteArrayCompress,0,byteArrayCompress.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        imagenFinal = Util.getResizedBitmap(thumbnail, PX_W, PX_H);
        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .apply(new RequestOptions().override(247, 200).fitCenterTransform())
                .into(ivImagen);
    }

    private boolean checkRegistro() {
        boolean check = true;

        if (txtNombre.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorNombre), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtDomicilio.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorDomicilio), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtTelefono.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorTelefono), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtIdentificador.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorIdentificador), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        return check;
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();
    }

    @Override
    public void onBackPressed() {
        Intent modificarIntent = new Intent(context, UsuarioDetallesActivity.class);
        startActivity(modificarIntent);
        finish();
    }

}

