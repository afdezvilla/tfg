package alvaro.vizzor.sql;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import alvaro.vizzor.helper.Util;
import alvaro.vizzor.helper.VolleySingleton;
import alvaro.vizzor.model.Cita;
import alvaro.vizzor.model.Usuario;

import static alvaro.vizzor.helper.Config.RC_BROADCAST;
import static alvaro.vizzor.helper.Config.RC_SYNC;
import static alvaro.vizzor.helper.Config.URL_DELETE_CITA;
import static alvaro.vizzor.helper.Config.URL_GET_CITAS_USUARIO;
import static alvaro.vizzor.helper.Config.URL_INSERT_AUTH;
import static alvaro.vizzor.helper.Config.URL_INSERT_CITA;
import static alvaro.vizzor.helper.Config.URL_UPDATE_CITA;

public class DAORemoto {

    public static void insertCita(final int id, final String usuario, final String especie, final String latitud,
                            final String longitud, final String paraje, final String municipio,
                            final String numero, final String edad, final String sexo, final String interes,
                            final String observaciones, final String privacidad, final String filtro,
                            final String fecha, final byte[] imagen, final String token, final String tokenUsuario, final Context context) {
        Log.e("DAORemoto", "insertCita");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_INSERT_CITA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (Objects.equals(obj.getString("error"), "false")) {
                                DAOLocal.updateCitaStatus(id, RC_SYNC, context);

                                context.sendBroadcast(new Intent(RC_BROADCAST));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("usuario",usuario);
                params.put("especie",especie);
                params.put("latitud",latitud);
                params.put("longitud",longitud);
                params.put("paraje",paraje);
                params.put("municipio",municipio);
                params.put("numero",numero);
                params.put("edad",edad);
                params.put("sexo",sexo);
                params.put("interes",interes);
                params.put("observaciones",observaciones);
                params.put("privacidad",privacidad);
                params.put("filtro",filtro);
                params.put("fecha",fecha);
                String imageBase64 = Base64.encodeToString(imagen, Base64.DEFAULT);
                params.put("imagen",imageBase64);
                params.put("token",token);
                params.put("tokenUsuario",tokenUsuario);
                return params;
            }
        };
        Log.e("DAORemoto", "insertCita "+ stringRequest);
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void updateCita(final int id, final String usuario, final String especie, final String latitud,
                            final String longitud, final String paraje, final String municipio,
                            final String numero, final String edad, final String sexo, final String interes,
                            final String observaciones, final String privacidad, final String filtro,
                            final String fecha, final byte[] imagen, final String token, final String tokenUsuario, final Context context) {
        Log.e("DAORemoto", "updateCita");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_UPDATE_CITA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("DAORemoto", "updateCita "+ response);
                            JSONObject obj = new JSONObject(response);
                            if (Objects.equals(obj.getString("error"), "false")) {
                                DAOLocal.updateCitaStatus(id, RC_SYNC, context);

                                context.sendBroadcast(new Intent(RC_BROADCAST));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("usuario",usuario);
                params.put("especie",especie);
                params.put("latitud",latitud);
                params.put("longitud",longitud);
                params.put("paraje",paraje);
                params.put("municipio",municipio);
                params.put("numero",numero);
                params.put("edad",edad);
                params.put("sexo",sexo);
                params.put("interes",interes);
                params.put("observaciones",observaciones);
                params.put("privacidad",privacidad);
                params.put("filtro",filtro);
                params.put("fecha",fecha);
                String imageBase64 = Base64.encodeToString(imagen, Base64.DEFAULT);
                params.put("imagen",imageBase64);
                params.put("token",token);
                params.put("tokenUsuario",tokenUsuario);
                return params;
            }
        };
        Log.e("DAORemoto", "updateCita "+ stringRequest);
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void deleteCita(final int id, final String token, final String tokenUsuario, final Context context) {
        Log.e("DAORemoto", "deleteCita");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DELETE_CITA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (Objects.equals(obj.getString("error"), "false")) {
                                DAOLocal.delCita(id, context);

                                context.sendBroadcast(new Intent(RC_BROADCAST));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("token",token);
                params.put("tokenUsuario",tokenUsuario);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void sincronizarCitas(final String usuario, final Context context) {
        Log.e("DAORemoto", "sincronizarCitas");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_GET_CITAS_USUARIO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (Objects.equals(obj.getString("error"), "false")) {
                                JSONArray arr = obj.getJSONArray("cita");
                                for(int i=0;i<arr.length();i++){
                                    JSONObject jso = arr.getJSONObject(i);
                                    Cita cita = new Cita();
                                    cita.setUsuario(jso.getString("usuario"));
                                    cita.setEspecie(jso.getString("especie"));
                                    cita.setLatitud(jso.getString("latitud"));
                                    cita.setLongitud(jso.getString("longitud"));
                                    cita.setParaje(jso.getString("paraje"));
                                    cita.setMunicipio(jso.getString("municipio"));
                                    cita.setNumero(jso.getString("numero"));
                                    cita.setEdad(jso.getString("edad"));
                                    cita.setSexo(jso.getString("sexo"));
                                    cita.setInteres(jso.getString("interes"));
                                    cita.setObservaciones(jso.getString("observaciones"));
                                    cita.setPrivacidad(jso.getString("privacidad"));
                                    cita.setFiltro(jso.getString("filtro"));
                                    cita.setFecha(jso.getString("fecha"));
                                    cita.setToken(jso.getString("token"));
                                    cita.setStatus(RC_SYNC);

                                    String imagenBlob = jso.getString("imagen");
                                    byte[] imageBase64 = Base64.decode(imagenBlob, Base64.DEFAULT);
                                    Bitmap imagen = Util.getImage(imageBase64);
                                    cita.setImagen(imagen);

                                    DAOLocal.addCita(cita, context);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("usuario",usuario);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    public static void insertAuth(final Usuario usuario, final Context context) {
        Log.e("DAORemoto", "insertAuth");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_INSERT_AUTH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DAORemoto", "insertAuthresponse " +response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            Log.e("DAORemoto", "insertAuthobj " +obj);
                            if (Objects.equals(obj.getString("error"), "false")) {
                                Log.e("DAORemoto", "error");
                                JSONObject jso = obj.getJSONObject("auth");
                                usuario.setToken(jso.getString("token"));
                                DAOLocal.updateUser(usuario, context);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("email",usuario.getEmail());
                params.put("token",usuario.getToken());
                return params;
            }
        };
        Log.e("DAORemoto", "insertAuth "+stringRequest);
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }



}
