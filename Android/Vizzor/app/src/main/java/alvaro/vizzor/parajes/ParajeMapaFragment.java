package alvaro.vizzor.parajes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;


public class ParajeMapaFragment extends Fragment implements OnMapReadyCallback{

    private GoogleMap mGoogleMap;
    private SupportMapFragment mapFrag;
    private SupportMapFragment mapFragC;
    private boolean miniMapReady = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_paraje_mapa, container, false);

        mapFrag = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.mapa);
        mapFragC = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.mapaCanarias);

        mapFrag.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        //mGoogleMap.getUiSettings().setScrollGesturesEnabled(true);

        LatLng latLng;

        for(int i = 0; Singleton.getInstance().getParajes().size()>i; i++) {
            latLng = new LatLng(Double.parseDouble(Singleton.getInstance().getParajes().get(i).getLatitud()), Double.parseDouble(Singleton.getInstance().getParajes().get(i).getLongitud()));
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(Singleton.getInstance().getParajes().get(i).getNombre());
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));

            mGoogleMap.addMarker(markerOptions);

        }

        if (!miniMapReady) {
            mGoogleMap = googleMap;
            latLng = new LatLng(38.4, -3);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
            mapFragC.getMapAsync(this);
            miniMapReady = true;
        } else {
            mGoogleMap = googleMap;
            latLng = new LatLng(29.25, -16.75);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
        }

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                if (marker.isInfoWindowShown()) {
                    marker.hideInfoWindow();
                } else {
                    marker.showInfoWindow();
                }
                return true;
            }
        });

        /*latLng = new LatLng(39.9734444, -4.8549893);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(5));*/

    }

}
