package alvaro.vizzor;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


public class SplashActivity extends AppCompatActivity {
    private Context context;

    protected boolean active = true;
    protected int tiempo = 3000;

    protected Handler exitHandler = null;
    protected Runnable exitRunnable = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = getApplicationContext();

        try {
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();

            String action = intent.getAction();

            if (Intent.ACTION_SEND.equals(action)) {
                if (extras.containsKey(Intent.EXTRA_STREAM)) {
                    Uri uri = extras.getParcelable(Intent.EXTRA_STREAM);
                }
            }
        } catch (Exception ex) {
            //Do nothing for now;
        }

        exitRunnable = new Runnable() {
            public void run() {
                exitSplash();
            }
        };

        exitHandler = new Handler();
        exitHandler.postDelayed(exitRunnable, tiempo);

    }

    private void exitSplash()
    {
        Intent intent = new Intent();
        intent.setClass(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
}
