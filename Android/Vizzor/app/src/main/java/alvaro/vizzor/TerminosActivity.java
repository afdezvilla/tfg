package alvaro.vizzor;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class TerminosActivity extends AppCompatActivity {
    private Context context;

    private Button btnCerrar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminos);

        context = getApplicationContext();

        Log.e("terminos", "terminos");

        btnCerrar = (Button) findViewById(R.id.btnCerrar);


        btnCerrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();
    }


}
