package alvaro.vizzor.parajes;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import java.util.ArrayList;

import alvaro.vizzor.R;
import alvaro.vizzor.citas.CitaAdaptadorLista;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.model.Cita;
import alvaro.vizzor.sql.DAOLocal;


public class ParajeCitasFragment extends Fragment {

    private RecyclerView recView;
    private ArrayList<Cita> datos;
    private ArrayList<Cita> filtro;
    private CitaAdaptadorLista adaptador;

    private SwipeRefreshLayout refreshLayout;

    private SearchView search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_paraje_citas, container, false);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);

        recView = (RecyclerView) view.findViewById(R.id.paraje_listar_recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recView.setLayoutManager(linearLayoutManager);

        datos = new ArrayList<>();
        filtro = new ArrayList<>();

        adaptador = new CitaAdaptadorLista(getActivity(), datos);

        recView.setAdapter(adaptador);

        search = (SearchView) view.findViewById(R.id.search);
        search.setIconified(false);
        search.clearFocus();

        loadCitasLocales();

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadCitasLocales();
                        refreshLayout.setRefreshing(false);
                    }
                }
        );

        adaptador = new CitaAdaptadorLista(getActivity(), datos);
        recView.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();
                filtro.clear();
                for (int i = 0; i < datos.size(); i++) {

                    final String text = datos.get(i).toString().toLowerCase();
                    if (text.contains(query)) {
                        filtro.add(datos.get(i));
                    }
                }

                adaptador = new CitaAdaptadorLista(getActivity(), filtro);
                recView.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
                return true;
            }
        });

        return view;
    }


    private void loadCitasLocales() {
        datos.clear();
        datos.addAll(DAOLocal.getAllCitasOnlineFiltro(Singleton.getInstance().getParaje().getNombre(), getActivity()));
        adaptador = new CitaAdaptadorLista(getActivity(), datos);
        recView.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

}