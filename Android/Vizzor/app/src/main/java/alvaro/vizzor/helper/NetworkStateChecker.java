package alvaro.vizzor.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import alvaro.vizzor.model.Cita;
import alvaro.vizzor.sql.DAOLocal;
import alvaro.vizzor.sql.DAORemoto;
import alvaro.vizzor.sql.DatabaseHelper;

public class NetworkStateChecker extends BroadcastReceiver {

    private Context context;
    private Cita cita = new Cita();

    @Override
    public void onReceive(Context context, Intent intent) {
        //NukeSSLCerts.nuke();
        Log.e("NetworkStateChecker", "onReceive");
        this.context = context;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                sincronizarCitasInsert();
                sincronizarCitasUpdate();
                sincronizarCitasDelete();
            }
        }
    }

    private void sincronizarCitasInsert(){
        Cursor cursor = DAOLocal.getUnsyncedCita(context);
        if (cursor.moveToFirst()) {
            do {
                DAORemoto.insertCita(
                        Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_ID))),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_USERNAME)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_ESPECIE)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_LATITUD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_LONGITUD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_PARAJE)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_MUNICIPIO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_NUMERO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_EDAD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_SEXO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_INTERES)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_OBSERVACION)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_PRIVACIDAD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_FILTRO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_FECHA)),
                        cursor.getBlob(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_IMAGEN)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_TOKEN)),
                        Singleton.getInstance().getUsuario().getToken(),
                        context
                );

            } while (cursor.moveToNext());
        }
    }

    private void sincronizarCitasUpdate(){
        Cursor cursor = DAOLocal.getUnupdatedCita(context);
        if (cursor.moveToFirst()) {
            do {
                DAORemoto.updateCita(
                        Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_ID))),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_USERNAME)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_ESPECIE)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_LATITUD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_LONGITUD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_PARAJE)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_MUNICIPIO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_NUMERO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_EDAD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_SEXO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_INTERES)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_OBSERVACION)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_PRIVACIDAD)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_FILTRO)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_FECHA)),
                        cursor.getBlob(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_IMAGEN)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_TOKEN)),
                        Singleton.getInstance().getUsuario().getToken(),
                        context
                );

            } while (cursor.moveToNext());
        }
    }

    private void sincronizarCitasDelete(){
        Cursor cursor = DAOLocal.getUndeletedCita(context);
        if (cursor.moveToFirst()) {
            do {
                DAORemoto.deleteCita(
                        Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_ID))),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITA_TOKEN)),
                        Singleton.getInstance().getUsuario().getToken(),
                        context
                );

            } while (cursor.moveToNext());
        }
    }



}
