package alvaro.vizzor.citas;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.model.Cita;


public class CitaAdaptadorLista extends RecyclerView.Adapter<CitaAdaptadorLista.CitaViewHolder>{

    private Context context;
    private ArrayList<Cita> datos;

    public CitaAdaptadorLista(Context context, ArrayList<Cita> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public CitaViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.element_cita, viewGroup, false);

        CitaViewHolder tvh = new CitaViewHolder(itemView);

        return tvh;
    }

    @Override
    public void onBindViewHolder(CitaViewHolder viewHolder, int pos) {
        Cita item = datos.get(pos);

        String castellano[] = item.getEspecie().split("\\( ");

        String cientifico[] = castellano[1].split(" \\)");

        Glide.with(context)
                .load(item.getImagen())
                .apply(new RequestOptions().override(100, 100).fitCenterTransform())
                .into(viewHolder.ivImagen);

        viewHolder.lblTitulo.setText(castellano[0]);
        viewHolder.lblSubTitulo.setText(cientifico[0]);
        viewHolder.lblMunicipio.setText(item.getMunicipio());
        viewHolder.lblParaje.setText(item.getParaje());
        viewHolder.lblCreador.setText(item.getUsuario());
        viewHolder.lblFecha.setText(item.getFecha());
    }

    @Override
    public int getItemCount() {
        return (null != datos ? datos.size() : 0);
    }

    public void addAll(ArrayList<Cita> citas) {
        datos.addAll(citas);
        notifyDataSetChanged();
    }

    public void add(Cita cita) {
        datos.add(cita);
        notifyDataSetChanged();
    }

    public void clear(){
        datos.clear();
        notifyDataSetChanged();
    }

    public class CitaViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private ImageView ivImagen;
        private TextView lblTitulo;
        private TextView lblSubTitulo;
        private TextView lblMunicipio;
        private TextView lblParaje;
        private TextView lblCreador;
        private TextView lblFecha;


        public CitaViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ivImagen = (ImageView)itemView.findViewById(R.id.ivImagen);
            lblTitulo = (TextView)itemView.findViewById(R.id.lblTitulo);
            lblSubTitulo = (TextView)itemView.findViewById(R.id.lblSubTitulo);
            lblMunicipio = (TextView)itemView.findViewById(R.id.lblMunicipio);
            lblParaje = (TextView)itemView.findViewById(R.id.lblParaje);
            lblCreador = (TextView)itemView.findViewById(R.id.lblCreador);
            lblFecha = (TextView)itemView.findViewById(R.id.lblFecha);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, CitaDetallesActivity.class);
                    Cita cita = new Cita();
                    cita.setUsuario(datos.get(getAdapterPosition()).getUsuario());
                    cita.setEspecie(datos.get(getAdapterPosition()).getEspecie());
                    cita.setLatitud(datos.get(getAdapterPosition()).getLatitud());
                    cita.setLongitud(datos.get(getAdapterPosition()).getLongitud());
                    cita.setParaje(datos.get(getAdapterPosition()).getParaje());
                    cita.setMunicipio(datos.get(getAdapterPosition()).getMunicipio());
                    cita.setNumero(datos.get(getAdapterPosition()).getNumero());
                    cita.setEdad(datos.get(getAdapterPosition()).getEdad());
                    cita.setSexo(datos.get(getAdapterPosition()).getSexo());
                    cita.setInteres(datos.get(getAdapterPosition()).getInteres());
                    cita.setObservaciones(datos.get(getAdapterPosition()).getObservaciones());
                    cita.setPrivacidad(datos.get(getAdapterPosition()).getPrivacidad());
                    cita.setFiltro(datos.get(getAdapterPosition()).getFiltro());
                    cita.setFecha(datos.get(getAdapterPosition()).getFecha());
                    cita.setImagen(datos.get(getAdapterPosition()).getImagen());
                    Singleton.getInstance().setCita(cita);
                    context.startActivity(i);
                    ((Activity)context).finish();

                }
            });
        }


    }
}