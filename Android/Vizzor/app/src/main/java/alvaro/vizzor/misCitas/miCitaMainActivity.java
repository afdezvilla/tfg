package alvaro.vizzor.misCitas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import alvaro.vizzor.AyudaActivity;
import alvaro.vizzor.InformacionActivity;
import alvaro.vizzor.MainActivity;
import alvaro.vizzor.R;
import alvaro.vizzor.citas.CitaMainActivity;
import alvaro.vizzor.helper.NetworkStateChecker;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.model.Cita;
import alvaro.vizzor.parajes.ParajeMainActivity;
import alvaro.vizzor.sql.DAOLocal;
import alvaro.vizzor.usuario.UsuarioDetallesActivity;

import static alvaro.vizzor.helper.Config.RC_BROADCAST;
import static alvaro.vizzor.helper.Config.RC_CREAR_CITA;


public class miCitaMainActivity extends AppCompatActivity {
    private Context context;

    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private FloatingActionButton fab;

    private ImageView ivCircle;
    private TextView txtUsername;

    private RecyclerView recView;
    private ArrayList<Cita> datos;
    private ArrayList<Cita> filtro;
    private SearchView search;
    private miCitaAdaptadorLista adaptador;

    private SwipeRefreshLayout refreshLayout;

    private BroadcastReceiver broadcastReceiver;

    private Bitmap imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_micita_main);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("micita_main", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Log.e("micita_main", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_nav_menu);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(true);

        collapsingToolbar.setTitle(getString(R.string.mis_citas));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navView = (NavigationView) findViewById(R.id.navview);

        View nav = navView.getHeaderView(0);

        ivCircle = (ImageView)nav.findViewById(R.id.ivCircle);
        txtUsername = (TextView) nav.findViewById(R.id.txtUsername);

        txtUsername.setText(Singleton.getInstance().getUsuario().getEmail());
        imagen = Singleton.getInstance().getUsuario().getImagen();

        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .into(ivCircle);

        datos = new ArrayList<>();
        filtro = new ArrayList<>();

        adaptador = new miCitaAdaptadorLista(context, datos);

        search = (SearchView) findViewById(R.id.search);
        search.setIconified(false);
        search.clearFocus();

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

        recView = (RecyclerView) findViewById(R.id.micita_listar_recyclerView);
        recView.setHasFixedSize(true);

        recView.setAdapter(adaptador);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recView.setLayoutManager(linearLayoutManager);

        loadCitas();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                loadCitas();
            }
        };

        registerReceiver(broadcastReceiver, new IntentFilter(RC_BROADCAST));

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        datos.clear();
                        loadCitas();
                        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        refreshLayout.setRefreshing(false);
                    }
                }
        );

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(context, miCitaCrearActivity.class), RC_CREAR_CITA);
            }
        });

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();
                filtro.clear();
                for (int i = 0; i < datos.size(); i++) {

                    final String text = datos.get(i).toString().toLowerCase();
                    if (text.contains(query)) {
                        filtro.add(datos.get(i));
                    }
                }

                adaptador = new miCitaAdaptadorLista(context, filtro);
                recView.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
                return true;
            }
        });

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.menu_inicio:
                                Intent intentoMain = new Intent(context, MainActivity.class);
                                startActivity(intentoMain);
                                finish();
                                break;
                            case R.id.menu_mis_citas:
                                Intent intentoMiCita = new Intent(context, miCitaMainActivity.class);
                                startActivity(intentoMiCita);
                                finish();
                                break;
                            case R.id.menu_citas:
                                Intent intentoCita = new Intent(context, CitaMainActivity.class);
                                startActivity(intentoCita);
                                finish();
                                break;
                            case R.id.menu_parajes:
                                Intent intentoParajes = new Intent(context, ParajeMainActivity.class);
                                startActivity(intentoParajes);
                                finish();
                                break;
                            case R.id.menu_perfil:
                                Intent intentoPerfil = new Intent(context, UsuarioDetallesActivity.class);
                                startActivity(intentoPerfil);
                                finish();
                                break;
                            case R.id.menu_informacion:
                                Intent intentoInformacion = new Intent(context, InformacionActivity.class);
                                startActivity(intentoInformacion);
                                finish();
                                break;
                            case R.id.menu_ayuda:
                                Intent intentoAyuda = new Intent(context, AyudaActivity.class);
                                startActivity(intentoAyuda);
                                finish();
                                break;
                            case R.id.menu_salir:
                                Util.logout(context, miCitaMainActivity.this);
                                finish();
                                break;
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_CREAR_CITA && resultCode == RESULT_OK) {
            loadCitas();
            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadCitas() {
        datos.clear();
        datos.addAll(DAOLocal.getAllCitas(Singleton.getInstance().getUsuario().getEmail(), context));

        adaptador = new miCitaAdaptadorLista(context, datos);
        recView.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        Intent intentoMain = new Intent(context, MainActivity.class);
        startActivity(intentoMain);
        finish();
    }

}


