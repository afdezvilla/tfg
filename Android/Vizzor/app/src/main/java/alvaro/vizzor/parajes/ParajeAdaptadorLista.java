package alvaro.vizzor.parajes;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.model.Paraje;


public class ParajeAdaptadorLista extends RecyclerView.Adapter<ParajeAdaptadorLista.ParajeViewHolder>{

    private Context context;
    private ArrayList<Paraje> datos;

    public ParajeAdaptadorLista(Context context, ArrayList<Paraje> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public ParajeViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.element_paraje, viewGroup, false);

        ParajeViewHolder tvh = new ParajeViewHolder(itemView);

        return tvh;
    }

    @Override
    public void onBindViewHolder(ParajeViewHolder viewHolder, int pos) {
        Paraje item = datos.get(pos);
        Glide.with(context)
                .load(Uri.parse("file:///android_asset/" + item.getImagen()))
                .apply(new RequestOptions().override(100, 100).fitCenterTransform())
                .into(viewHolder.ivImagen);

        viewHolder.lblTitulo.setText(item.getNombre());
        viewHolder.lblSubTitulo.setText(item.getMunicipio());
    }

    @Override
    public int getItemCount() {
        return (null != datos ? datos.size() : 0);
    }

    public void addAll(ArrayList<Paraje> parajes) {
        datos.addAll(parajes);
        notifyDataSetChanged();
    }

    public void clear(){
        datos.clear();
        notifyDataSetChanged();
    }

    public class ParajeViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private ImageView ivImagen;
        private TextView lblTitulo;
        private TextView lblSubTitulo;


        public ParajeViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ivImagen = (ImageView)itemView.findViewById(R.id.ivImagen);
            lblTitulo = (TextView)itemView.findViewById(R.id.lblTitulo);
            lblSubTitulo = (TextView)itemView.findViewById(R.id.lblSubTitulo);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ParajeDetallesMainActivity.class);
                    Paraje paraje = new Paraje();
                    paraje.setNombre(datos.get(getAdapterPosition()).getNombre());
                    paraje.setLatitud(datos.get(getAdapterPosition()).getLatitud());
                    paraje.setLongitud(datos.get(getAdapterPosition()).getLongitud());
                    paraje.setMunicipio(datos.get(getAdapterPosition()).getMunicipio());
                    paraje.setInteres(datos.get(getAdapterPosition()).getInteres());
                    paraje.setRadio(datos.get(getAdapterPosition()).getRadio());
                    paraje.setImagen(datos.get(getAdapterPosition()).getImagen());
                    Singleton.getInstance().setParaje(paraje);
                    context.startActivity(i);
                    ((Activity)context).finish();
                }
            });

        }


    }
}