package alvaro.vizzor.parajes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class ParajeViewPagerAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] =
            new String[] {"Mapa", "Listar"};

    public ParajeViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                ParajeMapaFragment parajeMapa = new ParajeMapaFragment();
                return parajeMapa;
            case 1:
                ParajeListarFragment parajeListar = new ParajeListarFragment();
                return parajeListar;
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}