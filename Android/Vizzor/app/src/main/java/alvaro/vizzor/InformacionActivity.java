package alvaro.vizzor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import alvaro.vizzor.citas.CitaMainActivity;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.misCitas.miCitaMainActivity;
import alvaro.vizzor.parajes.ParajeMainActivity;
import alvaro.vizzor.usuario.UsuarioDetallesActivity;

public class InformacionActivity extends AppCompatActivity {
    private Context context;

    private DrawerLayout drawerLayout;
    private NavigationView navView;

    private ImageView ivCircle;
    private TextView txtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("informacion", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Log.e("informacion", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_nav_menu);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(true);

        collapsingToolbar.setTitle(getString(R.string.informacion));

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        navView = (NavigationView)findViewById(R.id.navview);

        View nav =  navView.getHeaderView(0);

        ivCircle = (ImageView)nav.findViewById(R.id.ivCircle);
        txtUsername = (TextView) nav.findViewById(R.id.txtUsername);

        txtUsername.setText(Singleton.getInstance().getUsuario().getEmail());
        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .into(ivCircle);

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.menu_inicio:
                                Intent intentoMain = new Intent(context, MainActivity.class);
                                startActivity(intentoMain);
                                finish();
                                break;
                            case R.id.menu_mis_citas:
                                Intent intentoMiCita = new Intent(context, miCitaMainActivity.class);
                                startActivity(intentoMiCita);
                                finish();
                                break;
                            case R.id.menu_citas:
                                Intent intentoCita = new Intent(context, CitaMainActivity.class);
                                startActivity(intentoCita);
                                finish();
                                break;
                            case R.id.menu_parajes:
                                Intent intentoParajes = new Intent(context, ParajeMainActivity.class);
                                startActivity(intentoParajes);
                                finish();
                                break;
                            case R.id.menu_perfil:
                                Intent intentoPerfil = new Intent(context, UsuarioDetallesActivity.class);
                                startActivity(intentoPerfil);
                                finish();
                                break;
                            case R.id.menu_informacion:
                                Intent intentoInformacion = new Intent(context, InformacionActivity.class);
                                startActivity(intentoInformacion);
                                finish();
                                break;
                            case R.id.menu_ayuda:
                                Intent intentoAyuda = new Intent(context, AyudaActivity.class);
                                startActivity(intentoAyuda);
                                finish();
                                break;
                            case R.id.menu_salir:
                                Util.logout(context, InformacionActivity.this);
                                finish();
                                break;
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intentoMain = new Intent(context, MainActivity.class);
        startActivity(intentoMain);
        finish();
    }


}
