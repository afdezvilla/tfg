package alvaro.vizzor.citas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

import alvaro.vizzor.AyudaActivity;
import alvaro.vizzor.InformacionActivity;
import alvaro.vizzor.MainActivity;
import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.misCitas.miCitaMainActivity;
import alvaro.vizzor.parajes.ParajeDetallesMainActivity;
import alvaro.vizzor.parajes.ParajeMainActivity;
import alvaro.vizzor.usuario.UsuarioDetallesActivity;


public class CitaDetallesActivity extends AppCompatActivity implements OnMapReadyCallback{
    private Context context;

    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private FloatingActionButton fab;

    private GoogleMap mGoogleMap;
    private SupportMapFragment mapFrag;

    private TextView txtEspecie;
    private TextView txtMunicipio;
    private TextView txtParaje;
    private TextView txtNumero;
    private TextView txtEdad;
    private TextView txtSexo;
    private TextView txtInteres;
    private TextView txtObservaciones;
    private TextView txtCreador;
    private TextView txtFechaDia;
    private TextView txtFechaMes;
    private TextView txtFechaAno;
    private TextView txtCoordenadas;

    private String latitud;
    private String longitud;
    private ImageView ivImagen;

    private ImageView ivCircle;
    private TextView txtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cita_detalles);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("cita_detalles", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Log.e("cita_detalles", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_nav_menu);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(true);

        txtEspecie = (TextView) findViewById(R.id.txtEspecie);
        txtMunicipio = (TextView) findViewById(R.id.txtMunicipio);
        txtParaje = (TextView) findViewById(R.id.txtParaje);
        txtNumero = (TextView) findViewById(R.id.txtNumero);
        txtEdad = (TextView) findViewById(R.id.txtEdad);
        txtSexo = (TextView) findViewById(R.id.txtSexo);
        txtInteres = (TextView) findViewById(R.id.txtInteres);
        txtObservaciones = (TextView) findViewById(R.id.txtObservaciones);
        txtCreador = (TextView) findViewById(R.id.txtCreador);
        txtFechaDia = (TextView) findViewById(R.id.txtFechaDia);
        txtFechaMes = (TextView) findViewById(R.id.txtFechaMes);
        txtFechaAno = (TextView) findViewById(R.id.txtFechaAno);
        txtCoordenadas = (TextView) findViewById(R.id.txtCoordenadas);

        ivImagen = (ImageView) findViewById(R.id.ivImagen);

        txtEspecie.setText(Singleton.getInstance().getCita().getEspecie());
        txtMunicipio.setText(Singleton.getInstance().getCita().getMunicipio());
        txtParaje.setText(Singleton.getInstance().getCita().getParaje());
        txtNumero.setText(Singleton.getInstance().getCita().getNumero());
        txtEdad.setText(Singleton.getInstance().getCita().getEdad());
        txtSexo.setText(Singleton.getInstance().getCita().getSexo());
        txtInteres.setText(Singleton.getInstance().getCita().getInteres());
        txtObservaciones.setText(Singleton.getInstance().getCita().getObservaciones());
        txtCreador.setText(Singleton.getInstance().getCita().getUsuario());

        String parts[] = Singleton.getInstance().getCita().getFecha().split("/");

        int day = Integer.parseInt(parts[0]);
        int month = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[2]);

        txtFechaDia.setText(String.valueOf(day));
        txtFechaMes.setText(Util.getMes(context, (month-1)));
        txtFechaAno.setText(String.valueOf(year));

        latitud = Singleton.getInstance().getCita().getLatitud();
        longitud = Singleton.getInstance().getCita().getLongitud();

        txtCoordenadas.setText(Util.convertCoordenadas(Double.parseDouble(latitud), Double.parseDouble(longitud)));

        Glide.with(context)
                .load(Singleton.getInstance().getCita().getImagen())
                .apply(new RequestOptions().override(150, 150).fitCenterTransform())
                .into(ivImagen);

        String especies[] = Singleton.getInstance().getCita().getEspecie().split("\\(");

        collapsingToolbar.setTitle(especies[0]);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        navView = (NavigationView)findViewById(R.id.navview);

        View nav =  navView.getHeaderView(0);

        ivCircle = (ImageView)nav.findViewById(R.id.ivCircle);
        txtUsername = (TextView) nav.findViewById(R.id.txtUsername);

        txtUsername.setText(Singleton.getInstance().getUsuario().getEmail());
        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .into(ivCircle);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFrag.getMapAsync(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri imageUri = Util.getImageUri(context, Singleton.getInstance().getCita().getImagen());
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("image/png");
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                sharingIntent.putExtra(Intent.EXTRA_TEXT,Singleton.getInstance().getCita().compartir());
                sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.compartir)));
            }
        });

        ivImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog nagDialog = new Dialog(CitaDetallesActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(true);
                nagDialog.setContentView(R.layout.dialog_imagen);
                ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
                Glide.with(context)
                        .load(Singleton.getInstance().getCita().getImagen())
                        .into(ivPreview);

                ivPreview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();
            }
        });

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.menu_inicio:
                                Intent intentoMain = new Intent(context, MainActivity.class);
                                startActivity(intentoMain);
                                finish();
                                break;
                            case R.id.menu_mis_citas:
                                Intent intentoMiCita = new Intent(context, miCitaMainActivity.class);
                                startActivity(intentoMiCita);
                                finish();
                                break;
                            case R.id.menu_citas:
                                Intent intentoCita = new Intent(context, CitaMainActivity.class);
                                startActivity(intentoCita);
                                finish();
                                break;
                            case R.id.menu_parajes:
                                Intent intentoParajes = new Intent(context, ParajeMainActivity.class);
                                startActivity(intentoParajes);
                                finish();
                                break;
                            case R.id.menu_perfil:
                                Intent intentoPerfil = new Intent(context, UsuarioDetallesActivity.class);
                                startActivity(intentoPerfil);
                                finish();
                                break;
                            case R.id.menu_informacion:
                                Intent intentoInformacion = new Intent(context, InformacionActivity.class);
                                startActivity(intentoInformacion);
                                finish();
                                break;
                            case R.id.menu_ayuda:
                                Intent intentoAyuda = new Intent(context, AyudaActivity.class);
                                startActivity(intentoAyuda);
                                finish();
                                break;
                            case R.id.menu_salir:
                                Util.logout(context, CitaDetallesActivity.this);
                                finish();
                                break;
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);

        LatLng latLng = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));

        mGoogleMap.addMarker(markerOptions);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));

    }

    @Override
    public void onBackPressed() {
        if (Objects.equals(Singleton.getInstance().getOrigen(), "cita")) {
            Intent intentoCita = new Intent(context, CitaMainActivity.class);
            startActivity(intentoCita);
            finish();
        }else if(Objects.equals(Singleton.getInstance().getOrigen(), "paraje")) {
            Intent intentoCita = new Intent(context, ParajeDetallesMainActivity.class);
            startActivity(intentoCita);
            finish();
        }

    }




}
