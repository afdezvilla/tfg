package alvaro.vizzor.sql;


import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public class PopulateParajes {

    public static void poblarParajes(SQLiteDatabase db, String TABLE_PARAJE, String COLUMN_PARAJE_NOMBRE,
                                     String COLUMN_PARAJE_LATITUD, String COLUMN_PARAJE_LONGITUD,
                                     String COLUMN_PARAJE_MUNICIPIO, String COLUMN_PARAJE_RADIO,
                                     String COLUMN_PARAJE_INTERES, String COLUMN_PARAJE_IMAGEN) {

        ContentValues AiguasTortasyLagodeSanMauricio = new ContentValues();
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_NOMBRE, "Aiguas Tortas y Lago de San Mauricio");
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_LATITUD, "42.577222");
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_LONGITUD, "0.947778");
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_MUNICIPIO, "Lérida");
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_RADIO, "10000");
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        AiguasTortasyLagodeSanMauricio.put(COLUMN_PARAJE_IMAGEN, "Aiguestortes.png");

        ContentValues Doñana = new ContentValues();
        Doñana.put(COLUMN_PARAJE_NOMBRE, "Doñana");
        Doñana.put(COLUMN_PARAJE_LATITUD, "37.02");
        Doñana.put(COLUMN_PARAJE_LONGITUD, "-6.44");
        Doñana.put(COLUMN_PARAJE_MUNICIPIO, "Huelva, Sevilla, Cádiz");
        Doñana.put(COLUMN_PARAJE_RADIO, "10000");
        Doñana.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        Doñana.put(COLUMN_PARAJE_IMAGEN, "Donana.png");

        ContentValues Teide = new ContentValues();
        Teide.put(COLUMN_PARAJE_NOMBRE, "Teide");
        Teide.put(COLUMN_PARAJE_LATITUD, "28.25");
        Teide.put(COLUMN_PARAJE_LONGITUD, "-16.6167");
        Teide.put(COLUMN_PARAJE_MUNICIPIO, "Tenerife");
        Teide.put(COLUMN_PARAJE_RADIO, "10000");
        Teide.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        Teide.put(COLUMN_PARAJE_IMAGEN, "Teide.png");

        ContentValues CalderadeTaburiente = new ContentValues();
        CalderadeTaburiente.put(COLUMN_PARAJE_NOMBRE, "Caldera de Taburiente");
        CalderadeTaburiente.put(COLUMN_PARAJE_LATITUD, "28.753928");
        CalderadeTaburiente.put(COLUMN_PARAJE_LONGITUD, "-17.884719");
        CalderadeTaburiente.put(COLUMN_PARAJE_MUNICIPIO, "La Palma");
        CalderadeTaburiente.put(COLUMN_PARAJE_RADIO, "10000");
        CalderadeTaburiente.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        CalderadeTaburiente.put(COLUMN_PARAJE_IMAGEN, "Caldera.png");

        ContentValues PicosdeEuropa = new ContentValues();
        PicosdeEuropa.put(COLUMN_PARAJE_NOMBRE, "Picos de Europa");
        PicosdeEuropa.put(COLUMN_PARAJE_LATITUD, "43.21");
        PicosdeEuropa.put(COLUMN_PARAJE_LONGITUD, "-4.84");
        PicosdeEuropa.put(COLUMN_PARAJE_MUNICIPIO, "Asturias, León, Cantabria");
        PicosdeEuropa.put(COLUMN_PARAJE_RADIO, "10000");
        PicosdeEuropa.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        PicosdeEuropa.put(COLUMN_PARAJE_IMAGEN, "Picos.png");

        ContentValues OrdesayMontePerdido = new ContentValues();
        OrdesayMontePerdido.put(COLUMN_PARAJE_NOMBRE, "Ordesa y Monte Perdido");
        OrdesayMontePerdido.put(COLUMN_PARAJE_LATITUD, "42.671667");
        OrdesayMontePerdido.put(COLUMN_PARAJE_LONGITUD, "0.055556");
        OrdesayMontePerdido.put(COLUMN_PARAJE_MUNICIPIO, "Huesca");
        OrdesayMontePerdido.put(COLUMN_PARAJE_RADIO, "10000");
        OrdesayMontePerdido.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        OrdesayMontePerdido.put(COLUMN_PARAJE_IMAGEN, "Ordesa.png");

        ContentValues TablasdeDaimiel = new ContentValues();
        TablasdeDaimiel.put(COLUMN_PARAJE_NOMBRE, "Tablas de Daimiel");
        TablasdeDaimiel.put(COLUMN_PARAJE_LATITUD, "39.15");
        TablasdeDaimiel.put(COLUMN_PARAJE_LONGITUD, "-3.666667");
        TablasdeDaimiel.put(COLUMN_PARAJE_MUNICIPIO, "Ciudad Real");
        TablasdeDaimiel.put(COLUMN_PARAJE_RADIO, "10000");
        TablasdeDaimiel.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        TablasdeDaimiel.put(COLUMN_PARAJE_IMAGEN, "Tablas.png");

        ContentValues Timanfaya = new ContentValues();
        Timanfaya.put(COLUMN_PARAJE_NOMBRE, "Timanfaya");
        Timanfaya.put(COLUMN_PARAJE_LATITUD, "29.011444");
        Timanfaya.put(COLUMN_PARAJE_LONGITUD, "-13.779944");
        Timanfaya.put(COLUMN_PARAJE_MUNICIPIO, "Lanzarote");
        Timanfaya.put(COLUMN_PARAJE_RADIO, "10000");
        Timanfaya.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        Timanfaya.put(COLUMN_PARAJE_IMAGEN, "Timanfaya.png");

        ContentValues Garajonay = new ContentValues();
        Garajonay.put(COLUMN_PARAJE_NOMBRE, "Garajonay");
        Garajonay.put(COLUMN_PARAJE_LATITUD, "28.12625");
        Garajonay.put(COLUMN_PARAJE_LONGITUD, "-17.237222");
        Garajonay.put(COLUMN_PARAJE_MUNICIPIO, "La Gomera");
        Garajonay.put(COLUMN_PARAJE_RADIO, "10000");
        Garajonay.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        Garajonay.put(COLUMN_PARAJE_IMAGEN, "Garajonay.png");

        ContentValues ArchipiélagodeCabrera = new ContentValues();
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_NOMBRE, "Archipiélago de Cabrera");
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_LATITUD, "39.158333");
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_LONGITUD, "2.966667");
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_MUNICIPIO, "Islas Baleares");
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_RADIO, "10000");
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        ArchipiélagodeCabrera.put(COLUMN_PARAJE_IMAGEN, "Cabrera.png");

        ContentValues Cabañeros = new ContentValues();
        Cabañeros.put(COLUMN_PARAJE_NOMBRE, "Cabañeros");
        Cabañeros.put(COLUMN_PARAJE_LATITUD, "39.4099407");
        Cabañeros.put(COLUMN_PARAJE_LONGITUD, "-4.5069323");
        Cabañeros.put(COLUMN_PARAJE_MUNICIPIO, "Ciudad Real, Toledo");
        Cabañeros.put(COLUMN_PARAJE_RADIO, "10000");
        Cabañeros.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        Cabañeros.put(COLUMN_PARAJE_IMAGEN, "Cabaneros.png");

        ContentValues SierraNevada = new ContentValues();
        SierraNevada.put(COLUMN_PARAJE_NOMBRE, "Sierra Nevada");
        SierraNevada.put(COLUMN_PARAJE_LATITUD, "37.1");
        SierraNevada.put(COLUMN_PARAJE_LONGITUD, "-3.09");
        SierraNevada.put(COLUMN_PARAJE_MUNICIPIO, "Granada, Almería");
        SierraNevada.put(COLUMN_PARAJE_RADIO, "10000");
        SierraNevada.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        SierraNevada.put(COLUMN_PARAJE_IMAGEN, "Sierra.png");

        ContentValues IslasAtlánticasdeGalicia = new ContentValues();
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_NOMBRE, "Islas Atlánticas de Galicia");
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_LATITUD, "42.380556");
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_LONGITUD, "-8.933333");
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_MUNICIPIO, "Pontevedra, La Coruña");
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_RADIO, "10000");
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        IslasAtlánticasdeGalicia.put(COLUMN_PARAJE_IMAGEN, "Galicia.png");

        ContentValues Monfragüe = new ContentValues();
        Monfragüe.put(COLUMN_PARAJE_NOMBRE, "Monfragüe");
        Monfragüe.put(COLUMN_PARAJE_LATITUD, "39.82");
        Monfragüe.put(COLUMN_PARAJE_LONGITUD, "-5.97");
        Monfragüe.put(COLUMN_PARAJE_MUNICIPIO, "Cáceres");
        Monfragüe.put(COLUMN_PARAJE_RADIO, "10000");
        Monfragüe.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        Monfragüe.put(COLUMN_PARAJE_IMAGEN, "Monfrague.png");

        ContentValues SierradeGuadarrama = new ContentValues();
        SierradeGuadarrama.put(COLUMN_PARAJE_NOMBRE, "Sierra de Guadarrama");
        SierradeGuadarrama.put(COLUMN_PARAJE_LATITUD, "40.783333");
        SierradeGuadarrama.put(COLUMN_PARAJE_LONGITUD, "-3.983333");
        SierradeGuadarrama.put(COLUMN_PARAJE_MUNICIPIO, "Madrid, Segovia");
        SierradeGuadarrama.put(COLUMN_PARAJE_RADIO, "10000");
        SierradeGuadarrama.put(COLUMN_PARAJE_INTERES, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        SierradeGuadarrama.put(COLUMN_PARAJE_IMAGEN, "Guadarrama.png");

        db.insert(TABLE_PARAJE, null, AiguasTortasyLagodeSanMauricio);
        db.insert(TABLE_PARAJE, null, Doñana);
        db.insert(TABLE_PARAJE, null, Teide);
        db.insert(TABLE_PARAJE, null, CalderadeTaburiente);
        db.insert(TABLE_PARAJE, null, PicosdeEuropa);
        db.insert(TABLE_PARAJE, null, OrdesayMontePerdido);
        db.insert(TABLE_PARAJE, null, TablasdeDaimiel);
        db.insert(TABLE_PARAJE, null, Timanfaya);
        db.insert(TABLE_PARAJE, null, Garajonay);
        db.insert(TABLE_PARAJE, null, ArchipiélagodeCabrera);
        db.insert(TABLE_PARAJE, null, Cabañeros);
        db.insert(TABLE_PARAJE, null, SierraNevada);
        db.insert(TABLE_PARAJE, null, IslasAtlánticasdeGalicia);
        db.insert(TABLE_PARAJE, null, Monfragüe);
        db.insert(TABLE_PARAJE, null, SierradeGuadarrama);


    }
}
