package alvaro.vizzor.sql;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.List;

import alvaro.vizzor.model.Cita;
import alvaro.vizzor.model.Pajaro;
import alvaro.vizzor.model.Paraje;
import alvaro.vizzor.model.Usuario;

public class DAOLocal {
    private static DatabaseHelper databaseHelper;

    //USUARIO
    public static boolean checkUser(String email, Context context){
        Log.e("DAOLocal", "checkUser");
        Boolean check;
        databaseHelper = new DatabaseHelper(context);
        check = databaseHelper.checkUser(email);
        return check;
    }

    public static Usuario getUser(String email, Context context){
        Log.e("DAOLocal", "getUser");
        Usuario usuario;
        databaseHelper = new DatabaseHelper(context);
        usuario = databaseHelper.getUser(email);
        return usuario;
    }

    public static void addUser(Usuario usuario, Context context){
        Log.e("DAOLocal", "addUser");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.addUser(usuario);
    }

    public static void updateUser(Usuario usuario, Context context){
        Log.e("DAOLocal", "updateUser");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.updateUser(usuario);
    }

    //MIS CITAS
    public static List<Cita> getAllCitas(String email, Context context){
        Log.e("DAOLocal", "getAllCitas");
        List<Cita> datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getAllCitas(email);
        return datos;
    }

    public static void addCita(Cita cita, Context context){
        Log.e("DAOLocal", "addCita");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.addCita(cita);
    }

    public static void updateCita(Cita cita, Context context){
        Log.e("DAOLocal", "updateCita");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.updateCita(cita);
    }

    public static void delCita(Integer id, Context context){
        Log.e("DAOLocal", "delCita");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.delCita(id);
    }

    public static Cursor getUnsyncedCita(Context context) {
        Log.e("DAOLocal", "getUnsyncedCita");
        Cursor datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getUnsyncedCita();
        return datos;
    }

    public static Cursor getUnupdatedCita(Context context) {
        Log.e("DAOLocal", "getUnupdatedCita");
        Cursor datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getUnupdatedCita();
        return datos;
    }

    public static Cursor getUndeletedCita(Context context) {
        Log.e("DAOLocal", "getUndeletedCita");
        Cursor datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getUndeletedCita();
        return datos;
    }

    public static void updateCitaStatus(Integer id, Integer status, Context context) {
        Log.e("DAOLocal", "updateCitaStatus");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.updateCitaStatus(id,status);
    }

    //CITAS
    public static List<Cita> getAllCitasOnline(Context context){
        Log.e("DAOLocal", "getAllCitasOnline");
        List<Cita> datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getAllCitasOnline();
        return datos;
    }

    public static List<Cita> getAllCitasOnlineFiltro(String paraje, Context context){
        Log.e("DAOLocal", "getAllCitasOnline");
        List<Cita> datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getAllCitasOnlineFiltro(paraje);
        return datos;
    }

    public static void delCitasOnline(Context context){
        Log.e("DAOLocal", "delCitasOnline");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.delCitasOnline();
    }

    public static void addCitaOnline(Cita cita, Context context){
        Log.e("DAOLocal", "addCitaOnline");
        databaseHelper = new DatabaseHelper(context);
        databaseHelper.addCitaOnline(cita, context);
    }

    //PARAJES
    public static List<Paraje> getAllParajes(Context context){
        Log.e("DAOLocal", "getAllParajes");
        List<Paraje> datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getAllParajes();
        return datos;
    }

    //PAJAROS
    public static List<Pajaro> getAllPajaros(Context context){
        Log.e("DAOLocal", "getAllPajaros");
        List<Pajaro> datos;
        databaseHelper = new DatabaseHelper(context);
        datos = databaseHelper.getAllPajaros();
        return datos;
    }

}
