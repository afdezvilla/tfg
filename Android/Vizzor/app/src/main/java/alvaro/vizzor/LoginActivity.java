package alvaro.vizzor;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.model.Usuario;
import alvaro.vizzor.sql.DAOLocal;

import static alvaro.vizzor.helper.Config.RC_DIALOG_INTERNET;
import static alvaro.vizzor.helper.Config.RC_INTERNET;
import static alvaro.vizzor.helper.Config.RC_SIGN_IN;

public class LoginActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {

    private Context context;

    private Usuario user;

    private GoogleSignInClient mGoogleSignInClient;

    private ConnectivityManager cm;
    private NetworkInfo activeNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = getApplicationContext();

        user = new Usuario();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        loginGoogleConexion();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(context, "Error de conexion!", Toast.LENGTH_SHORT).show();
        Log.e("GoogleSignIn", "OnConnectionFailed: " + connectionResult);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        if (requestCode == RC_INTERNET) {
            loginGoogleConexion();
        }

        if (resultCode == RESULT_CANCELED){
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (!DAOLocal.checkUser(account.getEmail(), context)) {
                user.setNombre(account.getDisplayName());
                user.setEmail(account.getEmail());
                Singleton.getInstance().setUsuario(user);
                Util.sharedUser(context, account.getEmail());
                Intent intentoRegistrarGoogle = new Intent(context, RegistrarActivity.class);
                startActivity(intentoRegistrarGoogle);

            } else {
                user = DAOLocal.getUser(account.getEmail().trim(), context);
                Singleton.getInstance().setUsuario(user);
                Util.sharedUser(context, account.getEmail());
                Intent loginIntent = new Intent(context, MainActivity.class);
                startActivity(loginIntent);
                finish();
            }
        } catch (ApiException e) {
            Log.e("handleSignInResult", "signInResult:failed code=" + e.getStatusCode());
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        mGoogleSignInClient.silentSignIn().addOnCompleteListener(this,
                new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        if (task.isSuccessful()) {
                            handleSignInResult(task);
                        }else{
                            loginGoogleConexion();
                        }
                    }
                });
    }

    private void loginGoogleConexion(){
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }else{
            showDialog(RC_DIALOG_INTERNET);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case RC_DIALOG_INTERNET:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.dialog_error_internet));
                builder.setMessage(getString(R.string.dialog_conectarse));
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.dialog_si), new OkOnClickListener());
                builder.setNegativeButton(getString(R.string.dialog_no), new CancelOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class CancelOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            finish();
        }
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), RC_INTERNET);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
    }

}
