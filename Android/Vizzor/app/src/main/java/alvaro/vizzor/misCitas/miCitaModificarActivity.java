package alvaro.vizzor.misCitas;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.model.Cita;
import alvaro.vizzor.model.Pajaro;
import alvaro.vizzor.model.Paraje;
import alvaro.vizzor.sql.DAOLocal;

import static alvaro.vizzor.helper.Config.PX_H;
import static alvaro.vizzor.helper.Config.PX_W;
import static alvaro.vizzor.helper.Config.RC_CAMERA;
import static alvaro.vizzor.helper.Config.RC_DIALOG_INFO;
import static alvaro.vizzor.helper.Config.RC_GALLERY;
import static alvaro.vizzor.helper.Config.RC_NOT_UPDATE;
import static alvaro.vizzor.helper.Config.RC_UBICACION;

public class miCitaModificarActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private Context context;

    private Cita cita ;

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;

    private GoogleMap mGoogleMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private SupportMapFragment mapFrag;

    private AutoCompleteTextView txtEspecie;
    private AutoCompleteTextView txtMunicipio;
    private EditText txtParaje;
    private EditText txtNumero;
    private Spinner spEdad;
    private Spinner spSexo;
    private Spinner spInteres;
    private EditText txtObservaciones;
    private Spinner spFiltro;
    private Spinner spPrivacidad;
    private TextView txtSeleccionarLocalizacion;

    private TextView txtFechaDia;
    private TextView txtFechaMes;
    private TextView txtFechaAno;
    private TextView txtSeleccionarCalendario;
    private String fecha;
    private Button btnRegistrar;

    private TextView txtSeleccionarImagen;
    private ImageView ivImagen;

    private Bitmap imagenFinal;

    private double latitudGlobal;
    private double longitudGlobal;

    private ArrayList<Paraje> parajes;
    private ArrayList<Pajaro> pajaros;
    private String[] especies;
    private String[] municipios;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_micita_modificar);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("micita_modificar", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Log.e("micita_modificar", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(true);

        collapsingToolbar.setTitle("Modificar Cita");

        cita = new Cita();
        parajes = new ArrayList<>();

        pajaros = new ArrayList<>();
        pajaros.addAll(DAOLocal.getAllPajaros(context));

        especies = new String[pajaros.size()];

        for(int i = 0; i < pajaros.size(); i++)
        {
            especies[i] = pajaros.get(i).toString();
        }

        txtEspecie = (AutoCompleteTextView) findViewById(R.id.txtEspecie);
        txtMunicipio = (AutoCompleteTextView) findViewById(R.id.txtMunicipio);
        txtParaje = (EditText) findViewById(R.id.txtParaje);
        txtNumero = (EditText) findViewById(R.id.txtNumero);
        spEdad = (Spinner) findViewById(R.id.spEdad);
        spSexo = (Spinner) findViewById(R.id.spSexo);
        spInteres = (Spinner) findViewById(R.id.spInteres);
        txtObservaciones = (EditText) findViewById(R.id.txtObservaciones);
        spFiltro = (Spinner) findViewById(R.id.spFiltro);
        spPrivacidad = (Spinner) findViewById(R.id.spPrivacidad);
        txtSeleccionarLocalizacion = (TextView) findViewById(R.id.txtSeleccionarLocalizacion);
        txtSeleccionarImagen = (TextView) findViewById(R.id.txtSeleccionarImagen);
        ivImagen = (ImageView) findViewById(R.id.ivImagen);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        txtFechaDia = (TextView) findViewById(R.id.txtFechaDia);
        txtFechaMes = (TextView) findViewById(R.id.txtFechaMes);
        txtFechaAno = (TextView) findViewById(R.id.txtFechaAno);
        txtSeleccionarCalendario = (TextView) findViewById(R.id.txtSeleccionarCalendario);

        ArrayAdapter<String> adaptador = new ArrayAdapter<>(context, R.layout.autocomplete_pajaros, especies);
        txtEspecie.setAdapter(adaptador);

        txtEspecie.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                txtEspecie.showDropDown();
                return false;
            }
        });

        Util.textNoNull(txtEspecie, context);
        Util.textWatcherEspecies(txtEspecie, especies, context);

        Util.disableEditText(txtMunicipio);
        Util.textError(txtMunicipio, context);
        txtMunicipio.setInputType(InputType.TYPE_NULL);
        Util.textWatcher(txtMunicipio, context);

        Util.disableEditText(txtParaje);
        Util.textError(txtParaje, context);
        txtParaje.setInputType(InputType.TYPE_NULL);
        Util.textWatcher(txtParaje, context);

        Util.textNoNull(txtNumero, context);
        Util.textWatcher(txtNumero, context);

        Util.textNoNull(txtObservaciones, context);
        Util.textWatcher(txtObservaciones, context);

        String parts[] = Singleton.getInstance().getCita().getFecha().split("/");

        int day = Integer.parseInt(parts[0]);
        int month = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[2]);

        txtFechaDia.setText(String.valueOf(day));
        txtFechaMes.setText(Util.getMes(context, (month-1)));
        txtFechaAno.setText(String.valueOf(year));

        fecha = day+"/"+(month+1)+"/"+year;

        ArrayAdapter adapterEdad = ArrayAdapter.createFromResource(miCitaModificarActivity.this,R.array.edad, android.R.layout.simple_spinner_item);
        adapterEdad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEdad.setAdapter(adapterEdad);

        ArrayAdapter adapterSexo = ArrayAdapter.createFromResource(miCitaModificarActivity.this,R.array.sexo, android.R.layout.simple_spinner_item);
        adapterSexo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSexo.setAdapter(adapterSexo);

        ArrayAdapter adapterInteres = ArrayAdapter.createFromResource(miCitaModificarActivity.this,R.array.interes, android.R.layout.simple_spinner_item);
        adapterInteres.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spInteres.setAdapter(adapterInteres);

        ArrayAdapter adapterFiltro = ArrayAdapter.createFromResource(miCitaModificarActivity.this,R.array.filtro, android.R.layout.simple_spinner_item);
        adapterFiltro.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFiltro.setAdapter(adapterFiltro);

        ArrayAdapter adapterPrivacidad = ArrayAdapter.createFromResource(miCitaModificarActivity.this,R.array.privacidad, android.R.layout.simple_spinner_item);
        adapterPrivacidad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPrivacidad.setAdapter(adapterPrivacidad);

        txtEspecie.setText(Singleton.getInstance().getCita().getEspecie());
        txtMunicipio.setText(Singleton.getInstance().getCita().getMunicipio());
        txtParaje.setText(Singleton.getInstance().getCita().getParaje());
        txtNumero.setText(Singleton.getInstance().getCita().getNumero());
        spEdad.setSelection(adapterEdad.getPosition(Singleton.getInstance().getCita().getEdad()));
        spSexo.setSelection(adapterSexo.getPosition(Singleton.getInstance().getCita().getSexo()));
        spInteres.setSelection(adapterInteres.getPosition(Singleton.getInstance().getCita().getInteres()));
        txtObservaciones.setText(Singleton.getInstance().getCita().getObservaciones());

        latitudGlobal = Double.parseDouble(Singleton.getInstance().getCita().getLatitud());
        longitudGlobal = Double.parseDouble(Singleton.getInstance().getCita().getLongitud());

        imagenFinal = Singleton.getInstance().getCita().getImagen();
        Glide.with(context)
                .load(imagenFinal)
                .apply(new RequestOptions().override(247, 200).fitCenterTransform())
                .into(ivImagen);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFrag.getMapAsync(this);

        txtSeleccionarLocalizacion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(context, miCitaUbicacionActivity.class), RC_UBICACION);
            }

        });

        txtSeleccionarImagen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                final CharSequence[] items = { getString(R.string.hacer_foto), getString(R.string.seleccionar_foto), getString(R.string.cancelar) };
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(miCitaModificarActivity.this);
                builder.setTitle(getString(R.string.anadir_foto));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (items[item].equals(getString(R.string.hacer_foto))) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, RC_CAMERA);

                        } else if (items[item].equals(getString(R.string.seleccionar_foto))) {
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);//
                                startActivityForResult(Intent.createChooser(intent, getString(R.string.seleccionar_fichero)),RC_GALLERY);
                        } else if (items[item].equals(getString(R.string.cancelar))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }

        });

        txtSeleccionarCalendario.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DatePickerDialog mTimePicker = new DatePickerDialog(miCitaModificarActivity.this, R.style.MyDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int year, int month, int day) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, day);
                        txtFechaDia.setText(String.valueOf(day));
                        txtFechaMes.setText(Util.getMes(context, month));
                        txtFechaAno.setText(String.valueOf(year));
                        fecha = day+"/"+(month+1)+"/"+year;
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                mTimePicker.setTitle(getString(R.string.seleccionar_fecha));
                mTimePicker.show();
            }

        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(checkRegistro()) {
                    cita.setId(Singleton.getInstance().getCita().getId());
                    cita.setUsuario(Singleton.getInstance().getUsuario().getEmail());
                    cita.setEspecie(txtEspecie.getText().toString());
                    cita.setLatitud(String.valueOf(latitudGlobal));
                    cita.setLongitud(String.valueOf(longitudGlobal));
                    cita.setParaje(txtParaje.getText().toString());
                    cita.setMunicipio(txtMunicipio.getText().toString());
                    cita.setNumero(txtNumero.getText().toString());
                    cita.setEdad(spEdad.getSelectedItem().toString());
                    cita.setSexo(spSexo.getSelectedItem().toString());
                    cita.setInteres(spInteres.getSelectedItem().toString());
                    cita.setObservaciones(txtObservaciones.getText().toString());
                    cita.setFiltro(spFiltro.getSelectedItem().toString());
                    cita.setPrivacidad(spPrivacidad.getSelectedItem().toString());
                    cita.setFecha(fecha);
                    cita.setImagen(imagenFinal);

                    saveCita(cita);
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_GALLERY)
                onSelectFromGalleryResult(data);
            else if (requestCode == RC_CAMERA)
                onCaptureImageResult(data);
        }
        if (requestCode == RC_UBICACION && resultCode == RESULT_OK) {
            Location location = new Location("");
            location.setLatitude(data.getDoubleExtra("Latitude", 0));
            location.setLongitude(data.getDoubleExtra("Longitude", 0));
            onLocationChanged(location);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera/",
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;

        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imagenFinal = Util.getResizedBitmap(thumbnail, PX_W, PX_H);
        Glide.with(context)
                .load(imagenFinal)
                .into(ivImagen);
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap thumbnail = null;
        try {
            Uri selectedImageUri = data.getData();
            thumbnail = MediaStore.Images.Media.getBitmap(context.getContentResolver(), selectedImageUri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 50, bytes);
            byte[] byteArrayCompress = bytes.toByteArray();
            thumbnail = BitmapFactory.decodeByteArray(byteArrayCompress,0,byteArrayCompress.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        imagenFinal = Util.getResizedBitmap(thumbnail, PX_W, PX_H);
        Glide.with(context)
                .load(imagenFinal)
                .into(ivImagen);
    }

    private void saveCita(final Cita cita) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();

        saveCitaLocal(cita,RC_NOT_UPDATE,progressDialog );

    }

    private void saveCitaLocal(Cita cita, int status, ProgressDialog progressDialog ) {
        cita.setStatus(status);
        cita.setToken(Singleton.getInstance().getCita().getToken());
        DAOLocal.updateCita(cita, context);

        Singleton.getInstance().setCita(cita);

        Snackbar.make(drawerLayout, getString(R.string.success), Snackbar.LENGTH_LONG).show();
        progressDialog.dismiss();
        Intent intentoMiCita = new Intent(context, miCitaDetallesActivity.class);
        startActivity(intentoMiCita);
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);

        LatLng latLng = new LatLng(latitudGlobal, longitudGlobal);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));

        mGoogleMap.addMarker(markerOptions);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location)
    {
        LatLng latLng;

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        latitudGlobal = location.getLatitude();
        longitudGlobal = location.getLongitude();

        getLocalidad();
        getParaje(latLng);

        mGoogleMap.clear();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private void getLocalidad(){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(latitudGlobal, longitudGlobal, 1);
            if(addresses != null) {
                if(addresses.get(0).getLocality() == null){
                    txtMunicipio.setText(addresses.get(0).getSubAdminArea());
                    Util.disableEditText(txtMunicipio);
                    txtMunicipio.setError(null);
                }else {
                    txtMunicipio.setText(addresses.get(0).getLocality());
                    Util.disableEditText(txtMunicipio);
                    txtMunicipio.setError(null);
                }
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            showDialog(RC_DIALOG_INFO);
            e.printStackTrace();

        }
    }

    private void getParaje(LatLng latLng){
        float[] distance = new float[2];

        parajes.addAll(DAOLocal.getAllParajes(context));

        for(int i=0; i<parajes.size(); i++){
            String latitudParaje = parajes.get(i).getLatitud();
            String longitudParaje = parajes.get(i).getLongitud();
            String radioParaje = parajes.get(i).getRadio();
            Location.distanceBetween(Double.parseDouble(latitudParaje), Double.parseDouble(longitudParaje),
                    latLng.latitude, latLng.longitude, distance);
            if( distance[0] > Integer.parseInt(radioParaje)){
                txtParaje.setText(getString(R.string.indeterminado));
                txtParaje.setError(null);
                Util.disableEditText(txtParaje);
            } else {
                txtParaje.setText(parajes.get(i).getNombre());
                txtParaje.setError(null);
                break;
            }

        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case RC_DIALOG_INFO:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.dialog_municipio_servicio));
                builder.setPositiveButton(getString(R.string.dialog_ok), new miCitaModificarActivity.OkOnClickListener());
                builder.setCancelable(false);
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            txtMunicipio.setInputType(InputType.TYPE_CLASS_TEXT);
            Util.textNoNull(txtMunicipio, context);
            dialog.cancel();
            municipios = getResources().getStringArray(R.array.municipios);
            Util.textWatcherMunicipio(txtMunicipio, municipios, context);
            ArrayAdapter<String> adaptador = new ArrayAdapter<>(context, R.layout.autocomplete_pajaros, municipios);
            txtMunicipio.setAdapter(adaptador);

            txtMunicipio.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    txtMunicipio.showDropDown();
                    return false;
                }
            });
        }
    }

    private boolean checkRegistro() {
        boolean check = true;
        if (txtEspecie.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorEspecie), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtMunicipio.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorMunicipio), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtParaje.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorParaje), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtNumero.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorNumero), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        if (txtObservaciones.getError() != null) {
            Snackbar.make(drawerLayout, getString(R.string.errorObservaciones), Snackbar.LENGTH_LONG).show();
            check = false;
        }
        return check;
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        super.finish();
    }

    @Override
    public void onBackPressed() {
        Intent intentoMiCita = new Intent(context, miCitaMainActivity.class);
        startActivity(intentoMiCita);
        finish();
    }
}
