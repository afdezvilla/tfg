package alvaro.vizzor.sql;


import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public class PopulatePajaros {

    public static void poblarPajaros(SQLiteDatabase db,String TABLE_PAJARO, String COLUMN_PAJARO_NOMBRE_CASTELLANO, String COLUMN_PAJARO_NOMBRE_CIENTIFICO) {

        ContentValues CisneNegro = new ContentValues();
        CisneNegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cisne Negro ");
        CisneNegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cygnus Atratus");

        ContentValues CisneVulgar = new ContentValues();
        CisneVulgar.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cisne Vulgar ");
        CisneVulgar.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cygnus Olor");

        ContentValues CisneChico = new ContentValues();
        CisneChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cisne Chico  ");
        CisneChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cygnus Columbianus");

        ContentValues CisneCantor = new ContentValues();
        CisneCantor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cisne Cantor  ");
        CisneCantor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cygnus Cygnus");

        ContentValues ÁnsarCampestre = new ContentValues();
        ÁnsarCampestre.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánsar Campestre  ");
        ÁnsarCampestre.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Fabalis");

        ContentValues ÁnsarPiquicorto = new ContentValues();
        ÁnsarPiquicorto.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánsar Piquicorto  ");
        ÁnsarPiquicorto.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Brachyrhynchus");

        ContentValues ÁnsarCareto = new ContentValues();
        ÁnsarCareto.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánsar Careto ");
        ÁnsarCareto.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Albifrons");

        ContentValues ÁnsarChico = new ContentValues();
        ÁnsarChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánsar Chico  ");
        ÁnsarChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Erythropus");

        ContentValues ÁnsarComún = new ContentValues();
        ÁnsarComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánsar Común  ");
        ÁnsarComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Anser");

        ContentValues ÁnsarIndio = new ContentValues();
        ÁnsarIndio.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánsar Indio  ");
        ÁnsarIndio.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Indicus");

        ContentValues BarnaclaCanadienseGrande = new ContentValues();
        BarnaclaCanadienseGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Barnacla Canadiense Grande ");
        BarnaclaCanadienseGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Branta Canadensis");

        ContentValues BarnaclaCariblanca = new ContentValues();
        BarnaclaCariblanca.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Barnacla Cariblanca ");
        BarnaclaCariblanca.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Branta Leucopsis");

        ContentValues BarnaclaCarinegra = new ContentValues();
        BarnaclaCarinegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Barnacla Carinegra  ");
        BarnaclaCarinegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Branta Bernicla");

        ContentValues BarnaclaCuellirroja = new ContentValues();
        BarnaclaCuellirroja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Barnacla Cuellirroja ");
        BarnaclaCuellirroja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Branta Ruficollis");

        ContentValues GansoDelNilo = new ContentValues();
        GansoDelNilo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ganso Del Nilo  ");
        GansoDelNilo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Alopochen Aegyptiaca");

        ContentValues TarroCanelo = new ContentValues();
        TarroCanelo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tarro Canelo  ");
        TarroCanelo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tadorna Ferruginea");

        ContentValues TarroBlanco = new ContentValues();
        TarroBlanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tarro Blanco ");
        TarroBlanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tadorna Tadorna");

        ContentValues PatoMandarín = new ContentValues();
        PatoMandarín.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pato Mandarín ");
        PatoMandarín.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aix Galericulata");

        ContentValues SilbónEuropeo = new ContentValues();
        SilbónEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Silbón Europeo ");
        SilbónEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Penelope");

        ContentValues SilbónAmericano = new ContentValues();
        SilbónAmericano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Silbón Americano  ");
        SilbónAmericano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Americana");

        ContentValues CercetaDeAlfanjes = new ContentValues();
        CercetaDeAlfanjes.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta De Alfanjes ");
        CercetaDeAlfanjes.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Falcata");

        ContentValues ÁnadeFriso = new ContentValues();
        ÁnadeFriso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánade Friso ");
        ÁnadeFriso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Strepera");

        ContentValues CercetaDelBaikal = new ContentValues();
        CercetaDelBaikal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Del Baikal ");
        CercetaDelBaikal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Formosa");

        ContentValues CercetaComún = new ContentValues();
        CercetaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Común ");
        CercetaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Crecca");

        ContentValues CercetaAmericana = new ContentValues();
        CercetaAmericana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Americana ");
        CercetaAmericana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Carolinensis");

        ContentValues ÁnadeAzulón = new ContentValues();
        ÁnadeAzulón.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánade Azulón");
        ÁnadeAzulón.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas");

        ContentValues ÁnadeSombrío = new ContentValues();
        ÁnadeSombrío.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánade Sombrío ");
        ÁnadeSombrío.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Rubripes");

        ContentValues ÁnadeRabudo = new ContentValues();
        ÁnadeRabudo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ánade Rabudo ");
        ÁnadeRabudo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Acuta");

        ContentValues CercetaCarretona = new ContentValues();
        CercetaCarretona.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Carretona ");
        CercetaCarretona.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Querquedula");

        ContentValues CercetaAliazul = new ContentValues();
        CercetaAliazul.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Aliazul  ");
        CercetaAliazul.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Discors");

        ContentValues CucharaComún = new ContentValues();
        CucharaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cuchara Común ");
        CucharaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Clypeata");

        ContentValues CercetaPardilla = new ContentValues();
        CercetaPardilla.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Pardilla");
        CercetaPardilla.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Marmaronetta");

        ContentValues PatoColorado = new ContentValues();
        PatoColorado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pato Colorado  ");
        PatoColorado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Netta Rufina");

        ContentValues PorrónEuropeo = new ContentValues();
        PorrónEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Europeo ");
        PorrónEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aythya Ferina");

        ContentValues PorrónAcollarado = new ContentValues();
        PorrónAcollarado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Acollarado");
        PorrónAcollarado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aythya");

        ContentValues PorrónPardo = new ContentValues();
        PorrónPardo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Pardo ");
        PorrónPardo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aythya Nyroca");

        ContentValues PorrónMoñudo = new ContentValues();
        PorrónMoñudo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Moñudo ");
        PorrónMoñudo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aythya Fuligula");

        ContentValues PorrónBastardo = new ContentValues();
        PorrónBastardo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Bastardo   ");
        PorrónBastardo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aythya Marila");

        ContentValues PorrónBola = new ContentValues();
        PorrónBola.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Bola");
        PorrónBola.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aythya");

        ContentValues EiderComún = new ContentValues();
        EiderComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Eider Común ");
        EiderComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Somateria Mollissima");

        ContentValues EiderReal = new ContentValues();
        EiderReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Eider Real ");
        EiderReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Somateria Spectabilis");

        ContentValues PatoHavelda = new ContentValues();
        PatoHavelda.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pato Havelda ");
        PatoHavelda.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Clangula Hyemalis");

        ContentValues NegrónComún = new ContentValues();
        NegrónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Negrón Común ");
        NegrónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Melanitta Nigra");

        ContentValues NegrónAmericano = new ContentValues();
        NegrónAmericano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Negrón Americano");
        NegrónAmericano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Melanitta Americana");

        ContentValues NegrónCareto = new ContentValues();
        NegrónCareto.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Negrón Careto");
        NegrónCareto.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Melanitta");

        ContentValues NegrónEspeculado = new ContentValues();
        NegrónEspeculado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Negrón Especulado ");
        NegrónEspeculado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Melanitta Fusca");

        ContentValues NegrónAliblanco = new ContentValues();
        NegrónAliblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Negrón Aliblanco");
        NegrónAliblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Melanitta");

        ContentValues PorrónAlbeola = new ContentValues();
        PorrónAlbeola.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Albeola ");
        PorrónAlbeola.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bucephala Albeola");

        ContentValues PorrónIslándico = new ContentValues();
        PorrónIslándico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Islándico  ");
        PorrónIslándico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bucephala Islandica");

        ContentValues PorrónOsculado = new ContentValues();
        PorrónOsculado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Porrón Osculado ");
        PorrónOsculado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bucephala Clangula");

        ContentValues SerretaChica = new ContentValues();
        SerretaChica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Serreta Chica ");
        SerretaChica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mergellus Albellus");

        ContentValues SerretaCapuchona = new ContentValues();
        SerretaCapuchona.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Serreta Capuchona ");
        SerretaCapuchona.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mergus Cucullatus");

        ContentValues SerretaMediana = new ContentValues();
        SerretaMediana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Serreta Mediana ");
        SerretaMediana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mergus Serrator");

        ContentValues SerretaGrande = new ContentValues();
        SerretaGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Serreta Grande  ");
        SerretaGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mergus Merganser");

        ContentValues MalvasíaCanela = new ContentValues();
        MalvasíaCanela.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Malvasía Canela ");
        MalvasíaCanela.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oxyura Jamaicensis");

        ContentValues MalvasíaCabeciblanca = new ContentValues();
        MalvasíaCabeciblanca.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Malvasía Cabeciblanca ");
        MalvasíaCabeciblanca.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oxyura Leucocephala");

        ContentValues GrévolComún = new ContentValues();
        GrévolComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Grévol Común ");
        GrévolComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tetrastes Bonasia");

        ContentValues LagópodoAlpino = new ContentValues();
        LagópodoAlpino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Lagópodo Alpino ");
        LagópodoAlpino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lagopus");

        ContentValues UrogalloComún = new ContentValues();
        UrogalloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Urogallo Común ");
        UrogalloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tetrao Urogallus");

        ContentValues PerdizRoja = new ContentValues();
        PerdizRoja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Perdiz Roja ");
        PerdizRoja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Alectoris Rufa");

        ContentValues PerdizMoruna = new ContentValues();
        PerdizMoruna.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Perdiz Moruna ");
        PerdizMoruna.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Alectoris Barbara");

        ContentValues PerdizPardilla = new ContentValues();
        PerdizPardilla.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Perdiz Pardilla ");
        PerdizPardilla.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Perdix Perdix");

        ContentValues CodornizComún = new ContentValues();
        CodornizComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Codorniz Común");
        CodornizComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Coturnix");

        ContentValues FaisánVulgar = new ContentValues();
        FaisánVulgar.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Faisán Vulgar ");
        FaisánVulgar.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phasianus Colchicus");

        ContentValues ColimboChico = new ContentValues();
        ColimboChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colimbo Chico ");
        ColimboChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gavia Stellata");

        ContentValues ColimboÁrtico = new ContentValues();
        ColimboÁrtico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colimbo Ártico ");
        ColimboÁrtico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gavia Arctica");

        ContentValues ColimboDelPacífico = new ContentValues();
        ColimboDelPacífico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colimbo Del Pacífico ");
        ColimboDelPacífico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gavia Pacifica");

        ContentValues ColimboGrande = new ContentValues();
        ColimboGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colimbo Grande ");
        ColimboGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gavia Immer");

        ContentValues ZampullínPicogrueso = new ContentValues();
        ZampullínPicogrueso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zampullín Picogrueso");
        ZampullínPicogrueso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Podilymbus");

        ContentValues ZampullínComún = new ContentValues();
        ZampullínComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zampullín Común ");
        ZampullínComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tachybaptus Ruficollis");

        ContentValues SomormujoLavanco = new ContentValues();
        SomormujoLavanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Somormujo Lavanco");
        SomormujoLavanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Podiceps Cristatus");

        ContentValues SomormujoCuellirrojo = new ContentValues();
        SomormujoCuellirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Somormujo Cuellirrojo ");
        SomormujoCuellirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Podiceps Grisegena");

        ContentValues ZampullínCuellirrojo = new ContentValues();
        ZampullínCuellirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zampullín Cuellirrojo ");
        ZampullínCuellirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Podiceps Auritus");

        ContentValues ZampullínCuellinegro = new ContentValues();
        ZampullínCuellinegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zampullín Cuellinegro ");
        ZampullínCuellinegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Podiceps Nigricollis");

        ContentValues AlbatrosOjeroso = new ContentValues();
        AlbatrosOjeroso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Albatros Ojeroso ");
        AlbatrosOjeroso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Thalassarche Melanophris");

        ContentValues FulmarBoreal = new ContentValues();
        FulmarBoreal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Fulmar Boreal ");
        FulmarBoreal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fulmarus Glacialis");

        ContentValues PetrelDeElCabo = new ContentValues();
        PetrelDeElCabo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Petrel De El Cabo  ");
        PetrelDeElCabo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Daption Capense");

        ContentValues PetrelAntillano = new ContentValues();
        PetrelAntillano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Petrel Antillano  ");
        PetrelAntillano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pterodroma Hasitata");

        ContentValues PetrelDeBulwer = new ContentValues();
        PetrelDeBulwer.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Petrel De Bulwer ");
        PetrelDeBulwer.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bulweria Bulwerii");

        ContentValues PardelaCenicienta = new ContentValues();
        PardelaCenicienta.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Cenicienta  ");
        PardelaCenicienta.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calonectris Diomedea");

        ContentValues PardelaDeCaboVerde = new ContentValues();
        PardelaDeCaboVerde.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela De Cabo Verde ");
        PardelaDeCaboVerde.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calonectris Edwarsii");

        ContentValues PardelaCapirotada = new ContentValues();
        PardelaCapirotada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Capirotada");
        PardelaCapirotada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Puffinus");

        ContentValues PardelaSombría = new ContentValues();
        PardelaSombría.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Sombría  ");
        PardelaSombría.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Puffinus Griseus");

        ContentValues PardelaPichoneta = new ContentValues();
        PardelaPichoneta.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Pichoneta ");
        PardelaPichoneta.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Puffinus Puffinus");

        ContentValues PardelaMediterránea = new ContentValues();
        PardelaMediterránea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Mediterránea ");
        PardelaMediterránea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Puffinus Yelkouan");

        ContentValues PardelaBalear = new ContentValues();
        PardelaBalear.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Balear  ");
        PardelaBalear.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Puffinus Mauretanicus");

        ContentValues PardelaChica = new ContentValues();
        PardelaChica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardela Chica ");
        PardelaChica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Puffinus Baroli");

        ContentValues PaíñoDeWilson = new ContentValues();
        PaíñoDeWilson.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño De Wilson  ");
        PaíñoDeWilson.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oceanites Oceanicus");

        ContentValues PaíñoPechialbo = new ContentValues();
        PaíñoPechialbo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño Pechialbo ");
        PaíñoPechialbo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pelagodroma Marina");

        ContentValues PaíñoVentrinegro = new ContentValues();
        PaíñoVentrinegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño Ventrinegro ");
        PaíñoVentrinegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fregetta Tropica");

        ContentValues PaíñoEuropeo = new ContentValues();
        PaíñoEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño Europeo ");
        PaíñoEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Hydrobates");

        ContentValues PaíñoBoreal = new ContentValues();
        PaíñoBoreal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño Boreal ");
        PaíñoBoreal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oceanodroma Leucorhoa");

        ContentValues PaíñoDeSwinhoe = new ContentValues();
        PaíñoDeSwinhoe.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño De Swinhoe ");
        PaíñoDeSwinhoe.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oceanodroma Monorhis");

        ContentValues PaíñoDeMadeira = new ContentValues();
        PaíñoDeMadeira.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paíño De Madeira  ");
        PaíñoDeMadeira.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oceanodroma Castro");

        ContentValues RabijuncoEtéreo = new ContentValues();
        RabijuncoEtéreo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Rabijunco Etéreo ");
        RabijuncoEtéreo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phaethon Aethereus");

        ContentValues PiqueroPatirrojo = new ContentValues();
        PiqueroPatirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Piquero Patirrojo");
        PiqueroPatirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sula");

        ContentValues PiqueroEnmascarado = new ContentValues();
        PiqueroEnmascarado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Piquero Enmascarado ");
        PiqueroEnmascarado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sula Dactylatra");

        ContentValues PiqueroPardo = new ContentValues();
        PiqueroPardo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Piquero Pardo  ");
        PiqueroPardo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sula Leucogaster");

        ContentValues AlcatrazAtlántico = new ContentValues();
        AlcatrazAtlántico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcatraz Atlántico ");
        AlcatrazAtlántico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Morus Bassanus");

        ContentValues CormoránGrande = new ContentValues();
        CormoránGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cormorán Grande  ");
        CormoránGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phalacrocorax Carbo");

        ContentValues CormoránMoñudoS = new ContentValues();
        CormoránMoñudoS.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cormorán Moñudo S ");
        CormoránMoñudoS.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phalacrocorax Aristoteli");

        ContentValues PelícanoComún = new ContentValues();
        PelícanoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pelícano Común  ");
        PelícanoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pelecanus Onocrotalus");

        ContentValues PelícanoRosado = new ContentValues();
        PelícanoRosado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pelícano Rosado ");
        PelícanoRosado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pelecanus Rufescens");

        ContentValues RabihorcadoMagnífico = new ContentValues();
        RabihorcadoMagnífico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Rabihorcado Magnífico ");
        RabihorcadoMagnífico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fregata Magnificens");

        ContentValues AvetoroComún = new ContentValues();
        AvetoroComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avetoro Común ");
        AvetoroComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Botaurus Stellaris");

        ContentValues AvetoroLentiginoso = new ContentValues();
        AvetoroLentiginoso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avetoro Lentiginoso  ");
        AvetoroLentiginoso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Botaurus Lentiginosus");

        ContentValues AvetorilloComún = new ContentValues();
        AvetorilloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avetorillo Común ");
        AvetorilloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ixobrychus Minutus");

        ContentValues AvetorilloPlomizo = new ContentValues();
        AvetorilloPlomizo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avetorillo Plomizo ");
        AvetorilloPlomizo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ixobrychus Sturmii");

        ContentValues MartineteComún = new ContentValues();
        MartineteComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Martinete Común  ");
        MartineteComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Nycticorax Nycticorax");

        ContentValues GarcitaVerdosaS = new ContentValues();
        GarcitaVerdosaS.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garcita Verdosa S ");
        GarcitaVerdosaS.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Butorides Virescen");

        ContentValues GarcillaCangrejera = new ContentValues();
        GarcillaCangrejera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garcilla Cangrejera ");
        GarcillaCangrejera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ardeola Ralloides");

        ContentValues GarcillaBueyera = new ContentValues();
        GarcillaBueyera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garcilla Bueyera  ");
        GarcillaBueyera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bubulcus Ibis");

        ContentValues GarcetaTricolorr = new ContentValues();
        GarcetaTricolorr.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garceta Tricolorr ");
        GarcetaTricolorr.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Egretta Tricolo");

        ContentValues GarcetaDimorfa = new ContentValues();
        GarcetaDimorfa.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garceta Dimorfa  ");
        GarcetaDimorfa.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Egretta Gularis");

        ContentValues GarcetaComún = new ContentValues();
        GarcetaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garceta Común ");
        GarcetaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Egretta Garzetta");

        ContentValues GarcetaGrande = new ContentValues();
        GarcetaGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garceta Grande");
        GarcetaGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Egretta Alba");

        ContentValues GarzaReal = new ContentValues();
        GarzaReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garza Real ");
        GarzaReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ardea Cinerea");

        ContentValues GarzaAzulada = new ContentValues();
        GarzaAzulada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garza Azulada ");
        GarzaAzulada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ardea Herodias");

        ContentValues GarzaImperial = new ContentValues();
        GarzaImperial.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Garza Imperial ");
        GarzaImperial.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ardea Purpurea");

        ContentValues CigüeñaNegra = new ContentValues();
        CigüeñaNegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cigüeña Negra ");
        CigüeñaNegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ciconia Nigra");

        ContentValues CigüeñaBlanca = new ContentValues();
        CigüeñaBlanca.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cigüeña Blanca  ");
        CigüeñaBlanca.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ciconia Ciconia");

        ContentValues MarabúAfricano = new ContentValues();
        MarabúAfricano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Marabú Africano ");
        MarabúAfricano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Leptoptilos Crumenifer");

        ContentValues MoritoComún = new ContentValues();
        MoritoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Morito Común");
        MoritoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Plegadis");

        ContentValues IbisEremita = new ContentValues();
        IbisEremita.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ibis Eremita ");
        IbisEremita.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Geronticus Eremita");

        ContentValues IbisSagrado = new ContentValues();
        IbisSagrado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ibis Sagrado ");
        IbisSagrado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Threskiornis Aethiopicus");

        ContentValues EspátulaComún = new ContentValues();
        EspátulaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Espátula Común ");
        EspátulaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Platalea Leucorodia");

        ContentValues FlamencoComún = new ContentValues();
        FlamencoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Flamenco Común ");
        FlamencoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phoenicopterus Roseus");

        ContentValues FlamencoEnano = new ContentValues();
        FlamencoEnano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Flamenco Enano ");
        FlamencoEnano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phoenicopterus Minor");

        ContentValues AbejeroEuropeo = new ContentValues();
        AbejeroEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Abejero Europeo ");
        AbejeroEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pernis Apivorus");

        ContentValues ElanioTijereta = new ContentValues();
        ElanioTijereta.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Elanio Tijereta");
        ElanioTijereta.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Elanoides");

        ContentValues ElanioComún = new ContentValues();
        ElanioComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Elanio Común ");
        ElanioComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Elanus Caeruleus");

        ContentValues MilanoNegro = new ContentValues();
        MilanoNegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Milano Negro ");
        MilanoNegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Milvus Migrans");

        ContentValues MilanoReal = new ContentValues();
        MilanoReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Milano Real  ");
        MilanoReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Milvus Milvus");

        ContentValues PigargoEuropeo = new ContentValues();
        PigargoEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pigargo Europeo ");
        PigargoEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Haliaeetus Albicilla");

        ContentValues Quebrantahuesos = new ContentValues();
        Quebrantahuesos.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Quebrantahuesos ");
        Quebrantahuesos.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gypaetus Barbatus");

        ContentValues AlimocheComún = new ContentValues();
        AlimocheComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alimoche Común ");
        AlimocheComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Neophron Percnopterus");

        ContentValues BuitreDorsiblancoAfricano = new ContentValues();
        BuitreDorsiblancoAfricano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buitre Dorsiblanco Africano  ");
        BuitreDorsiblancoAfricano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gyps Africanus");

        ContentValues BuitreLeonado = new ContentValues();
        BuitreLeonado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buitre Leonado ");
        BuitreLeonado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gyps Fulvus");

        ContentValues BuitreMoteado = new ContentValues();
        BuitreMoteado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buitre Moteado ");
        BuitreMoteado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gyps Rueppellii");

        ContentValues BuitreOrejudo = new ContentValues();
        BuitreOrejudo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buitre Orejudo ");
        BuitreOrejudo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Torgos Tracheliotus");

        ContentValues BuitreNegro = new ContentValues();
        BuitreNegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buitre Negro ");
        BuitreNegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aegypius Monachus");

        ContentValues CulebreraEuropea = new ContentValues();
        CulebreraEuropea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Culebrera Europea ");
        CulebreraEuropea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Circaetus Gallicus");

        ContentValues AguiluchoLaguneroOccidental = new ContentValues();
        AguiluchoLaguneroOccidental.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Aguilucho Lagunero Occidental ");
        AguiluchoLaguneroOccidental.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Circus Aeruginosus");

        ContentValues AguiluchoPálido = new ContentValues();
        AguiluchoPálido.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Aguilucho Pálido  ");
        AguiluchoPálido.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Circus Cyaneus");

        ContentValues AguiluchoPapialbo = new ContentValues();
        AguiluchoPapialbo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Aguilucho Papialbo ");
        AguiluchoPapialbo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Circus Macrourus");

        ContentValues AguiluchoCenizo = new ContentValues();
        AguiluchoCenizo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Aguilucho Cenizo");
        AguiluchoCenizo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Circus");

        ContentValues AzorComún = new ContentValues();
        AzorComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Azor Común ");
        AzorComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Accipiter Gentilis");

        ContentValues GavilánComún = new ContentValues();
        GavilánComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gavilán Común ");
        GavilánComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Accipiter Nisus");

        ContentValues BusardoRatonero = new ContentValues();
        BusardoRatonero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Busardo Ratonero ");
        BusardoRatonero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Buteo Buteo");

        ContentValues BusardoMoro = new ContentValues();
        BusardoMoro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Busardo Moro ");
        BusardoMoro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Buteo Rufinus");

        ContentValues BusardoCalzado = new ContentValues();
        BusardoCalzado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Busardo Calzado ");
        BusardoCalzado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Buteo Lagopus");

        ContentValues ÁguilaPomerana = new ContentValues();
        ÁguilaPomerana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Pomerana  ");
        ÁguilaPomerana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Pomarina");

        ContentValues ÁguilaMoteada = new ContentValues();
        ÁguilaMoteada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Moteada ");
        ÁguilaMoteada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Clanga");

        ContentValues ÁguilaEsteparia = new ContentValues();
        ÁguilaEsteparia.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Esteparia ");
        ÁguilaEsteparia.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Nipalensis");

        ContentValues ÁguilaImperialIbérica = new ContentValues();
        ÁguilaImperialIbérica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Imperial Ibérica ");
        ÁguilaImperialIbérica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Adalberti");

        ContentValues ÁguilaImperialOriental = new ContentValues();
        ÁguilaImperialOriental.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Imperial Oriental ");
        ÁguilaImperialOriental.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Heliaca");

        ContentValues ÁguilaReal = new ContentValues();
        ÁguilaReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Real");
        ÁguilaReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila");

        ContentValues ÁguilaCalzada = new ContentValues();
        ÁguilaCalzada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Calzada ");
        ÁguilaCalzada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Pennata");

        ContentValues ÁguilaPerdicera = new ContentValues();
        ÁguilaPerdicera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Perdicera ");
        ÁguilaPerdicera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aquila Fasciata");

        ContentValues ÁguilaPescadora = new ContentValues();
        ÁguilaPescadora.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Águila Pescadora");
        ÁguilaPescadora.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pandion");

        ContentValues CernícaloPrimilla = new ContentValues();
        CernícaloPrimilla.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cernícalo Primilla ");
        CernícaloPrimilla.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Naumanni");

        ContentValues CernícaloVulgar = new ContentValues();
        CernícaloVulgar.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cernícalo Vulgar ");
        CernícaloVulgar.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Tinnunculus");

        ContentValues CernícaloPatirrojo = new ContentValues();
        CernícaloPatirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cernícalo Patirrojo");
        CernícaloPatirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco");

        ContentValues Esmerejón = new ContentValues();
        Esmerejón.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Esmerejón");
        Esmerejón.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco");

        ContentValues AlcotánEuropeo = new ContentValues();
        AlcotánEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcotán Europeo ");
        AlcotánEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Subbuteo");

        ContentValues HalcónDeEleonora = new ContentValues();
        HalcónDeEleonora.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Halcón De Eleonora ");
        HalcónDeEleonora.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Eleonorae");

        ContentValues HalcónBorní = new ContentValues();
        HalcónBorní.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Halcón Borní ");
        HalcónBorní.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Biarmicus");

        ContentValues HalcónSacre = new ContentValues();
        HalcónSacre.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Halcón Sacre");
        HalcónSacre.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco");

        ContentValues HalcónGerifalte = new ContentValues();
        HalcónGerifalte.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Halcón Gerifalte ");
        HalcónGerifalte.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Rusticolus");

        ContentValues HalcónPeregrino = new ContentValues();
        HalcónPeregrino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Halcón Peregrino ");
        HalcónPeregrino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Peregrinus");

        ContentValues HalcónTagarote = new ContentValues();
        HalcónTagarote.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Halcón Tagarote ");
        HalcónTagarote.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Falco Pelegrinoides");

        ContentValues RascónEuropeo = new ContentValues();
        RascónEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Rascón Europeo ");
        RascónEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Rallus Aquaticus");

        ContentValues PolluelaPintoja = new ContentValues();
        PolluelaPintoja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Polluela Pintoja ");
        PolluelaPintoja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porzana Porzana");

        ContentValues PolluelaSora = new ContentValues();
        PolluelaSora.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Polluela Sora ");
        PolluelaSora.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porzana Carolina");

        ContentValues PolluelaBastarda = new ContentValues();
        PolluelaBastarda.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Polluela Bastarda");
        PolluelaBastarda.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porzana");

        ContentValues PolluelaChica = new ContentValues();
        PolluelaChica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Polluela Chica ");
        PolluelaChica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porzana Pusilla");

        ContentValues PolluelaCulirroja = new ContentValues();
        PolluelaCulirroja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Polluela Culirroja");
        PolluelaCulirroja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porzana");

        ContentValues GuiónAfricano = new ContentValues();
        GuiónAfricano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Guión Africano ");
        GuiónAfricano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Crex Egregia");

        ContentValues GuiónDeCodornices = new ContentValues();
        GuiónDeCodornices.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Guión De Codornices");
        GuiónDeCodornices.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Crex");

        ContentValues GallinetaComún = new ContentValues();
        GallinetaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gallineta Común ");
        GallinetaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gallinula Chloropus");

        ContentValues GallinetaChica = new ContentValues();
        GallinetaChica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gallineta Chica ");
        GallinetaChica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gallinula Angulata");

        ContentValues CalamoncilloAfricano = new ContentValues();
        CalamoncilloAfricano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Calamoncillo Africano ");
        CalamoncilloAfricano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porphyrio Alleni");

        ContentValues CalamoncilloAmericano = new ContentValues();
        CalamoncilloAmericano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Calamoncillo Americano ");
        CalamoncilloAmericano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porphyrio Martinicus");

        ContentValues CalamónComún = new ContentValues();
        CalamónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Calamón Común ");
        CalamónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Porphyrio Porphyrio");

        ContentValues FochaComún = new ContentValues();
        FochaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Focha Común ");
        FochaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fulica Atra");

        ContentValues FochaAmericana = new ContentValues();
        FochaAmericana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Focha Americana ");
        FochaAmericana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fulica Americana");

        ContentValues FochaMoruna = new ContentValues();
        FochaMoruna.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Focha Moruna");
        FochaMoruna.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fulica");

        ContentValues TorilloAndaluz = new ContentValues();
        TorilloAndaluz.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Torillo Andaluz");
        TorilloAndaluz.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turnix");

        ContentValues GrullaComún = new ContentValues();
        GrullaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Grulla Común");
        GrullaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Grus Grus");

        ContentValues GrullaCanadiense = new ContentValues();
        GrullaCanadiense.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Grulla Canadiense ");
        GrullaCanadiense.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Grus Canadensis");

        ContentValues GrullaDamisela = new ContentValues();
        GrullaDamisela.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Grulla Damisela ");
        GrullaDamisela.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Grus Virgo");

        ContentValues SisónComún = new ContentValues();
        SisónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Sisón Común  ");
        SisónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tetrax Tetrax");

        ContentValues AvutardaHubara = new ContentValues();
        AvutardaHubara.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avutarda Hubara ");
        AvutardaHubara.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chlamydotis Undulata");

        ContentValues AvutardaComún = new ContentValues();
        AvutardaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avutarda Común ");
        AvutardaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Otis Tarda");

        ContentValues OstreroEuroasiático = new ContentValues();
        OstreroEuroasiático.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ostrero Euroasiático  ");
        OstreroEuroasiático.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Haematopus Ostralegus");

        ContentValues OstreroNegroCanario = new ContentValues();
        OstreroNegroCanario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ostrero Negro Canario ");
        OstreroNegroCanario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Haematopus Moquini");

        ContentValues CigüeñuelaComún = new ContentValues();
        CigüeñuelaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cigüeñuela Común ");
        CigüeñuelaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Himantopus Himantopus");

        ContentValues AvocetaComún = new ContentValues();
        AvocetaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avoceta Común ");
        AvocetaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Recurvirostra Avosetta");

        ContentValues AlcaravánComún = new ContentValues();
        AlcaravánComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaraván  Común ");
        AlcaravánComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Burhinus Oedicnemus");

        ContentValues Pluvial = new ContentValues();
        Pluvial.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pluvial ");
        Pluvial.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pluvianus Aegyptius");

        ContentValues CorredorSahariano = new ContentValues();
        CorredorSahariano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Corredor Sahariano ");
        CorredorSahariano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cursorius Cursor");

        ContentValues CanasteraComún = new ContentValues();
        CanasteraComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Canastera Común ");
        CanasteraComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Glareola Pratincola");

        ContentValues CanasteraAlinegra = new ContentValues();
        CanasteraAlinegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Canastera Alinegra ");
        CanasteraAlinegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Glareola Nordmanni");

        ContentValues ChorlitejoChico = new ContentValues();
        ChorlitejoChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Chico ");
        ChorlitejoChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Dubius");

        ContentValues ChorlitejoGrande = new ContentValues();
        ChorlitejoGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Grande ");
        ChorlitejoGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Hiaticula");

        ContentValues ChorlitejoSemipalmeado = new ContentValues();
        ChorlitejoSemipalmeado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Semipalmeado ");
        ChorlitejoSemipalmeado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Semipalmatus");

        ContentValues ChorlitejoCulirrojo = new ContentValues();
        ChorlitejoCulirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Culirrojo ");
        ChorlitejoCulirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Vociferus");

        ContentValues ChorlitejoPecuario = new ContentValues();
        ChorlitejoPecuario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Pecuario  ");
        ChorlitejoPecuario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Pecuarius");

        ContentValues ChorlitejoPatinegro = new ContentValues();
        ChorlitejoPatinegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Patinegro ");
        ChorlitejoPatinegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Alexandrinus");

        ContentValues ChorlitejoMongolChico = new ContentValues();
        ChorlitejoMongolChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Mongol Chico");
        ChorlitejoMongolChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius");

        ContentValues ChorlitejoMongolGrande = new ContentValues();
        ChorlitejoMongolGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlitejo Mongol Grande ");
        ChorlitejoMongolGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Leschenaultii");

        ContentValues ChorlitoCarambolo = new ContentValues();
        ChorlitoCarambolo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlito Carambolo ");
        ChorlitoCarambolo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Charadrius Morinellus");

        ContentValues ChorlitoDoradoSiberiano = new ContentValues();
        ChorlitoDoradoSiberiano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlito Dorado Siberiano  ");
        ChorlitoDoradoSiberiano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pluvialis Fulva");

        ContentValues ChorlitoDoradoAmericano = new ContentValues();
        ChorlitoDoradoAmericano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlito Dorado Americano ");
        ChorlitoDoradoAmericano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pluvialis");

        ContentValues ChorlitoDoradoEuropeo = new ContentValues();
        ChorlitoDoradoEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlito Dorado Europeo ");
        ChorlitoDoradoEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pluvialis Apricaria");

        ContentValues ChorlitoGris = new ContentValues();
        ChorlitoGris.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chorlito Gris ");
        ChorlitoGris.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pluvialis Squatarola");

        ContentValues AvefríaSociable = new ContentValues();
        AvefríaSociable.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avefría Sociable ");
        AvefríaSociable.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Vanellus Gregarius");

        ContentValues AvefríaColiblanca = new ContentValues();
        AvefríaColiblanca.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avefría Coliblanca ");
        AvefríaColiblanca.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Vanellus Leucurus");

        ContentValues AvefríaEuropea = new ContentValues();
        AvefríaEuropea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avefría Europea  ");
        AvefríaEuropea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Vanellus Vanellus");

        ContentValues CorrelimosGrande = new ContentValues();
        CorrelimosGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Grande   ");
        CorrelimosGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Tenuirostris");

        ContentValues CorrelimosGordo = new ContentValues();
        CorrelimosGordo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Gordo");
        CorrelimosGordo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Canutus");

        ContentValues CorrelimosTridáctilo = new ContentValues();
        CorrelimosTridáctilo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Tridáctilo ");
        CorrelimosTridáctilo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Alba");

        ContentValues CorrelimosSemipalmeado = new ContentValues();
        CorrelimosSemipalmeado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Semipalmeado ");
        CorrelimosSemipalmeado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Pusilla");

        ContentValues CorrelimosDeAlaska = new ContentValues();
        CorrelimosDeAlaska.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos De Alaska ");
        CorrelimosDeAlaska.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Mauri");

        ContentValues CorrelimosMenudo = new ContentValues();
        CorrelimosMenudo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Menudo ");
        CorrelimosMenudo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Minuta");

        ContentValues CorrelimosDeTemminck = new ContentValues();
        CorrelimosDeTemminck.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos De Temminck ");
        CorrelimosDeTemminck.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Temminckii");

        ContentValues CorrelimosMenudillo = new ContentValues();
        CorrelimosMenudillo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Menudillo  ");
        CorrelimosMenudillo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Minutilla");

        ContentValues CorrelimosCuliblanco = new ContentValues();
        CorrelimosCuliblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Culiblanco ");
        CorrelimosCuliblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Fuscicollis");

        ContentValues CorrelimosDeBairdi = new ContentValues();
        CorrelimosDeBairdi.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos De Bairdi ");
        CorrelimosDeBairdi.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris");

        ContentValues CorrelimosPectoral = new ContentValues();
        CorrelimosPectoral.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Pectoral ");
        CorrelimosPectoral.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Melanotos");

        ContentValues CorrelimosAcuminado = new ContentValues();
        CorrelimosAcuminado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Acuminado  ");
        CorrelimosAcuminado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Acuminata");

        ContentValues CorrelimosZarapitín = new ContentValues();
        CorrelimosZarapitín.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Zarapitín  ");
        CorrelimosZarapitín.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Ferruginea");

        ContentValues CorrelimosOscuro = new ContentValues();
        CorrelimosOscuro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Oscuro ");
        CorrelimosOscuro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Maritima");

        ContentValues CorrelimosComún = new ContentValues();
        CorrelimosComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Común ");
        CorrelimosComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Alpina");

        ContentValues CorrelimosZancolín = new ContentValues();
        CorrelimosZancolín.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Zancolín");
        CorrelimosZancolín.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calidris Himantopus");

        ContentValues CorrelimosFalcinelo = new ContentValues();
        CorrelimosFalcinelo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Falcinelo ");
        CorrelimosFalcinelo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Limicola Falcinellus");

        ContentValues CorrelimosCanelo = new ContentValues();
        CorrelimosCanelo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Canelo ");
        CorrelimosCanelo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tryngites Subruficollis");

        ContentValues Combatiente = new ContentValues();
        Combatiente.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Combatiente ");
        Combatiente.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Philomachus Pugnax");

        ContentValues AgachadizaChica = new ContentValues();
        AgachadizaChica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Agachadiza Chica");
        AgachadizaChica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lymnocryptes");

        ContentValues AgachadizaComún = new ContentValues();
        AgachadizaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Agachadiza Común ");
        AgachadizaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gallinago Gallinago");

        ContentValues AgachadizaReal = new ContentValues();
        AgachadizaReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Agachadiza Real ");
        AgachadizaReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gallinago Media");

        ContentValues AgujetaEscolopácea = new ContentValues();
        AgujetaEscolopácea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Agujeta Escolopácea ");
        AgujetaEscolopácea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Limnodromus Scolopaceus");

        ContentValues ChochaPerdiz = new ContentValues();
        ChochaPerdiz.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chocha Perdiz");
        ChochaPerdiz.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Scolopax");

        ContentValues AgujaColinegra = new ContentValues();
        AgujaColinegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Aguja Colinegra ");
        AgujaColinegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Limosa Limosa");

        ContentValues AgujaColipinta = new ContentValues();
        AgujaColipinta.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Aguja Colipinta ");
        AgujaColipinta.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Limosa Lapponica");

        ContentValues ZarapitoDeHudson = new ContentValues();
        ZarapitoDeHudson.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarapito De Hudson ");
        ZarapitoDeHudson.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Numenius Hudsonicus");

        ContentValues ZarapitoTrinador = new ContentValues();
        ZarapitoTrinador.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarapito Trinador  ");
        ZarapitoTrinador.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Numenius Phaeopus");

        ContentValues ZarapitoFino = new ContentValues();
        ZarapitoFino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarapito Fino");
        ZarapitoFino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Numenius Tenuirostris");

        ContentValues ZarapitoReal = new ContentValues();
        ZarapitoReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarapito Real ");
        ZarapitoReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Numenius Arquata");

        ContentValues CorrelimosBatitú = new ContentValues();
        CorrelimosBatitú.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Correlimos Batitú ");
        CorrelimosBatitú.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bartramia Longicauda");

        ContentValues AndarríosDelTerek = new ContentValues();
        AndarríosDelTerek.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Andarríos Del Terek ");
        AndarríosDelTerek.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Xenus Cinereus");

        ContentValues AndarríosChico = new ContentValues();
        AndarríosChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Andarríos Chico ");
        AndarríosChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Actitis Hypoleucos");

        ContentValues AndarríosMaculado = new ContentValues();
        AndarríosMaculado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Andarríos Maculado ");
        AndarríosMaculado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Actitis Macularius");

        ContentValues AndarríosGrande = new ContentValues();
        AndarríosGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Andarríos Grande ");
        AndarríosGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa Ochropus");

        ContentValues AndarríosSolitario = new ContentValues();
        AndarríosSolitario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Andarríos Solitario");
        AndarríosSolitario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa");

        ContentValues ArchibebeOscuro = new ContentValues();
        ArchibebeOscuro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Archibebe Oscuro ");
        ArchibebeOscuro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa");

        ContentValues ArchibebePatigualdoGrande = new ContentValues();
        ArchibebePatigualdoGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Archibebe Patigualdo Grande ");
        ArchibebePatigualdoGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa Melanoleuca");

        ContentValues ArchibebeClaro = new ContentValues();
        ArchibebeClaro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Archibebe Claro");
        ArchibebeClaro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa");

        ContentValues ArchibebePatigualdoChico = new ContentValues();
        ArchibebePatigualdoChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Archibebe Patigualdo Chico ");
        ArchibebePatigualdoChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa Flavipes");

        ContentValues ArchibebeFino = new ContentValues();
        ArchibebeFino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Archibebe Fino   ");
        ArchibebeFino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa Stagnatilis");

        ContentValues AndarríosBastardo = new ContentValues();
        AndarríosBastardo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Andarríos Bastardo");
        AndarríosBastardo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa Glareola");

        ContentValues ArchibebeComún = new ContentValues();
        ArchibebeComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Archibebe Común ");
        ArchibebeComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tringa Totanus");

        ContentValues VuelvepiedrasComún = new ContentValues();
        VuelvepiedrasComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vuelvepiedras Común ");
        VuelvepiedrasComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Arenaria Interpres");

        ContentValues FalaropoTricolor = new ContentValues();
        FalaropoTricolor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Falaropo Tricolor ");
        FalaropoTricolor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phalaropus ");

        ContentValues FalaropoPicofino = new ContentValues();
        FalaropoPicofino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Falaropo Picofino  ");
        FalaropoPicofino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phalaropus Lobatus");

        ContentValues FalaropoPicogrueso = new ContentValues();
        FalaropoPicogrueso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Falaropo Picogrueso ");
        FalaropoPicogrueso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phalaropus Fulicarius");

        ContentValues PágaloParasito = new ContentValues();
        PágaloParasito.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Págalo Parasito ");
        PágaloParasito.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Stercorarius Parasiticus");

        ContentValues PágaloRabero = new ContentValues();
        PágaloRabero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Págalo Rabero ");
        PágaloRabero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Stercorarius Longicaudus");

        ContentValues PágaloGrande = new ContentValues();
        PágaloGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Págalo Grande ");
        PágaloGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Stercorarius Skua");

        ContentValues PágaloPolar = new ContentValues();
        PágaloPolar.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Págalo Polar ");
        PágaloPolar.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Stercorarius");

        ContentValues GaviotaDeBonaparte = new ContentValues();
        GaviotaDeBonaparte.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota De Bonaparte ");
        GaviotaDeBonaparte.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chroicocephalus Philadelphia");

        ContentValues GaviotaCabecigrís = new ContentValues();
        GaviotaCabecigrís.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Cabecigrís ");
        GaviotaCabecigrís.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chroicocephalus Cirrocephalus");

        ContentValues GaviotaPicofina = new ContentValues();
        GaviotaPicofina.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Picofina ");
        GaviotaPicofina.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chroicocephalus Genei");

        ContentValues GaviotaCabecinegra = new ContentValues();
        GaviotaCabecinegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Cabecinegra  ");
        GaviotaCabecinegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Melanocephalus");

        ContentValues GaviotaGuanaguanare = new ContentValues();
        GaviotaGuanaguanare.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Guanaguanare  ");
        GaviotaGuanaguanare.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Atricilla");

        ContentValues GaviotaPipizcan = new ContentValues();
        GaviotaPipizcan.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Pipizcan ");
        GaviotaPipizcan.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Pipixcan");

        ContentValues GaviotaDeAudouin = new ContentValues();
        GaviotaDeAudouin.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota De Audouin ");
        GaviotaDeAudouin.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Audouinii");

        ContentValues GaviotaDeDelaware = new ContentValues();
        GaviotaDeDelaware.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota De Delaware ");
        GaviotaDeDelaware.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Delawarensis");

        ContentValues GaviotaCana = new ContentValues();
        GaviotaCana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Cana ");
        GaviotaCana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Canus");

        ContentValues GaviotaSombría = new ContentValues();
        GaviotaSombría.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Sombría ");
        GaviotaSombría.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Fuscus");

        ContentValues GaviotaArgénteaEuropea = new ContentValues();
        GaviotaArgénteaEuropea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Argéntea Europea  ");
        GaviotaArgénteaEuropea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Argentatus");

        ContentValues GaviotaArgénteaAmericana = new ContentValues();
        GaviotaArgénteaAmericana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Argéntea Americana ");
        GaviotaArgénteaAmericana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus");

        ContentValues GaviotaDelCaspio = new ContentValues();
        GaviotaDelCaspio.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Del Caspio ");
        GaviotaDelCaspio.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Cachinnans");

        ContentValues GaviotaPatiamarilla = new ContentValues();
        GaviotaPatiamarilla.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Patiamarilla ");
        GaviotaPatiamarilla.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Michahellis");

        ContentValues GaviotaDeBering = new ContentValues();
        GaviotaDeBering.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota De Bering ");
        GaviotaDeBering.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Glaucescens");

        ContentValues GaviotaGroenlandesa = new ContentValues();
        GaviotaGroenlandesa.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Groenlandesa ");
        GaviotaGroenlandesa.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Glaucoides");

        ContentValues GaviónHiperbóreo = new ContentValues();
        GaviónHiperbóreo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gavión Hiperbóreo ");
        GaviónHiperbóreo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Hyperboreus");

        ContentValues GaviónAtlántico = new ContentValues();
        GaviónAtlántico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gavión Atlántico ");
        GaviónAtlántico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Marinus");

        ContentValues GaviotaCocinera = new ContentValues();
        GaviotaCocinera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Cocinera ");
        GaviotaCocinera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larus Dominicanus");

        ContentValues GaviotaRosada = new ContentValues();
        GaviotaRosada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Rosada ");
        GaviotaRosada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Rhodostethia Rosea");

        ContentValues GaviotaTridáctila = new ContentValues();
        GaviotaTridáctila.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Tridáctila ");
        GaviotaTridáctila.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Rissa Tridactyla");

        ContentValues GaviotaDeSabine = new ContentValues();
        GaviotaDeSabine.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota De Sabine");
        GaviotaDeSabine.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Xema");

        ContentValues GaviotaEnana = new ContentValues();
        GaviotaEnana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gaviota Enana");
        GaviotaEnana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Hydrocoloeus");

        ContentValues PagazaPiconegra = new ContentValues();
        PagazaPiconegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pagaza Piconegra ");
        PagazaPiconegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Gelochelidon Nilotica");

        ContentValues PagazaPiquirroja = new ContentValues();
        PagazaPiquirroja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pagaza Piquirroja ");
        PagazaPiquirroja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Hydroprogne Caspia");

        ContentValues CharránBengalí = new ContentValues();
        CharránBengalí.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Bengalí ");
        CharránBengalí.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna Bengalensis");

        ContentValues CharránPatinegro = new ContentValues();
        CharránPatinegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Patinegro  ");
        CharránPatinegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna Sandvicensis");

        ContentValues CharránElegante = new ContentValues();
        CharránElegante.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Elegante");
        CharránElegante.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna");

        ContentValues CharránRosado = new ContentValues();
        CharránRosado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Rosado  ");
        CharránRosado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna Dougallii");

        ContentValues CharránComún = new ContentValues();
        CharránComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Común ");
        CharránComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna Hirundo");

        ContentValues CharránÁrtico = new ContentValues();
        CharránÁrtico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Ártico  ");
        CharránÁrtico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna Paradisaea");

        ContentValues CharránDeForster = new ContentValues();
        CharránDeForster.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán De Forster ");
        CharránDeForster.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sterna Forsteri");

        ContentValues CharránEmbridado = new ContentValues();
        CharránEmbridado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Embridado  ");
        CharránEmbridado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Onychoprion Anaethetus");

        ContentValues CharránSombrío = new ContentValues();
        CharránSombrío.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrán Sombrío ");
        CharránSombrío.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Onychoprion Fuscatus");

        ContentValues CharrancitoComún = new ContentValues();
        CharrancitoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charrancito Común ");
        CharrancitoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sternula Albifrons");

        ContentValues FumarelCariblanco = new ContentValues();
        FumarelCariblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Fumarel Cariblanco ");
        FumarelCariblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chlidonias Hybrida");

        ContentValues FumarelComún = new ContentValues();
        FumarelComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Fumarel Común ");
        FumarelComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chlidonias Niger");

        ContentValues FumarelAliblanco = new ContentValues();
        FumarelAliblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Fumarel Aliblanco  ");
        FumarelAliblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chlidonias Leucopterus");

        ContentValues AraoComún = new ContentValues();
        AraoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Arao Común");
        AraoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Uria");

        ContentValues AlcaComún = new ContentValues();
        AlcaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alca  Común");
        AlcaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Alca");

        ContentValues AraoAliblanco = new ContentValues();
        AraoAliblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Arao Aliblanco ");
        AraoAliblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cepphus Grylle");

        ContentValues FrailecilloAtlántico = new ContentValues();
        FrailecilloAtlántico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Frailecillo Atlántico ");
        FrailecilloAtlántico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fratercula Arctica");

        ContentValues GangaOrtega = new ContentValues();
        GangaOrtega.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ganga Ortega ");
        GangaOrtega.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pterocles Orientalis");

        ContentValues GangaIbérica = new ContentValues();
        GangaIbérica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ganga Ibérica ");
        GangaIbérica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pterocles Alchata");

        ContentValues GangaDePallas = new ContentValues();
        GangaDePallas.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ganga De Pallas ");
        GangaDePallas.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Syrrhaptes Paradoxus");

        ContentValues PalomaBravía = new ContentValues();
        PalomaBravía.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paloma Bravía ");
        PalomaBravía.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Columba Livia");

        ContentValues PalomaTorcaz = new ContentValues();
        PalomaTorcaz.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paloma Torcaz ");
        PalomaTorcaz.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Columba Palumbus");

        ContentValues PalomaTurqué = new ContentValues();
        PalomaTurqué.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paloma Turqué ");
        PalomaTurqué.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Columba Bollii");

        ContentValues PalomaRabiche = new ContentValues();
        PalomaRabiche.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Paloma Rabiche ");
        PalomaRabiche.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Columba Junoniae");

        ContentValues TórtolaTurca = new ContentValues();
        TórtolaTurca.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tórtola Turca ");
        TórtolaTurca.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Streptopelia Decaocto");

        ContentValues TórtolaEuropea = new ContentValues();
        TórtolaEuropea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tórtola Europea ");
        TórtolaEuropea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Streptopelia Turtur");

        ContentValues TórtolaOriental = new ContentValues();
        TórtolaOriental.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tórtola Oriental ");
        TórtolaOriental.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Streptopelia Orientalis");

        ContentValues TórtolaSenegalesa = new ContentValues();
        TórtolaSenegalesa.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tórtola Senegalesa ");
        TórtolaSenegalesa.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Streptopelia Senegalensis");

        ContentValues CotorraDeKramer = new ContentValues();
        CotorraDeKramer.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cotorra De Kramer ");
        CotorraDeKramer.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Psittacula Krameri");

        ContentValues CotorraArgentina = new ContentValues();
        CotorraArgentina.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cotorra Argentina ");
        CotorraArgentina.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Myiopsitta Monachus");

        ContentValues CríaloEuropeo = new ContentValues();
        CríaloEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Críalo Europeo  ");
        CríaloEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Clamator Glandarius");

        ContentValues CucoComún = new ContentValues();
        CucoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cuco Común ");
        CucoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cuculus Canorus");

        ContentValues CuclilloPiquigualdo = new ContentValues();
        CuclilloPiquigualdo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cuclillo Piquigualdo ");
        CuclilloPiquigualdo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Coccyzus Americanus");

        ContentValues LechuzaComún = new ContentValues();
        LechuzaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Lechuza Común ");
        LechuzaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tyto Alba");

        ContentValues BúhoReal = new ContentValues();
        BúhoReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Búho Real ");
        BúhoReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bubo Bubo");

        ContentValues MochueloComún = new ContentValues();
        MochueloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mochuelo Común  ");
        MochueloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Athene Noctua");

        ContentValues BúhoChico = new ContentValues();
        BúhoChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Búho Chico ");
        BúhoChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Asio Otus");

        ContentValues BúhoCampestre = new ContentValues();
        BúhoCampestre.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Búho Campestre ");
        BúhoCampestre.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Asio Flammeus");

        ContentValues BúhoMoro = new ContentValues();
        BúhoMoro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Búho Moro  ");
        BúhoMoro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Asio Capensis");

        ContentValues ChotacabrasEuropeo = new ContentValues();
        ChotacabrasEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chotacabras Europeo  ");
        ChotacabrasEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Caprimulgus Europaeus");

        ContentValues AñaperoYanqui = new ContentValues();
        AñaperoYanqui.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Añapero Yanqui  ");
        AñaperoYanqui.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chordeiles Minor");

        ContentValues VencejoDeChimenea = new ContentValues();
        VencejoDeChimenea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo De Chimenea  ");
        VencejoDeChimenea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chaetura Pelagica");

        ContentValues VencejoUnicolor = new ContentValues();
        VencejoUnicolor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo Unicolor ");
        VencejoUnicolor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Apus Unicolor");

        ContentValues VencejoComún = new ContentValues();
        VencejoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo Común  ");
        VencejoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Apus Apus");

        ContentValues VencejoPálido = new ContentValues();
        VencejoPálido.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo Pálido ");
        VencejoPálido.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Apus Pallidus");

        ContentValues VencejoReal = new ContentValues();
        VencejoReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo Real");
        VencejoReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Apus");

        ContentValues VencejoCafre = new ContentValues();
        VencejoCafre.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo Cafre  ");
        VencejoCafre.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Apus Caffer");

        ContentValues VencejoMoro = new ContentValues();
        VencejoMoro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vencejo Moro ");
        VencejoMoro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Apus Affinis");

        ContentValues MartínGiganteNorteamericano = new ContentValues();
        MartínGiganteNorteamericano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Martín Gigante Norteamericano ");
        MartínGiganteNorteamericano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Megaceryle Alcyon");

        ContentValues AbejarucoPersa = new ContentValues();
        AbejarucoPersa.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Abejaruco Persa ");
        AbejarucoPersa.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Merops Persicus");

        ContentValues AbejarucoEuropeo = new ContentValues();
        AbejarucoEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Abejaruco Europeo  ");
        AbejarucoEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Merops Apiaster");

        ContentValues Abubilla = new ContentValues();
        Abubilla.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Abubilla ");
        Abubilla.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Upupa Epops");

        ContentValues TorcecuelloEuroasiático = new ContentValues();
        TorcecuelloEuroasiático.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Torcecuello Euroasiático");
        TorcecuelloEuroasiático.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Jynx");

        ContentValues PitoReal = new ContentValues();
        PitoReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pito Real ");
        PitoReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Picus Viridis");

        ContentValues PicamaderosNegro = new ContentValues();
        PicamaderosNegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Picamaderos Negro ");
        PicamaderosNegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dryocopus Martius");

        ContentValues PicoPicapinos = new ContentValues();
        PicoPicapinos.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pico Picapinos");
        PicoPicapinos.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dendrocopos");

        ContentValues PicoMediano = new ContentValues();
        PicoMediano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pico Mediano  ");
        PicoMediano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dendrocopos Medius");

        ContentValues PicoMenor = new ContentValues();
        PicoMenor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pico Menor ");
        PicoMenor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dendrocopos Minor");

        ContentValues TijeretaSabanera = new ContentValues();
        TijeretaSabanera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tijereta Sabanera ");
        TijeretaSabanera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tyrannus Savana");

        ContentValues TerreraColinegra = new ContentValues();
        TerreraColinegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Terrera Colinegra ");
        TerreraColinegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ammomanes Cinctura");

        ContentValues AlondraRicotí = new ContentValues();
        AlondraRicotí.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alondra Ricotí  ");
        AlondraRicotí.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chersophilus Duponti");

        ContentValues CalandriaComún = new ContentValues();
        CalandriaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Calandria Común ");
        CalandriaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Melanocorypha Calandra");

        ContentValues TerreraMarismeña = new ContentValues();
        TerreraMarismeña.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Terrera Marismeña ");
        TerreraMarismeña.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calandrella");

        ContentValues CogujadaMontesina = new ContentValues();
        CogujadaMontesina.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cogujada Montesina ");
        CogujadaMontesina.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Galerida Theklae");

        ContentValues AlondraComún = new ContentValues();
        AlondraComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alondra Común  ");
        AlondraComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Alauda Arvensis");

        ContentValues AlondraCornuda = new ContentValues();
        AlondraCornuda.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alondra Cornuda ");
        AlondraCornuda.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Eremophila Alpestris");

        ContentValues AviónZapador = new ContentValues();
        AviónZapador.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avión Zapador ");
        AviónZapador.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Riparia Riparia");

        ContentValues AviónRoquero = new ContentValues();
        AviónRoquero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avión Roquero  ");
        AviónRoquero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ptyonoprogne");

        ContentValues GolondrinaComún = new ContentValues();
        GolondrinaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Golondrina Común ");
        GolondrinaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Hirundo Rustica");

        ContentValues GolondrinaDáurica = new ContentValues();
        GolondrinaDáurica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Golondrina Dáurica ");
        GolondrinaDáurica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cecropis Daurica");

        ContentValues AviónComún = new ContentValues();
        AviónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Avión Común ");
        AviónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Delichon Urbicum");

        ContentValues BisbitaEstepario = new ContentValues();
        BisbitaEstepario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Estepario ");
        BisbitaEstepario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Godlewskii");

        ContentValues BisbitaCaminero = new ContentValues();
        BisbitaCaminero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Caminero ");
        BisbitaCaminero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Berthelotii");

        ContentValues BisbitaDeHodgson = new ContentValues();
        BisbitaDeHodgson.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita De Hodgson ");
        BisbitaDeHodgson.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Hodgsoni");

        ContentValues BisbitaArbóreo = new ContentValues();
        BisbitaArbóreo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Arbóreo ");
        BisbitaArbóreo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Trivialis");

        ContentValues BisbitaPratense = new ContentValues();
        BisbitaPratense.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Pratense  ");
        BisbitaPratense.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Pratensis");

        ContentValues BisbitaGorgirrojo = new ContentValues();
        BisbitaGorgirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Gorgirrojo ");
        BisbitaGorgirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Cervinus");

        ContentValues BisbitaNorteamericano = new ContentValues();
        BisbitaNorteamericano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Norteamericano  ");
        BisbitaNorteamericano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Rubescens");

        ContentValues BisbitaAlpino = new ContentValues();
        BisbitaAlpino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Alpino ");
        BisbitaAlpino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Spinoletta");

        ContentValues BisbitaCostero = new ContentValues();
        BisbitaCostero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bisbita Costero  ");
        BisbitaCostero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anthus Petrosus");

        ContentValues LavanderaBoyera = new ContentValues();
        LavanderaBoyera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Lavandera Boyera ");
        LavanderaBoyera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Motacilla Flava");

        ContentValues LavanderaCetrina = new ContentValues();
        LavanderaCetrina.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Lavandera Cetrina ");
        LavanderaCetrina.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Motacilla");

        ContentValues LavanderaCascadeña = new ContentValues();
        LavanderaCascadeña.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Lavandera Cascadeña ");
        LavanderaCascadeña.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Motacilla Cinerea");

        ContentValues BulbulNaranjero = new ContentValues();
        BulbulNaranjero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bulbul Naranjero ");
        BulbulNaranjero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pycnonotus Barbatus");

        ContentValues AmpelisEuropeo = new ContentValues();
        AmpelisEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ampelis Europeo  ");
        AmpelisEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bombycilla Garrulus");

        ContentValues MirloAcuáticoEuropeo = new ContentValues();
        MirloAcuáticoEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mirlo Acuático Europeo ");
        MirloAcuáticoEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cinclus Cinclus");

        ContentValues ChochínComún = new ContentValues();
        ChochínComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chochín Común  ");
        ChochínComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Troglodytes Troglodytes");

        ContentValues PájaroGatoGris = new ContentValues();
        PájaroGatoGris.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pájaro Gato Gris ");
        PájaroGatoGris.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dumetella");

        ContentValues AcentorComún = new ContentValues();
        AcentorComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Acentor Común ");
        AcentorComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Prunella");

        ContentValues AcentorAlpino = new ContentValues();
        AcentorAlpino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Acentor Alpino ");
        AcentorAlpino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Prunella Collaris");

        ContentValues AlzacolaRojizo = new ContentValues();
        AlzacolaRojizo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alzacola Rojizo ");
        AlzacolaRojizo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cercotrichas Galactotes");

        ContentValues PetirrojoEuropeo = new ContentValues();
        PetirrojoEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Petirrojo Europeo  ");
        PetirrojoEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Erithacus Rubecula");

        ContentValues RuiseñorComúns = new ContentValues();
        RuiseñorComúns.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ruiseñor Comúns ");
        RuiseñorComúns.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Luscinia");

        ContentValues RuiseñorPechiazul = new ContentValues();
        RuiseñorPechiazul.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ruiseñor Pechiazul");
        RuiseñorPechiazul.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Luscinia");

        ContentValues RuiseñorAzul = new ContentValues();
        RuiseñorAzul.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ruiseñor Azul  ");
        RuiseñorAzul.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Larvivora Cyane");

        ContentValues RuiseñorColiazul = new ContentValues();
        RuiseñorColiazul.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Ruiseñor Coliazul  ");
        RuiseñorColiazul.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tarsiger Cyanurus");

        ContentValues ColirrojoTizón = new ContentValues();
        ColirrojoTizón.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colirrojo Tizón ");
        ColirrojoTizón.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phoenicurus Ochruros");

        ContentValues ColirrojoReal = new ContentValues();
        ColirrojoReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colirrojo Real ");
        ColirrojoReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phoenicurus Phoenicurus");

        ContentValues ColirrojoDiademado = new ContentValues();
        ColirrojoDiademado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Colirrojo Diademado  ");
        ColirrojoDiademado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phoenicurus Moussieri");

        ContentValues TarabillaNorteña = new ContentValues();
        TarabillaNorteña.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tarabilla Norteña ");
        TarabillaNorteña.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Saxicola Rubetra");

        ContentValues TarabillaSiberiana = new ContentValues();
        TarabillaSiberiana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tarabilla Siberiana ");
        TarabillaSiberiana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Saxicola Maurus");

        ContentValues TarabillaCanaria = new ContentValues();
        TarabillaCanaria.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tarabilla Canaria  ");
        TarabillaCanaria.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Saxicola Dacotiae");

        ContentValues CollalbaIsabel = new ContentValues();
        CollalbaIsabel.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Collalba Isabel ");
        CollalbaIsabel.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oenanthe Isabellina");

        ContentValues CollalbaRubia = new ContentValues();
        CollalbaRubia.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Collalba Rubia ");
        CollalbaRubia.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oenanthe Hispanica");

        ContentValues CollalbaYebélica = new ContentValues();
        CollalbaYebélica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Collalba Yebélica  ");
        CollalbaYebélica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oenanthe Leucopyga");

        ContentValues RoqueroRojo = new ContentValues();
        RoqueroRojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Roquero Rojo  ");
        RoqueroRojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Monticola Saxatilis");

        ContentValues ZorzalDoradoDelHimalaya = new ContentValues();
        ZorzalDoradoDelHimalaya.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal Dorado Del Himalaya ");
        ZorzalDoradoDelHimalaya.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Zoothera Dauma");

        ContentValues MirloCapiblanco = new ContentValues();
        MirloCapiblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mirlo Capiblanco ");
        MirloCapiblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Torquatus");

        ContentValues MirloComún = new ContentValues();
        MirloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mirlo Común  ");
        MirloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Merula");

        ContentValues ZorzalDeNaumann = new ContentValues();
        ZorzalDeNaumann.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal De Naumann  ");
        ZorzalDeNaumann.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Naumanni");

        ContentValues ZorzalReal = new ContentValues();
        ZorzalReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal Real ");
        ZorzalReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Pilaris");

        ContentValues ZorzalComún = new ContentValues();
        ZorzalComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal Común ");
        ZorzalComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Philomelos");

        ContentValues ZorzalAlirrojo = new ContentValues();
        ZorzalAlirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal Alirrojo ");
        ZorzalAlirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Iliacus");

        ContentValues ZorzalCharlo = new ContentValues();
        ZorzalCharlo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal Charlo ");
        ZorzalCharlo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Viscivorus");

        ContentValues ZorzalRobín = new ContentValues();
        ZorzalRobín.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zorzal Robín ");
        ZorzalRobín.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Turdus Migratorius");

        ContentValues CetiaRuiseñor = new ContentValues();
        CetiaRuiseñor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cetia Ruiseñor ");
        CetiaRuiseñor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cettia Cetti");

        ContentValues CistícolaBuitrón = new ContentValues();
        CistícolaBuitrón.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cistícola Buitrón ");
        CistícolaBuitrón.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cisticola Juncidis");

        ContentValues BuscarlaPintoja = new ContentValues();
        BuscarlaPintoja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buscarla Pintoja  ");
        BuscarlaPintoja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Locustella Naevia");

        ContentValues BuscarlaFluvial = new ContentValues();
        BuscarlaFluvial.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buscarla Fluvial ");
        BuscarlaFluvial.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Locustella Fluviatilis");

        ContentValues BuscarlaUnicolor = new ContentValues();
        BuscarlaUnicolor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Buscarla Unicolor ");
        BuscarlaUnicolor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Locustella Luscinioides");

        ContentValues ZarceroEscita = new ContentValues();
        ZarceroEscita.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarcero Escita ");
        ZarceroEscita.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Iduna Caligata");

        ContentValues ZarceroBereber = new ContentValues();
        ZarceroBereber.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarcero Bereber ");
        ZarceroBereber.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Iduna Opaca");

        ContentValues CarricerínReal = new ContentValues();
        CarricerínReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricerín Real  ");
        CarricerínReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Melanopogon");

        ContentValues CarricerínCejudo = new ContentValues();
        CarricerínCejudo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricerín Cejudo ");
        CarricerínCejudo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Paludicola");

        ContentValues CarricerínComún = new ContentValues();
        CarricerínComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricerín Común ");
        CarricerínComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Schoenobaenus");

        ContentValues CarriceroAgrícola = new ContentValues();
        CarriceroAgrícola.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricero Agrícola ");
        CarriceroAgrícola.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Agricola");

        ContentValues CarriceroDeBlyth = new ContentValues();
        CarriceroDeBlyth.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricero De Blyth ");
        CarriceroDeBlyth.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Dumetorum");

        ContentValues CarriceroPolíglota = new ContentValues();
        CarriceroPolíglota.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricero Políglota ");
        CarriceroPolíglota.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Palustris");

        ContentValues CarriceroComún = new ContentValues();
        CarriceroComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricero Común ");
        CarriceroComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Scirpaceus");

        ContentValues CarriceroTordal = new ContentValues();
        CarriceroTordal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carricero Tordal ");
        CarriceroTordal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Acrocephalus Arundinaceus");

        ContentValues ZarceroIcterino = new ContentValues();
        ZarceroIcterino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarcero Icterino ");
        ZarceroIcterino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Hippolais Icterina");

        ContentValues ZarceroPolíglota = new ContentValues();
        ZarceroPolíglota.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Zarcero Políglota ");
        ZarceroPolíglota.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Hippolais Polyglotta");

        ContentValues CurrucaSarda = new ContentValues();
        CurrucaSarda.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Sarda ");
        CurrucaSarda.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Sarda");

        ContentValues CurrucaBalear = new ContentValues();
        CurrucaBalear.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Balear ");
        CurrucaBalear.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Balearica");

        ContentValues CurrucaRabilarga = new ContentValues();
        CurrucaRabilarga.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Rabilarga ");
        CurrucaRabilarga.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Undata");

        ContentValues CurrucaDelAtlas = new ContentValues();
        CurrucaDelAtlas.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Del Atlas ");
        CurrucaDelAtlas.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Deserticola");

        ContentValues CurrucaTomillera = new ContentValues();
        CurrucaTomillera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Tomillera ");
        CurrucaTomillera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Conspicillata");

        ContentValues CurrucaCarrasqueña = new ContentValues();
        CurrucaCarrasqueña.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Carrasqueña ");
        CurrucaCarrasqueña.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Cantillans");

        ContentValues CurrucaCabecinegra = new ContentValues();
        CurrucaCabecinegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Cabecinegra  ");
        CurrucaCabecinegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Melanocephala");

        ContentValues CurrucaDeRuppell = new ContentValues();
        CurrucaDeRuppell.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca De Ruppell ");
        CurrucaDeRuppell.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Rueppellii");

        ContentValues CurrucaSahariana = new ContentValues();
        CurrucaSahariana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Sahariana ");
        CurrucaSahariana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Deserti");

        ContentValues CurrucaMirlona = new ContentValues();
        CurrucaMirlona.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Mirlona ");
        CurrucaMirlona.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Hortensis");

        ContentValues CurrucaGavilana = new ContentValues();
        CurrucaGavilana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Gavilana ");
        CurrucaGavilana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia");

        ContentValues CurrucaZarcerilla = new ContentValues();
        CurrucaZarcerilla.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Zarcerilla ");
        CurrucaZarcerilla.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Curruca");

        ContentValues CurrucaZarcera = new ContentValues();
        CurrucaZarcera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Zarcera ");
        CurrucaZarcera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Communis");

        ContentValues CurrucaMosquitera = new ContentValues();
        CurrucaMosquitera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Mosquitera ");
        CurrucaMosquitera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Borin");

        ContentValues CurrucaCapirotada = new ContentValues();
        CurrucaCapirotada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Curruca Capirotada ");
        CurrucaCapirotada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sylvia Atricapilla");

        ContentValues MosquiteroVerdoso = new ContentValues();
        MosquiteroVerdoso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Verdoso");
        MosquiteroVerdoso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus");

        ContentValues MosquiteroBoreal = new ContentValues();
        MosquiteroBoreal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Boreal ");
        MosquiteroBoreal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Borealis");

        ContentValues MosquiteroDePallas = new ContentValues();
        MosquiteroDePallas.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero De Pallas  ");
        MosquiteroDePallas.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Proregulus");

        ContentValues MosquiteroBilistado = new ContentValues();
        MosquiteroBilistado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Bilistado  ");
        MosquiteroBilistado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Inornatus");

        ContentValues MosquiteroDeHume = new ContentValues();
        MosquiteroDeHume.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero De Hume ");
        MosquiteroDeHume.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Humei");

        ContentValues MosquiteroDeSchwarz = new ContentValues();
        MosquiteroDeSchwarz.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero De Schwarz  ");
        MosquiteroDeSchwarz.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Schwarzi");

        ContentValues MosquiteroSombrío = new ContentValues();
        MosquiteroSombrío.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Sombrío  ");
        MosquiteroSombrío.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Fuscatus");

        ContentValues MosquiteroPapialbo = new ContentValues();
        MosquiteroPapialbo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Papialbo ");
        MosquiteroPapialbo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Bonelli");

        ContentValues MosquiteroSilbador = new ContentValues();
        MosquiteroSilbador.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Silbador  ");
        MosquiteroSilbador.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Sibilatrix");

        ContentValues MosquiteroComún = new ContentValues();
        MosquiteroComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Común ");
        MosquiteroComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Collybita");

        ContentValues MosquiteroIbérico = new ContentValues();
        MosquiteroIbérico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Ibérico  ");
        MosquiteroIbérico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Ibericus");

        ContentValues MosquiteroCanario = new ContentValues();
        MosquiteroCanario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Canario ");
        MosquiteroCanario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Canariensis");

        ContentValues MosquiteroMusical = new ContentValues();
        MosquiteroMusical.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mosquitero Musical ");
        MosquiteroMusical.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phylloscopus Trochilus");

        ContentValues ReyezueloSencillo = new ContentValues();
        ReyezueloSencillo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Reyezuelo Sencillo ");
        ReyezueloSencillo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Regulus Regulus");

        ContentValues ReyezueloListado = new ContentValues();
        ReyezueloListado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Reyezuelo Listado  ");
        ReyezueloListado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Regulus Ignicapilla");

        ContentValues PapamoscasGris = new ContentValues();
        PapamoscasGris.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Papamoscas Gris ");
        PapamoscasGris.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Muscicapa Striata");

        ContentValues PapamoscasPapirrojo = new ContentValues();
        PapamoscasPapirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Papamoscas Papirrojo ");
        PapamoscasPapirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ficedula Parva");

        ContentValues PapamoscasSemicollarino = new ContentValues();
        PapamoscasSemicollarino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Papamoscas Semicollarino ");
        PapamoscasSemicollarino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ficedula Semitorquata");

        ContentValues PapamoscasAcollarado = new ContentValues();
        PapamoscasAcollarado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Papamoscas Acollarado  ");
        PapamoscasAcollarado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ficedula Albicollis");

        ContentValues PapamoscasCerrojillo = new ContentValues();
        PapamoscasCerrojillo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Papamoscas Cerrojillo  ");
        PapamoscasCerrojillo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Ficedula Hypoleuca");

        ContentValues Bigotudo = new ContentValues();
        Bigotudo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bigotudo ");
        Bigotudo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Panurus Biarmicus");

        ContentValues LeiotrixPiquirrojo = new ContentValues();
        LeiotrixPiquirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Leiotrix Piquirrojo ");
        LeiotrixPiquirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Leiothrix Lutea");

        ContentValues MitoComún = new ContentValues();
        MitoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Mito Común ");
        MitoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aegithalos Caudatus");

        ContentValues CarboneroPalustre = new ContentValues();
        CarboneroPalustre.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carbonero Palustre  ");
        CarboneroPalustre.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Poecile Palustris");

        ContentValues HerrerilloCapuchino = new ContentValues();
        HerrerilloCapuchino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Herrerillo Capuchino");
        HerrerilloCapuchino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lophophanes");

        ContentValues CarboneroGarrapinos = new ContentValues();
        CarboneroGarrapinos.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carbonero Garrapinos ");
        CarboneroGarrapinos.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Periparus Ater");

        ContentValues HerrerilloComún = new ContentValues();
        HerrerilloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Herrerillo Común ");
        HerrerilloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cyanistes Caeruleus");

        ContentValues HerrerilloCanario = new ContentValues();
        HerrerilloCanario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Herrerillo Canario ");
        HerrerilloCanario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cyanistes Teneriffae");

        ContentValues CarboneroComún = new ContentValues();
        CarboneroComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Carbonero Común ");
        CarboneroComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Parus Major");

        ContentValues TrepadorAzul = new ContentValues();
        TrepadorAzul.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Trepador Azul ");
        TrepadorAzul.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sitta Europaea");

        ContentValues Treparriscos = new ContentValues();
        Treparriscos.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Treparriscos ");
        Treparriscos.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tichodroma Muraria");

        ContentValues AgateadorEuroasiático = new ContentValues();
        AgateadorEuroasiático.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Agateador Euroasiático  ");
        AgateadorEuroasiático.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Certhia Familiaris");

        ContentValues AgateadorEuropeo = new ContentValues();
        AgateadorEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Agateador Europeo  ");
        AgateadorEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Certhia Brachydactyla");

        ContentValues PájaroMoscónEuropeo = new ContentValues();

        PájaroMoscónEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pájaro Moscón Europeo ");
        PájaroMoscónEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Remiz Pendulinus");

        ContentValues OropéndolaEuropea = new ContentValues();
        OropéndolaEuropea.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Oropéndola Europea  ");
        OropéndolaEuropea.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oriolus Oriolus");

        ContentValues ChagraDelSenegal = new ContentValues();
        ChagraDelSenegal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chagra Del Senegal ");
        ChagraDelSenegal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Tchagra Senegalus");

        ContentValues AlcaudónIsabel = new ContentValues();
        AlcaudónIsabel.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Isabel ");
        AlcaudónIsabel.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Isabellinus");

        ContentValues AlcaudónDorsirrojo = new ContentValues();
        AlcaudónDorsirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Dorsirrojo ");
        AlcaudónDorsirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Collurio");

        ContentValues AlcaudónChico = new ContentValues();
        AlcaudónChico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Chico  ");
        AlcaudónChico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Minor");

        ContentValues AlcaudónNorteño = new ContentValues();
        AlcaudónNorteño.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Norteño ");
        AlcaudónNorteño.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Excubitor");

        ContentValues AlcaudónReal = new ContentValues();
        AlcaudónReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Real ");
        AlcaudónReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Meridionalis");

        ContentValues AlcaudónComún = new ContentValues();
        AlcaudónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Común  ");
        AlcaudónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Senator");

        ContentValues AlcaudónNúbico = new ContentValues();
        AlcaudónNúbico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alcaudón Núbico ");
        AlcaudónNúbico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Lanius Nubicus");

        ContentValues ArrendajoEuroasiático = new ContentValues();
        ArrendajoEuroasiático.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Arrendajo Euroasiático  ");
        ArrendajoEuroasiático.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Garrulus Glandarius");

        ContentValues RabilargoIbérico = new ContentValues();
        RabilargoIbérico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Rabilargo Ibérico ");
        RabilargoIbérico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Cyanopica Cookii");

        ContentValues UrracaComún = new ContentValues();
        UrracaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Urraca Común ");
        UrracaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pica");

        ContentValues CascanuecesComún = new ContentValues();
        CascanuecesComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cascanueces Común ");
        CascanuecesComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Nucifraga Caryocatactes");

        ContentValues ChovaPiquigualda = new ContentValues();
        ChovaPiquigualda.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chova Piquigualda ");
        ChovaPiquigualda.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pyrrhocorax Graculus");

        ContentValues ChovaPiquirroja = new ContentValues();
        ChovaPiquirroja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Chova Piquirroja ");
        ChovaPiquirroja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pyrrhocorax Pyrrhocorax");

        ContentValues GrajillaOccidental = new ContentValues();
        GrajillaOccidental.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Grajilla Occidental ");
        GrajillaOccidental.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Corvus Monedula");

        ContentValues Graja = new ContentValues();
        Graja.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Graja  ");
        Graja.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Corvus Frugilegus");

        ContentValues CornejaNegra = new ContentValues();
        CornejaNegra.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Corneja Negra ");
        CornejaNegra.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Corvus Corone");

        ContentValues CornejaCenicienta = new ContentValues();
        CornejaCenicienta.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Corneja Cenicienta  ");
        CornejaCenicienta.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Corvus Cornix");

        ContentValues CuervoGrande = new ContentValues();
        CuervoGrande.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cuervo Grande ");
        CuervoGrande.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Corvus Corax");

        ContentValues EstorninoNegro = new ContentValues();
        EstorninoNegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Estornino Negro  ");
        EstorninoNegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sturnus Unicolor");

        ContentValues EstorninoPinto = new ContentValues();
        EstorninoPinto.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Estornino Pinto ");
        EstorninoPinto.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Sturnus Vulgaris");

        ContentValues EstorninoRosado = new ContentValues();
        EstorninoRosado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Estornino Rosado  ");
        EstorninoRosado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pastor Roseus");

        ContentValues GorriónComún = new ContentValues();
        GorriónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gorrión Común  ");
        GorriónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Passer Domesticus");

        ContentValues GorriónMoruno = new ContentValues();
        GorriónMoruno.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gorrión Moruno  ");
        GorriónMoruno.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Passer Hispaniolensis");

        ContentValues GorriónMolinero = new ContentValues();
        GorriónMolinero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gorrión Molinero ");
        GorriónMolinero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Passer Montanus");

        ContentValues GorriónChillón = new ContentValues();
        GorriónChillón.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gorrión Chillón ");
        GorriónChillón.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Petronia Petronia");

        ContentValues GorriónAlpino = new ContentValues();
        GorriónAlpino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Gorrión Alpino ");
        GorriónAlpino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Montifringilla Nivalis");

        ContentValues EstrildaComún = new ContentValues();
        EstrildaComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Estrilda Común ");
        EstrildaComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Estrilda");

        ContentValues BengalíRojo = new ContentValues();
        BengalíRojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Bengalí Rojo ");
        BengalíRojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Amandava Amandava");

        ContentValues VireoChiví = new ContentValues();
        VireoChiví.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Vireo Chiví ");
        VireoChiví.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Vireo Olivaceus");

        ContentValues PinzónVulgar = new ContentValues();
        PinzónVulgar.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pinzón Vulgar ");
        PinzónVulgar.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fringilla Coelebs");

        ContentValues PinzónAzul = new ContentValues();
        PinzónAzul.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pinzón Azul  ");
        PinzónAzul.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fringilla Teydea");

        ContentValues PinzónReal = new ContentValues();
        PinzónReal.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pinzón Real ");
        PinzónReal.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Fringilla Montifringilla");

        ContentValues SerínVerdecillo = new ContentValues();
        SerínVerdecillo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Serín Verdecillo");
        SerínVerdecillo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Serinus");

        ContentValues SerínCanario = new ContentValues();
        SerínCanario.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Serín Canario ");
        SerínCanario.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Serinus Canaria");

        ContentValues VerderónComún = new ContentValues();
        VerderónComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Verderón Común  ");
        VerderónComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Chloris Chloris");

        ContentValues VerderónSerrano = new ContentValues();
        VerderónSerrano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Verderón Serrano  ");
        VerderónSerrano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carduelis Citrinella");

        ContentValues JilgueroEuropeo = new ContentValues();
        JilgueroEuropeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Jilguero Europeo ");
        JilgueroEuropeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carduelis Carduelis");

        ContentValues JilgueroLúgano = new ContentValues();
        JilgueroLúgano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Jilguero Lúgano ");
        JilgueroLúgano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carduelis Spinus");

        ContentValues PardilloComún = new ContentValues();
        PardilloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardillo Común  ");
        PardilloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carduelis Cannabina");

        ContentValues PardilloPiquigualdo = new ContentValues();
        PardilloPiquigualdo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardillo Piquigualdo ");
        PardilloPiquigualdo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carduelis Flavirostris");

        ContentValues PardilloSizerín = new ContentValues();
        PardilloSizerín.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pardillo Sizerín  ");
        PardilloSizerín.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carduelis Flammea");

        ContentValues PiquituertoComún = new ContentValues();
        PiquituertoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Piquituerto Común ");
        PiquituertoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Loxia Curvirostra");

        ContentValues CamachueloTrompetero = new ContentValues();
        CamachueloTrompetero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Camachuelo Trompetero ");
        CamachueloTrompetero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Bucanetes Githagineus");

        ContentValues CamachueloCarminoso = new ContentValues();
        CamachueloCarminoso.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Camachuelo Carminoso ");
        CamachueloCarminoso.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Carpodacus Erythrinus");

        ContentValues CamachueloComún = new ContentValues();
        CamachueloComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Camachuelo Común  ");
        CamachueloComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pyrrhula Pyrrhula");

        ContentValues PicogordoComún = new ContentValues();
        PicogordoComún.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Picogordo Común ");
        PicogordoComún.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Coccothraustes Coccothraustes");

        ContentValues ReinitaTrepadora = new ContentValues();
        ReinitaTrepadora.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Reinita Trepadora  ");
        ReinitaTrepadora.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mniotilta Varia");

        ContentValues ReinitaCoronada = new ContentValues();
        ReinitaCoronada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Reinita Coronada ");
        ReinitaCoronada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Setophaga Coronata");

        ContentValues ReinitaCharqueraDeLuisiana = new ContentValues();
        ReinitaCharqueraDeLuisiana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Reinita Charquera De Luisiana  ");
        ReinitaCharqueraDeLuisiana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Parkesia Motacilla");

        ContentValues EscribanoLapón = new ContentValues();
        EscribanoLapón.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Lapón ");
        EscribanoLapón.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Calcarius Lapponicus");

        ContentValues EscribanoNival = new ContentValues();
        EscribanoNival.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Nival ");
        EscribanoNival.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Plectrophenax Nivalis");

        ContentValues EscribanoCabeciblanco = new ContentValues();
        EscribanoCabeciblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Cabeciblanco ");
        EscribanoCabeciblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Leucocephalos");

        ContentValues EscribanoCerillo = new ContentValues();
        EscribanoCerillo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Cerillo ");
        EscribanoCerillo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Citrinella");

        ContentValues EscribanoSoteño = new ContentValues();
        EscribanoSoteño.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Soteño  ");
        EscribanoSoteño.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Cirlus");

        ContentValues EscribanoMontesino = new ContentValues();
        EscribanoMontesino.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Montesino ");
        EscribanoMontesino.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Cia");

        ContentValues EscribanoSahariano = new ContentValues();
        EscribanoSahariano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Sahariano ");
        EscribanoSahariano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Sahari");

        ContentValues EscribanoHortelano = new ContentValues();
        EscribanoHortelano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Hortelano  ");
        EscribanoHortelano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Hortulana");

        ContentValues EscribanoCeniciento = new ContentValues();
        EscribanoCeniciento.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Ceniciento ");
        EscribanoCeniciento.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Caesia");

        ContentValues EscribanoRústico = new ContentValues();
        EscribanoRústico.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Rústico  ");
        EscribanoRústico.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Rustica");

        ContentValues EscribanoPigmeo = new ContentValues();
        EscribanoPigmeo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Pigmeo  ");
        EscribanoPigmeo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Pusilla");

        ContentValues EscribanoAureolado = new ContentValues();
        EscribanoAureolado.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Aureolado ");
        EscribanoAureolado.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Aureola");

        ContentValues EscribanoPalustre = new ContentValues();
        EscribanoPalustre.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Palustre ");
        EscribanoPalustre.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Schoeniclus");

        ContentValues EscribanoCarirrojo = new ContentValues();
        EscribanoCarirrojo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Carirrojo ");
        EscribanoCarirrojo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Bruniceps");

        ContentValues EscribanoCabecinegro = new ContentValues();
        EscribanoCabecinegro.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Cabecinegro  ");
        EscribanoCabecinegro.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Melanocephala");

        ContentValues EscribanoTriguero = new ContentValues();
        EscribanoTriguero.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano Triguero ");
        EscribanoTriguero.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Calandra");

        ContentValues Charlatán = new ContentValues();
        Charlatán.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Charlatán ");
        Charlatán.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dolichonyx Oryzivorus");

        ContentValues PetrelFreira = new ContentValues();
        PetrelFreira.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Petrel Freira   ");
        PetrelFreira.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pterodroma Madeira");

        ContentValues PetrelGonGon = new ContentValues();

        PetrelGonGon.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Petrel Gon-Gon  ");
        PetrelGonGon.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Pterodroma Feae");

        ContentValues GolondrinaPueblera = new ContentValues();
        GolondrinaPueblera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Golondrina Pueblera  ");
        GolondrinaPueblera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Petrochelidon Fulva");

        ContentValues GolondrinaRisquera = new ContentValues();
        GolondrinaRisquera.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Golondrina Risquera ");
        GolondrinaRisquera.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Petrochelidon Pyrrhonota");

        ContentValues SuiriríBicolor = new ContentValues();
        SuiriríBicolor.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Suirirí Bicolor ");
        SuiriríBicolor.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dendrocygna Bicolor");

        ContentValues SuiriríCariblanco = new ContentValues();
        SuiriríCariblanco.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Suirirí Cariblanco ");
        SuiriríCariblanco.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Dendrocygna Viduata");

        ContentValues NsarNival = new ContentValues();
        NsarNival.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Nsar Nival  ");
        NsarNival.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anser Caerulescens");

        ContentValues BarnaclaCanadienseChica = new ContentValues();
        BarnaclaCanadienseChica.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Barnacla Canadiense Chica ");
        BarnaclaCanadienseChica.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Branta Hutchinsii");

        ContentValues PatoJoyuyo = new ContentValues();
        PatoJoyuyo.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Pato Joyuyo ");
        PatoJoyuyo.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Aix Sponsa");

        ContentValues CercetaColorada = new ContentValues();
        CercetaColorada.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cerceta Colorada ");
        CercetaColorada.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Anas Cyanoptera");

        ContentValues CormoránAfricano = new ContentValues();
        CormoránAfricano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Cormorán Africano  ");
        CormoránAfricano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Phalacrocorax Africanus");

        ContentValues TántaloAfricano = new ContentValues();
        TántaloAfricano.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tántalo Africano ");
        TántaloAfricano.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mycteria Ibis");

        ContentValues EspátulaAfricana = new ContentValues();
        EspátulaAfricana.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Espátula Africana ");
        EspátulaAfricana.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Platalea Alba");

        ContentValues AlimocheSombrío = new ContentValues();
        AlimocheSombrío.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Alimoche Sombrío  ");
        AlimocheSombrío.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Necrosyrtes Monachus");

        ContentValues TortolitaRabilarga = new ContentValues();
        TortolitaRabilarga.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Tortolita Rabilarga  ");
        TortolitaRabilarga.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Oena Capensis");

        ContentValues SinsonteNorteño = new ContentValues();
        SinsonteNorteño.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Sinsonte Norteño");
        SinsonteNorteño.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Mimus");

        ContentValues CamachueloDesertícola = new ContentValues();
        CamachueloDesertícola.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Camachuelo Desertícola  ");
        CamachueloDesertícola.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Rhodospiza Obsoleta");

        ContentValues EscribanoDeBrandt = new ContentValues();
        EscribanoDeBrandt.put(COLUMN_PAJARO_NOMBRE_CASTELLANO, "Escribano De Brandt ");
        EscribanoDeBrandt.put(COLUMN_PAJARO_NOMBRE_CIENTIFICO, "Emberiza Cioides");


        db.insert(TABLE_PAJARO, null, CisneNegro);

        db.insert(TABLE_PAJARO, null, CisneVulgar);

        db.insert(TABLE_PAJARO, null, CisneChico);

        db.insert(TABLE_PAJARO, null, CisneCantor);

        db.insert(TABLE_PAJARO, null, ÁnsarCampestre);

        db.insert(TABLE_PAJARO, null, ÁnsarPiquicorto);

        db.insert(TABLE_PAJARO, null, ÁnsarCareto);

        db.insert(TABLE_PAJARO, null, ÁnsarChico);

        db.insert(TABLE_PAJARO, null, ÁnsarComún);

        db.insert(TABLE_PAJARO, null, ÁnsarIndio);

        db.insert(TABLE_PAJARO, null, BarnaclaCanadienseGrande);

        db.insert(TABLE_PAJARO, null, BarnaclaCariblanca);

        db.insert(TABLE_PAJARO, null, BarnaclaCarinegra);

        db.insert(TABLE_PAJARO, null, BarnaclaCuellirroja);

        db.insert(TABLE_PAJARO, null, GansoDelNilo);

        db.insert(TABLE_PAJARO, null, TarroCanelo);

        db.insert(TABLE_PAJARO, null, TarroBlanco);

        db.insert(TABLE_PAJARO, null, PatoMandarín);

        db.insert(TABLE_PAJARO, null, SilbónEuropeo);

        db.insert(TABLE_PAJARO, null, SilbónAmericano);

        db.insert(TABLE_PAJARO, null, CercetaDeAlfanjes);

        db.insert(TABLE_PAJARO, null, ÁnadeFriso);

        db.insert(TABLE_PAJARO, null, CercetaDelBaikal);

        db.insert(TABLE_PAJARO, null, CercetaComún);

        db.insert(TABLE_PAJARO, null, CercetaAmericana);

        db.insert(TABLE_PAJARO, null, ÁnadeAzulón);

        db.insert(TABLE_PAJARO, null, ÁnadeSombrío);

        db.insert(TABLE_PAJARO, null, ÁnadeRabudo);

        db.insert(TABLE_PAJARO, null, CercetaCarretona);

        db.insert(TABLE_PAJARO, null, CercetaAliazul);

        db.insert(TABLE_PAJARO, null, CucharaComún);

        db.insert(TABLE_PAJARO, null, CercetaPardilla);

        db.insert(TABLE_PAJARO, null, PatoColorado);

        db.insert(TABLE_PAJARO, null, PorrónEuropeo);

        db.insert(TABLE_PAJARO, null, PorrónAcollarado);

        db.insert(TABLE_PAJARO, null, PorrónPardo);

        db.insert(TABLE_PAJARO, null, PorrónMoñudo);

        db.insert(TABLE_PAJARO, null, PorrónBastardo);

        db.insert(TABLE_PAJARO, null, PorrónBola);

        db.insert(TABLE_PAJARO, null, EiderComún);

        db.insert(TABLE_PAJARO, null, EiderReal);

        db.insert(TABLE_PAJARO, null, PatoHavelda);

        db.insert(TABLE_PAJARO, null, NegrónComún);

        db.insert(TABLE_PAJARO, null, NegrónAmericano);

        db.insert(TABLE_PAJARO, null, NegrónCareto);

        db.insert(TABLE_PAJARO, null, NegrónEspeculado);

        db.insert(TABLE_PAJARO, null, NegrónAliblanco);

        db.insert(TABLE_PAJARO, null, PorrónAlbeola);

        db.insert(TABLE_PAJARO, null, PorrónIslándico);

        db.insert(TABLE_PAJARO, null, PorrónOsculado);

        db.insert(TABLE_PAJARO, null, SerretaChica);

        db.insert(TABLE_PAJARO, null, SerretaCapuchona);

        db.insert(TABLE_PAJARO, null, SerretaMediana);

        db.insert(TABLE_PAJARO, null, SerretaGrande);

        db.insert(TABLE_PAJARO, null, MalvasíaCanela);

        db.insert(TABLE_PAJARO, null, MalvasíaCabeciblanca);

        db.insert(TABLE_PAJARO, null, GrévolComún);

        db.insert(TABLE_PAJARO, null, LagópodoAlpino);

        db.insert(TABLE_PAJARO, null, UrogalloComún);

        db.insert(TABLE_PAJARO, null, PerdizRoja);

        db.insert(TABLE_PAJARO, null, PerdizMoruna);

        db.insert(TABLE_PAJARO, null, PerdizPardilla);

        db.insert(TABLE_PAJARO, null, CodornizComún);

        db.insert(TABLE_PAJARO, null, FaisánVulgar);

        db.insert(TABLE_PAJARO, null, ColimboChico);

        db.insert(TABLE_PAJARO, null, ColimboÁrtico);

        db.insert(TABLE_PAJARO, null, ColimboDelPacífico);

        db.insert(TABLE_PAJARO, null, ColimboGrande);

        db.insert(TABLE_PAJARO, null, ZampullínPicogrueso);

        db.insert(TABLE_PAJARO, null, ZampullínComún);

        db.insert(TABLE_PAJARO, null, SomormujoLavanco);

        db.insert(TABLE_PAJARO, null, SomormujoCuellirrojo);

        db.insert(TABLE_PAJARO, null, ZampullínCuellirrojo);

        db.insert(TABLE_PAJARO, null, ZampullínCuellinegro);

        db.insert(TABLE_PAJARO, null, AlbatrosOjeroso);

        db.insert(TABLE_PAJARO, null, FulmarBoreal);

        db.insert(TABLE_PAJARO, null, PetrelDeElCabo);

        db.insert(TABLE_PAJARO, null, PetrelAntillano);

        db.insert(TABLE_PAJARO, null, PetrelDeBulwer);

        db.insert(TABLE_PAJARO, null, PardelaCenicienta);

        db.insert(TABLE_PAJARO, null, PardelaDeCaboVerde);

        db.insert(TABLE_PAJARO, null, PardelaCapirotada);

        db.insert(TABLE_PAJARO, null, PardelaSombría);

        db.insert(TABLE_PAJARO, null, PardelaPichoneta);

        db.insert(TABLE_PAJARO, null, PardelaMediterránea);

        db.insert(TABLE_PAJARO, null, PardelaBalear);

        db.insert(TABLE_PAJARO, null, PardelaChica);

        db.insert(TABLE_PAJARO, null, PaíñoDeWilson);

        db.insert(TABLE_PAJARO, null, PaíñoPechialbo);

        db.insert(TABLE_PAJARO, null, PaíñoVentrinegro);

        db.insert(TABLE_PAJARO, null, PaíñoEuropeo);

        db.insert(TABLE_PAJARO, null, PaíñoBoreal);

        db.insert(TABLE_PAJARO, null, PaíñoDeSwinhoe);

        db.insert(TABLE_PAJARO, null, PaíñoDeMadeira);

        db.insert(TABLE_PAJARO, null, RabijuncoEtéreo);

        db.insert(TABLE_PAJARO, null, PiqueroPatirrojo);

        db.insert(TABLE_PAJARO, null, PiqueroEnmascarado);

        db.insert(TABLE_PAJARO, null, PiqueroPardo);

        db.insert(TABLE_PAJARO, null, AlcatrazAtlántico);

        db.insert(TABLE_PAJARO, null, CormoránGrande);

        db.insert(TABLE_PAJARO, null, CormoránMoñudoS);

        db.insert(TABLE_PAJARO, null, PelícanoComún);

        db.insert(TABLE_PAJARO, null, PelícanoRosado);

        db.insert(TABLE_PAJARO, null, RabihorcadoMagnífico);

        db.insert(TABLE_PAJARO, null, AvetoroComún);

        db.insert(TABLE_PAJARO, null, AvetoroLentiginoso);

        db.insert(TABLE_PAJARO, null, AvetorilloComún);

        db.insert(TABLE_PAJARO, null, AvetorilloPlomizo);

        db.insert(TABLE_PAJARO, null, MartineteComún);

        db.insert(TABLE_PAJARO, null, GarcitaVerdosaS);

        db.insert(TABLE_PAJARO, null, GarcillaCangrejera);

        db.insert(TABLE_PAJARO, null, GarcillaBueyera);

        db.insert(TABLE_PAJARO, null, GarcetaTricolorr);

        db.insert(TABLE_PAJARO, null, GarcetaDimorfa);

        db.insert(TABLE_PAJARO, null, GarcetaComún);

        db.insert(TABLE_PAJARO, null, GarcetaGrande);

        db.insert(TABLE_PAJARO, null, GarzaReal);

        db.insert(TABLE_PAJARO, null, GarzaAzulada);

        db.insert(TABLE_PAJARO, null, GarzaImperial);

        db.insert(TABLE_PAJARO, null, CigüeñaNegra);

        db.insert(TABLE_PAJARO, null, CigüeñaBlanca);

        db.insert(TABLE_PAJARO, null, MarabúAfricano);

        db.insert(TABLE_PAJARO, null, MoritoComún);

        db.insert(TABLE_PAJARO, null, IbisEremita);

        db.insert(TABLE_PAJARO, null, IbisSagrado);

        db.insert(TABLE_PAJARO, null, EspátulaComún);

        db.insert(TABLE_PAJARO, null, FlamencoComún);

        db.insert(TABLE_PAJARO, null, FlamencoEnano);

        db.insert(TABLE_PAJARO, null, AbejeroEuropeo);

        db.insert(TABLE_PAJARO, null, ElanioTijereta);

        db.insert(TABLE_PAJARO, null, ElanioComún);

        db.insert(TABLE_PAJARO, null, MilanoNegro);

        db.insert(TABLE_PAJARO, null, MilanoReal);

        db.insert(TABLE_PAJARO, null, PigargoEuropeo);

        db.insert(TABLE_PAJARO, null, Quebrantahuesos);

        db.insert(TABLE_PAJARO, null, AlimocheComún);

        db.insert(TABLE_PAJARO, null, BuitreDorsiblancoAfricano);

        db.insert(TABLE_PAJARO, null, BuitreLeonado);

        db.insert(TABLE_PAJARO, null, BuitreMoteado);

        db.insert(TABLE_PAJARO, null, BuitreOrejudo);

        db.insert(TABLE_PAJARO, null, BuitreNegro);

        db.insert(TABLE_PAJARO, null, CulebreraEuropea);

        db.insert(TABLE_PAJARO, null, AguiluchoLaguneroOccidental);

        db.insert(TABLE_PAJARO, null, AguiluchoPálido);

        db.insert(TABLE_PAJARO, null, AguiluchoPapialbo);

        db.insert(TABLE_PAJARO, null, AguiluchoCenizo);

        db.insert(TABLE_PAJARO, null, AzorComún);

        db.insert(TABLE_PAJARO, null, GavilánComún);

        db.insert(TABLE_PAJARO, null, BusardoRatonero);

        db.insert(TABLE_PAJARO, null, BusardoMoro);

        db.insert(TABLE_PAJARO, null, BusardoCalzado);

        db.insert(TABLE_PAJARO, null, ÁguilaPomerana);

        db.insert(TABLE_PAJARO, null, ÁguilaMoteada);

        db.insert(TABLE_PAJARO, null, ÁguilaEsteparia);

        db.insert(TABLE_PAJARO, null, ÁguilaImperialIbérica);

        db.insert(TABLE_PAJARO, null, ÁguilaImperialOriental);

        db.insert(TABLE_PAJARO, null, ÁguilaReal);

        db.insert(TABLE_PAJARO, null, ÁguilaCalzada);

        db.insert(TABLE_PAJARO, null, ÁguilaPerdicera);

        db.insert(TABLE_PAJARO, null, ÁguilaPescadora);

        db.insert(TABLE_PAJARO, null, CernícaloPrimilla);

        db.insert(TABLE_PAJARO, null, CernícaloVulgar);

        db.insert(TABLE_PAJARO, null, CernícaloPatirrojo);

        db.insert(TABLE_PAJARO, null, Esmerejón);

        db.insert(TABLE_PAJARO, null, AlcotánEuropeo);

        db.insert(TABLE_PAJARO, null, HalcónDeEleonora);

        db.insert(TABLE_PAJARO, null, HalcónBorní);

        db.insert(TABLE_PAJARO, null, HalcónSacre);

        db.insert(TABLE_PAJARO, null, HalcónGerifalte);

        db.insert(TABLE_PAJARO, null, HalcónPeregrino);

        db.insert(TABLE_PAJARO, null, HalcónTagarote);

        db.insert(TABLE_PAJARO, null, RascónEuropeo);

        db.insert(TABLE_PAJARO, null, PolluelaPintoja);

        db.insert(TABLE_PAJARO, null, PolluelaSora);

        db.insert(TABLE_PAJARO, null, PolluelaBastarda);

        db.insert(TABLE_PAJARO, null, PolluelaChica);

        db.insert(TABLE_PAJARO, null, PolluelaCulirroja);

        db.insert(TABLE_PAJARO, null, GuiónAfricano);

        db.insert(TABLE_PAJARO, null, GuiónDeCodornices);

        db.insert(TABLE_PAJARO, null, GallinetaComún);

        db.insert(TABLE_PAJARO, null, GallinetaChica);

        db.insert(TABLE_PAJARO, null, CalamoncilloAfricano);

        db.insert(TABLE_PAJARO, null, CalamoncilloAmericano);

        db.insert(TABLE_PAJARO, null, CalamónComún);

        db.insert(TABLE_PAJARO, null, FochaComún);

        db.insert(TABLE_PAJARO, null, FochaAmericana);

        db.insert(TABLE_PAJARO, null, FochaMoruna);

        db.insert(TABLE_PAJARO, null, TorilloAndaluz);

        db.insert(TABLE_PAJARO, null, GrullaComún);

        db.insert(TABLE_PAJARO, null, GrullaCanadiense);

        db.insert(TABLE_PAJARO, null, GrullaDamisela);

        db.insert(TABLE_PAJARO, null, SisónComún);

        db.insert(TABLE_PAJARO, null, AvutardaHubara);

        db.insert(TABLE_PAJARO, null, AvutardaComún);

        db.insert(TABLE_PAJARO, null, OstreroEuroasiático);

        db.insert(TABLE_PAJARO, null, OstreroNegroCanario);

        db.insert(TABLE_PAJARO, null, CigüeñuelaComún);

        db.insert(TABLE_PAJARO, null, AvocetaComún);

        db.insert(TABLE_PAJARO, null, AlcaravánComún);

        db.insert(TABLE_PAJARO, null, Pluvial);

        db.insert(TABLE_PAJARO, null, CorredorSahariano);

        db.insert(TABLE_PAJARO, null, CanasteraComún);

        db.insert(TABLE_PAJARO, null, CanasteraAlinegra);

        db.insert(TABLE_PAJARO, null, ChorlitejoChico);

        db.insert(TABLE_PAJARO, null, ChorlitejoGrande);

        db.insert(TABLE_PAJARO, null, ChorlitejoSemipalmeado);

        db.insert(TABLE_PAJARO, null, ChorlitejoCulirrojo);

        db.insert(TABLE_PAJARO, null, ChorlitejoPecuario);

        db.insert(TABLE_PAJARO, null, ChorlitejoPatinegro);

        db.insert(TABLE_PAJARO, null, ChorlitejoMongolChico);

        db.insert(TABLE_PAJARO, null, ChorlitejoMongolGrande);

        db.insert(TABLE_PAJARO, null, ChorlitoCarambolo);

        db.insert(TABLE_PAJARO, null, ChorlitoDoradoSiberiano);

        db.insert(TABLE_PAJARO, null, ChorlitoDoradoAmericano);

        db.insert(TABLE_PAJARO, null, ChorlitoDoradoEuropeo);

        db.insert(TABLE_PAJARO, null, ChorlitoGris);

        db.insert(TABLE_PAJARO, null, AvefríaSociable);

        db.insert(TABLE_PAJARO, null, AvefríaColiblanca);

        db.insert(TABLE_PAJARO, null, AvefríaEuropea);

        db.insert(TABLE_PAJARO, null, CorrelimosGrande);

        db.insert(TABLE_PAJARO, null, CorrelimosGordo);

        db.insert(TABLE_PAJARO, null, CorrelimosTridáctilo);

        db.insert(TABLE_PAJARO, null, CorrelimosSemipalmeado);

        db.insert(TABLE_PAJARO, null, CorrelimosDeAlaska);

        db.insert(TABLE_PAJARO, null, CorrelimosMenudo);

        db.insert(TABLE_PAJARO, null, CorrelimosDeTemminck);

        db.insert(TABLE_PAJARO, null, CorrelimosMenudillo);

        db.insert(TABLE_PAJARO, null, CorrelimosCuliblanco);

        db.insert(TABLE_PAJARO, null, CorrelimosDeBairdi);

        db.insert(TABLE_PAJARO, null, CorrelimosPectoral);

        db.insert(TABLE_PAJARO, null, CorrelimosAcuminado);

        db.insert(TABLE_PAJARO, null, CorrelimosZarapitín);

        db.insert(TABLE_PAJARO, null, CorrelimosOscuro);

        db.insert(TABLE_PAJARO, null, CorrelimosComún);

        db.insert(TABLE_PAJARO, null, CorrelimosZancolín);

        db.insert(TABLE_PAJARO, null, CorrelimosFalcinelo);

        db.insert(TABLE_PAJARO, null, CorrelimosCanelo);

        db.insert(TABLE_PAJARO, null, Combatiente);

        db.insert(TABLE_PAJARO, null, AgachadizaChica);

        db.insert(TABLE_PAJARO, null, AgachadizaComún);

        db.insert(TABLE_PAJARO, null, AgachadizaReal);

        db.insert(TABLE_PAJARO, null, AgujetaEscolopácea);

        db.insert(TABLE_PAJARO, null, ChochaPerdiz);

        db.insert(TABLE_PAJARO, null, AgujaColinegra);

        db.insert(TABLE_PAJARO, null, AgujaColipinta);

        db.insert(TABLE_PAJARO, null, ZarapitoDeHudson);

        db.insert(TABLE_PAJARO, null, ZarapitoTrinador);

        db.insert(TABLE_PAJARO, null, ZarapitoFino);

        db.insert(TABLE_PAJARO, null, ZarapitoReal);

        db.insert(TABLE_PAJARO, null, CorrelimosBatitú);

        db.insert(TABLE_PAJARO, null, AndarríosDelTerek);

        db.insert(TABLE_PAJARO, null, AndarríosChico);

        db.insert(TABLE_PAJARO, null, AndarríosMaculado);

        db.insert(TABLE_PAJARO, null, AndarríosGrande);

        db.insert(TABLE_PAJARO, null, AndarríosSolitario);

        db.insert(TABLE_PAJARO, null, ArchibebeOscuro);

        db.insert(TABLE_PAJARO, null, ArchibebePatigualdoGrande);

        db.insert(TABLE_PAJARO, null, ArchibebeClaro);

        db.insert(TABLE_PAJARO, null, ArchibebePatigualdoChico);

        db.insert(TABLE_PAJARO, null, ArchibebeFino);

        db.insert(TABLE_PAJARO, null, AndarríosBastardo);

        db.insert(TABLE_PAJARO, null, ArchibebeComún);

        db.insert(TABLE_PAJARO, null, VuelvepiedrasComún);

        db.insert(TABLE_PAJARO, null, FalaropoTricolor);

        db.insert(TABLE_PAJARO, null, FalaropoPicofino);

        db.insert(TABLE_PAJARO, null, FalaropoPicogrueso);

        db.insert(TABLE_PAJARO, null, PágaloParasito);

        db.insert(TABLE_PAJARO, null, PágaloRabero);

        db.insert(TABLE_PAJARO, null, PágaloGrande);

        db.insert(TABLE_PAJARO, null, PágaloPolar);

        db.insert(TABLE_PAJARO, null, GaviotaDeBonaparte);

        db.insert(TABLE_PAJARO, null, GaviotaCabecigrís);

        db.insert(TABLE_PAJARO, null, GaviotaPicofina);

        db.insert(TABLE_PAJARO, null, GaviotaCabecinegra);

        db.insert(TABLE_PAJARO, null, GaviotaGuanaguanare);

        db.insert(TABLE_PAJARO, null, GaviotaPipizcan);

        db.insert(TABLE_PAJARO, null, GaviotaDeAudouin);

        db.insert(TABLE_PAJARO, null, GaviotaDeDelaware);

        db.insert(TABLE_PAJARO, null, GaviotaCana);

        db.insert(TABLE_PAJARO, null, GaviotaSombría);

        db.insert(TABLE_PAJARO, null, GaviotaArgénteaEuropea);

        db.insert(TABLE_PAJARO, null, GaviotaArgénteaAmericana);

        db.insert(TABLE_PAJARO, null, GaviotaDelCaspio);

        db.insert(TABLE_PAJARO, null, GaviotaPatiamarilla);

        db.insert(TABLE_PAJARO, null, GaviotaDeBering);

        db.insert(TABLE_PAJARO, null, GaviotaGroenlandesa);

        db.insert(TABLE_PAJARO, null, GaviónHiperbóreo);

        db.insert(TABLE_PAJARO, null, GaviónAtlántico);

        db.insert(TABLE_PAJARO, null, GaviotaCocinera);

        db.insert(TABLE_PAJARO, null, GaviotaRosada);

        db.insert(TABLE_PAJARO, null, GaviotaTridáctila);

        db.insert(TABLE_PAJARO, null, GaviotaDeSabine);

        db.insert(TABLE_PAJARO, null, GaviotaEnana);

        db.insert(TABLE_PAJARO, null, PagazaPiconegra);

        db.insert(TABLE_PAJARO, null, PagazaPiquirroja);

        db.insert(TABLE_PAJARO, null, CharránBengalí);

        db.insert(TABLE_PAJARO, null, CharránPatinegro);

        db.insert(TABLE_PAJARO, null, CharránElegante);

        db.insert(TABLE_PAJARO, null, CharránRosado);

        db.insert(TABLE_PAJARO, null, CharránComún);

        db.insert(TABLE_PAJARO, null, CharránÁrtico);

        db.insert(TABLE_PAJARO, null, CharránDeForster);

        db.insert(TABLE_PAJARO, null, CharránEmbridado);

        db.insert(TABLE_PAJARO, null, CharránSombrío);

        db.insert(TABLE_PAJARO, null, CharrancitoComún);

        db.insert(TABLE_PAJARO, null, FumarelCariblanco);

        db.insert(TABLE_PAJARO, null, FumarelComún);

        db.insert(TABLE_PAJARO, null, FumarelAliblanco);

        db.insert(TABLE_PAJARO, null, AraoComún);

        db.insert(TABLE_PAJARO, null, AlcaComún);

        db.insert(TABLE_PAJARO, null, AraoAliblanco);

        db.insert(TABLE_PAJARO, null, FrailecilloAtlántico);

        db.insert(TABLE_PAJARO, null, GangaOrtega);

        db.insert(TABLE_PAJARO, null, GangaIbérica);

        db.insert(TABLE_PAJARO, null, GangaDePallas);

        db.insert(TABLE_PAJARO, null, PalomaBravía);

        db.insert(TABLE_PAJARO, null, PalomaTorcaz);

        db.insert(TABLE_PAJARO, null, PalomaTurqué);

        db.insert(TABLE_PAJARO, null, PalomaRabiche);

        db.insert(TABLE_PAJARO, null, TórtolaTurca);

        db.insert(TABLE_PAJARO, null, TórtolaEuropea);

        db.insert(TABLE_PAJARO, null, TórtolaOriental);

        db.insert(TABLE_PAJARO, null, TórtolaSenegalesa);

        db.insert(TABLE_PAJARO, null, CotorraDeKramer);

        db.insert(TABLE_PAJARO, null, CotorraArgentina);

        db.insert(TABLE_PAJARO, null, CríaloEuropeo);

        db.insert(TABLE_PAJARO, null, CucoComún);

        db.insert(TABLE_PAJARO, null, CuclilloPiquigualdo);

        db.insert(TABLE_PAJARO, null, LechuzaComún);

        db.insert(TABLE_PAJARO, null, BúhoReal);

        db.insert(TABLE_PAJARO, null, MochueloComún);

        db.insert(TABLE_PAJARO, null, BúhoChico);

        db.insert(TABLE_PAJARO, null, BúhoCampestre);

        db.insert(TABLE_PAJARO, null, BúhoMoro);

        db.insert(TABLE_PAJARO, null, ChotacabrasEuropeo);

        db.insert(TABLE_PAJARO, null, AñaperoYanqui);

        db.insert(TABLE_PAJARO, null, VencejoDeChimenea);

        db.insert(TABLE_PAJARO, null, VencejoUnicolor);

        db.insert(TABLE_PAJARO, null, VencejoComún);

        db.insert(TABLE_PAJARO, null, VencejoPálido);

        db.insert(TABLE_PAJARO, null, VencejoReal);

        db.insert(TABLE_PAJARO, null, VencejoCafre);

        db.insert(TABLE_PAJARO, null, VencejoMoro);

        db.insert(TABLE_PAJARO, null, MartínGiganteNorteamericano);

        db.insert(TABLE_PAJARO, null, AbejarucoPersa);

        db.insert(TABLE_PAJARO, null, AbejarucoEuropeo);

        db.insert(TABLE_PAJARO, null, Abubilla);

        db.insert(TABLE_PAJARO, null, TorcecuelloEuroasiático);

        db.insert(TABLE_PAJARO, null, PitoReal);

        db.insert(TABLE_PAJARO, null, PicamaderosNegro);

        db.insert(TABLE_PAJARO, null, PicoPicapinos);

        db.insert(TABLE_PAJARO, null, PicoMediano);

        db.insert(TABLE_PAJARO, null, PicoMenor);

        db.insert(TABLE_PAJARO, null, TijeretaSabanera);

        db.insert(TABLE_PAJARO, null, TerreraColinegra);

        db.insert(TABLE_PAJARO, null, AlondraRicotí);

        db.insert(TABLE_PAJARO, null, CalandriaComún);

        db.insert(TABLE_PAJARO, null, TerreraMarismeña);

        db.insert(TABLE_PAJARO, null, CogujadaMontesina);

        db.insert(TABLE_PAJARO, null, AlondraComún);

        db.insert(TABLE_PAJARO, null, AlondraCornuda);

        db.insert(TABLE_PAJARO, null, AviónZapador);

        db.insert(TABLE_PAJARO, null, AviónRoquero);

        db.insert(TABLE_PAJARO, null, GolondrinaComún);

        db.insert(TABLE_PAJARO, null, GolondrinaDáurica);

        db.insert(TABLE_PAJARO, null, AviónComún);

        db.insert(TABLE_PAJARO, null, BisbitaEstepario);

        db.insert(TABLE_PAJARO, null, BisbitaCaminero);

        db.insert(TABLE_PAJARO, null, BisbitaDeHodgson);

        db.insert(TABLE_PAJARO, null, BisbitaArbóreo);

        db.insert(TABLE_PAJARO, null, BisbitaPratense);

        db.insert(TABLE_PAJARO, null, BisbitaGorgirrojo);

        db.insert(TABLE_PAJARO, null, BisbitaNorteamericano);

        db.insert(TABLE_PAJARO, null, BisbitaAlpino);

        db.insert(TABLE_PAJARO, null, BisbitaCostero);

        db.insert(TABLE_PAJARO, null, LavanderaBoyera);

        db.insert(TABLE_PAJARO, null, LavanderaCetrina);

        db.insert(TABLE_PAJARO, null, LavanderaCascadeña);

        db.insert(TABLE_PAJARO, null, BulbulNaranjero);

        db.insert(TABLE_PAJARO, null, AmpelisEuropeo);

        db.insert(TABLE_PAJARO, null, MirloAcuáticoEuropeo);

        db.insert(TABLE_PAJARO, null, ChochínComún);

        db.insert(TABLE_PAJARO, null, PájaroGatoGris);

        db.insert(TABLE_PAJARO, null, AcentorComún);

        db.insert(TABLE_PAJARO, null, AcentorAlpino);

        db.insert(TABLE_PAJARO, null, AlzacolaRojizo);

        db.insert(TABLE_PAJARO, null, PetirrojoEuropeo);

        db.insert(TABLE_PAJARO, null, RuiseñorComúns);

        db.insert(TABLE_PAJARO, null, RuiseñorPechiazul);

        db.insert(TABLE_PAJARO, null, RuiseñorAzul);

        db.insert(TABLE_PAJARO, null, RuiseñorColiazul);

        db.insert(TABLE_PAJARO, null, ColirrojoTizón);

        db.insert(TABLE_PAJARO, null, ColirrojoReal);

        db.insert(TABLE_PAJARO, null, ColirrojoDiademado);

        db.insert(TABLE_PAJARO, null, TarabillaNorteña);

        db.insert(TABLE_PAJARO, null, TarabillaSiberiana);

        db.insert(TABLE_PAJARO, null, TarabillaCanaria);

        db.insert(TABLE_PAJARO, null, CollalbaIsabel);

        db.insert(TABLE_PAJARO, null, CollalbaRubia);

        db.insert(TABLE_PAJARO, null, CollalbaYebélica);

        db.insert(TABLE_PAJARO, null, RoqueroRojo);

        db.insert(TABLE_PAJARO, null, ZorzalDoradoDelHimalaya);

        db.insert(TABLE_PAJARO, null, MirloCapiblanco);

        db.insert(TABLE_PAJARO, null, MirloComún);

        db.insert(TABLE_PAJARO, null, ZorzalDeNaumann);

        db.insert(TABLE_PAJARO, null, ZorzalReal);

        db.insert(TABLE_PAJARO, null, ZorzalComún);

        db.insert(TABLE_PAJARO, null, ZorzalAlirrojo);

        db.insert(TABLE_PAJARO, null, ZorzalCharlo);

        db.insert(TABLE_PAJARO, null, ZorzalRobín);

        db.insert(TABLE_PAJARO, null, CetiaRuiseñor);

        db.insert(TABLE_PAJARO, null, CistícolaBuitrón);

        db.insert(TABLE_PAJARO, null, BuscarlaPintoja);

        db.insert(TABLE_PAJARO, null, BuscarlaFluvial);

        db.insert(TABLE_PAJARO, null, BuscarlaUnicolor);

        db.insert(TABLE_PAJARO, null, ZarceroEscita);

        db.insert(TABLE_PAJARO, null, ZarceroBereber);

        db.insert(TABLE_PAJARO, null, CarricerínReal);

        db.insert(TABLE_PAJARO, null, CarricerínCejudo);

        db.insert(TABLE_PAJARO, null, CarricerínComún);

        db.insert(TABLE_PAJARO, null, CarriceroAgrícola);

        db.insert(TABLE_PAJARO, null, CarriceroDeBlyth);

        db.insert(TABLE_PAJARO, null, CarriceroPolíglota);

        db.insert(TABLE_PAJARO, null, CarriceroComún);

        db.insert(TABLE_PAJARO, null, CarriceroTordal);

        db.insert(TABLE_PAJARO, null, ZarceroIcterino);

        db.insert(TABLE_PAJARO, null, ZarceroPolíglota);

        db.insert(TABLE_PAJARO, null, CurrucaSarda);

        db.insert(TABLE_PAJARO, null, CurrucaBalear);

        db.insert(TABLE_PAJARO, null, CurrucaRabilarga);

        db.insert(TABLE_PAJARO, null, CurrucaDelAtlas);

        db.insert(TABLE_PAJARO, null, CurrucaTomillera);

        db.insert(TABLE_PAJARO, null, CurrucaCarrasqueña);

        db.insert(TABLE_PAJARO, null, CurrucaCabecinegra);

        db.insert(TABLE_PAJARO, null, CurrucaDeRuppell);

        db.insert(TABLE_PAJARO, null, CurrucaSahariana);

        db.insert(TABLE_PAJARO, null, CurrucaMirlona);

        db.insert(TABLE_PAJARO, null, CurrucaGavilana);

        db.insert(TABLE_PAJARO, null, CurrucaZarcerilla);

        db.insert(TABLE_PAJARO, null, CurrucaZarcera);

        db.insert(TABLE_PAJARO, null, CurrucaMosquitera);

        db.insert(TABLE_PAJARO, null, CurrucaCapirotada);

        db.insert(TABLE_PAJARO, null, MosquiteroVerdoso);

        db.insert(TABLE_PAJARO, null, MosquiteroBoreal);

        db.insert(TABLE_PAJARO, null, MosquiteroDePallas);

        db.insert(TABLE_PAJARO, null, MosquiteroBilistado);

        db.insert(TABLE_PAJARO, null, MosquiteroDeHume);

        db.insert(TABLE_PAJARO, null, MosquiteroDeSchwarz);

        db.insert(TABLE_PAJARO, null, MosquiteroSombrío);

        db.insert(TABLE_PAJARO, null, MosquiteroPapialbo);

        db.insert(TABLE_PAJARO, null, MosquiteroSilbador);

        db.insert(TABLE_PAJARO, null, MosquiteroComún);

        db.insert(TABLE_PAJARO, null, MosquiteroIbérico);

        db.insert(TABLE_PAJARO, null, MosquiteroCanario);

        db.insert(TABLE_PAJARO, null, MosquiteroMusical);

        db.insert(TABLE_PAJARO, null, ReyezueloSencillo);

        db.insert(TABLE_PAJARO, null, ReyezueloListado);

        db.insert(TABLE_PAJARO, null, PapamoscasGris);

        db.insert(TABLE_PAJARO, null, PapamoscasPapirrojo);

        db.insert(TABLE_PAJARO, null, PapamoscasSemicollarino);

        db.insert(TABLE_PAJARO, null, PapamoscasAcollarado);

        db.insert(TABLE_PAJARO, null, PapamoscasCerrojillo);

        db.insert(TABLE_PAJARO, null, Bigotudo);

        db.insert(TABLE_PAJARO, null, LeiotrixPiquirrojo);

        db.insert(TABLE_PAJARO, null, MitoComún);

        db.insert(TABLE_PAJARO, null, CarboneroPalustre);

        db.insert(TABLE_PAJARO, null, HerrerilloCapuchino);

        db.insert(TABLE_PAJARO, null, CarboneroGarrapinos);

        db.insert(TABLE_PAJARO, null, HerrerilloComún);

        db.insert(TABLE_PAJARO, null, HerrerilloCanario);

        db.insert(TABLE_PAJARO, null, CarboneroComún);

        db.insert(TABLE_PAJARO, null, TrepadorAzul);

        db.insert(TABLE_PAJARO, null, Treparriscos);

        db.insert(TABLE_PAJARO, null, AgateadorEuroasiático);

        db.insert(TABLE_PAJARO, null, AgateadorEuropeo);

        db.insert(TABLE_PAJARO, null, PájaroMoscónEuropeo);

        db.insert(TABLE_PAJARO, null, OropéndolaEuropea);

        db.insert(TABLE_PAJARO, null, ChagraDelSenegal);

        db.insert(TABLE_PAJARO, null, AlcaudónIsabel);

        db.insert(TABLE_PAJARO, null, AlcaudónDorsirrojo);

        db.insert(TABLE_PAJARO, null, AlcaudónChico);

        db.insert(TABLE_PAJARO, null, AlcaudónNorteño);

        db.insert(TABLE_PAJARO, null, AlcaudónReal);

        db.insert(TABLE_PAJARO, null, AlcaudónComún);

        db.insert(TABLE_PAJARO, null, AlcaudónNúbico);

        db.insert(TABLE_PAJARO, null, ArrendajoEuroasiático);

        db.insert(TABLE_PAJARO, null, RabilargoIbérico);

        db.insert(TABLE_PAJARO, null, UrracaComún);

        db.insert(TABLE_PAJARO, null, CascanuecesComún);

        db.insert(TABLE_PAJARO, null, ChovaPiquigualda);

        db.insert(TABLE_PAJARO, null, ChovaPiquirroja);

        db.insert(TABLE_PAJARO, null, GrajillaOccidental);

        db.insert(TABLE_PAJARO, null, Graja);

        db.insert(TABLE_PAJARO, null, CornejaNegra);

        db.insert(TABLE_PAJARO, null, CornejaCenicienta);

        db.insert(TABLE_PAJARO, null, CuervoGrande);

        db.insert(TABLE_PAJARO, null, EstorninoNegro);

        db.insert(TABLE_PAJARO, null, EstorninoPinto);

        db.insert(TABLE_PAJARO, null, EstorninoRosado);

        db.insert(TABLE_PAJARO, null, GorriónComún);

        db.insert(TABLE_PAJARO, null, GorriónMoruno);

        db.insert(TABLE_PAJARO, null, GorriónMolinero);

        db.insert(TABLE_PAJARO, null, GorriónChillón);

        db.insert(TABLE_PAJARO, null, GorriónAlpino);

        db.insert(TABLE_PAJARO, null, EstrildaComún);

        db.insert(TABLE_PAJARO, null, BengalíRojo);

        db.insert(TABLE_PAJARO, null, VireoChiví);

        db.insert(TABLE_PAJARO, null, PinzónVulgar);

        db.insert(TABLE_PAJARO, null, PinzónAzul);

        db.insert(TABLE_PAJARO, null, PinzónReal);

        db.insert(TABLE_PAJARO, null, SerínVerdecillo);

        db.insert(TABLE_PAJARO, null, SerínCanario);

        db.insert(TABLE_PAJARO, null, VerderónComún);

        db.insert(TABLE_PAJARO, null, VerderónSerrano);

        db.insert(TABLE_PAJARO, null, JilgueroEuropeo);

        db.insert(TABLE_PAJARO, null, JilgueroLúgano);

        db.insert(TABLE_PAJARO, null, PardilloComún);

        db.insert(TABLE_PAJARO, null, PardilloPiquigualdo);

        db.insert(TABLE_PAJARO, null, PardilloSizerín);

        db.insert(TABLE_PAJARO, null, PiquituertoComún);

        db.insert(TABLE_PAJARO, null, CamachueloTrompetero);

        db.insert(TABLE_PAJARO, null, CamachueloCarminoso);

        db.insert(TABLE_PAJARO, null, CamachueloComún);

        db.insert(TABLE_PAJARO, null, PicogordoComún);

        db.insert(TABLE_PAJARO, null, ReinitaTrepadora);

        db.insert(TABLE_PAJARO, null, ReinitaCoronada);

        db.insert(TABLE_PAJARO, null, ReinitaCharqueraDeLuisiana);

        db.insert(TABLE_PAJARO, null, EscribanoLapón);

        db.insert(TABLE_PAJARO, null, EscribanoNival);

        db.insert(TABLE_PAJARO, null, EscribanoCabeciblanco);

        db.insert(TABLE_PAJARO, null, EscribanoCerillo);

        db.insert(TABLE_PAJARO, null, EscribanoSoteño);

        db.insert(TABLE_PAJARO, null, EscribanoMontesino);

        db.insert(TABLE_PAJARO, null, EscribanoSahariano);

        db.insert(TABLE_PAJARO, null, EscribanoHortelano);

        db.insert(TABLE_PAJARO, null, EscribanoCeniciento);

        db.insert(TABLE_PAJARO, null, EscribanoRústico);

        db.insert(TABLE_PAJARO, null, EscribanoPigmeo);

        db.insert(TABLE_PAJARO, null, EscribanoAureolado);

        db.insert(TABLE_PAJARO, null, EscribanoPalustre);

        db.insert(TABLE_PAJARO, null, EscribanoCarirrojo);

        db.insert(TABLE_PAJARO, null, EscribanoCabecinegro);

        db.insert(TABLE_PAJARO, null, EscribanoTriguero);

        db.insert(TABLE_PAJARO, null, Charlatán);

        db.insert(TABLE_PAJARO, null, PetrelFreira);

        db.insert(TABLE_PAJARO, null, PetrelGonGon);

        db.insert(TABLE_PAJARO, null, GolondrinaPueblera);

        db.insert(TABLE_PAJARO, null, GolondrinaRisquera);

        db.insert(TABLE_PAJARO, null, SuiriríBicolor);

        db.insert(TABLE_PAJARO, null, SuiriríCariblanco);

        db.insert(TABLE_PAJARO, null, NsarNival);

        db.insert(TABLE_PAJARO, null, BarnaclaCanadienseChica);

        db.insert(TABLE_PAJARO, null, PatoJoyuyo);

        db.insert(TABLE_PAJARO, null, CercetaColorada);

        db.insert(TABLE_PAJARO, null, CormoránAfricano);

        db.insert(TABLE_PAJARO, null, TántaloAfricano);

        db.insert(TABLE_PAJARO, null, EspátulaAfricana);

        db.insert(TABLE_PAJARO, null, AlimocheSombrío);

        db.insert(TABLE_PAJARO, null, TortolitaRabilarga);

        db.insert(TABLE_PAJARO, null, SinsonteNorteño);

        db.insert(TABLE_PAJARO, null, CamachueloDesertícola);

        db.insert(TABLE_PAJARO, null, EscribanoDeBrandt);


    }

}
