package alvaro.vizzor.helper;

import java.util.ArrayList;

import alvaro.vizzor.model.Cita;
import alvaro.vizzor.model.Paraje;
import alvaro.vizzor.model.Usuario;

public class Singleton {
    private static Singleton singleton = null;

    private Usuario usuario;
    private Cita cita;
    private Paraje paraje;
    private String origen;

    private ArrayList<Paraje> parajes;

    private Singleton(){
    }

    public static Singleton getInstance(){
        if(singleton == null)
        {
            singleton = new Singleton();
        }
        return singleton;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cita getCita() {
        return cita;
    }

    public void setCita(Cita cita) {
        this.cita = cita;
    }

    public Paraje getParaje() {
        return paraje;
    }

    public void setParaje(Paraje paraje) {
        this.paraje = paraje;
    }

    public ArrayList<Paraje> getParajes() {
        return parajes;
    }

    public void setParajes(ArrayList<Paraje> parajes) {
        this.parajes = parajes;
    }

    public void cleanParajes() {
        parajes.clear();
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
}

