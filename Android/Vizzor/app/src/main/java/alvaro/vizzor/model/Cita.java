package alvaro.vizzor.model;


import android.graphics.Bitmap;

import alvaro.vizzor.helper.Util;

public class Cita {
    private Integer id;
    private String usuario;
    private String especie;
    private String latitud;
    private String longitud;
    private String paraje;
    private String municipio;
    private String numero;
    private String edad;
    private String sexo;
    private String interes;
    private String observaciones;
    private String privacidad;
    private String filtro;
    private Bitmap imagen;
    private String fecha;
    private int status;
    private String token;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getParaje() {
        return paraje;
    }

    public void setParaje(String paraje) {
        this.paraje = paraje;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getInteres() {
        return interes;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getPrivacidad() {
        return privacidad;
    }

    public void setPrivacidad(String privacidad) {
        this.privacidad = privacidad;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Cita{" +
                "id=" + id +
                ", usuario='" + usuario + '\'' +
                ", especie='" + especie + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                ", paraje='" + paraje + '\'' +
                ", municipio='" + municipio + '\'' +
                ", numero='" + numero + '\'' +
                ", edad='" + edad + '\'' +
                ", sexo='" + sexo + '\'' +
                ", interes='" + interes + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", privacidad='" + privacidad + '\'' +
                ", filtro='" + filtro + '\'' +
                ", imagen=" + imagen +
                ", fecha='" + fecha + '\'' +
                ", status=" + status +
                ", token='" + token + '\'' +
                '}';
    }

    public String compartir() {
        return "Especie: " + especie + '\n' +
                "Paraje: " + paraje + '\n' +
                "Municipio: " + municipio + '\n' +
                "Coordenadas: " + Util.convertCoordenadas(Double.parseDouble(latitud), Double.parseDouble(longitud)) + '\n' +
                "Numero: " + numero + '\n' +
                "Edad: " + edad + '\n' +
                "Sexo: " + sexo + '\n' +
                "Interes: " + interes + '\n' +
                "Observaciones: " + observaciones + '\n' +
                "Privacidad: " + privacidad + '\n' +
                "Filtro: " + filtro + '\n' +
                "Fecha: " + fecha + '\n' +
                "Creador: " + usuario + '\n' +
                "Ubicación: " + "http://maps.google.com/maps?q=loc:" + latitud + "," + longitud + '\n';
    }
}
