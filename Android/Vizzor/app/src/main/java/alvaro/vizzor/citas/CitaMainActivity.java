package alvaro.vizzor.citas;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import alvaro.vizzor.AyudaActivity;
import alvaro.vizzor.InformacionActivity;
import alvaro.vizzor.MainActivity;
import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.helper.VolleySingleton;
import alvaro.vizzor.misCitas.miCitaMainActivity;
import alvaro.vizzor.model.Cita;
import alvaro.vizzor.parajes.ParajeMainActivity;
import alvaro.vizzor.sql.DAOLocal;
import alvaro.vizzor.usuario.UsuarioDetallesActivity;

import static alvaro.vizzor.helper.Config.RC_DIALOG_ALERT;
import static alvaro.vizzor.helper.Config.URL_GET_CITAS;


public class CitaMainActivity extends AppCompatActivity {
    private Context context;

    private DrawerLayout drawerLayout;
    private NavigationView navView;

    private ImageView ivCircle;
    private TextView txtUsername;

    private RecyclerView recView;
    private ArrayList<Cita> datos;
    private ArrayList<Cita> filtro;
    private CitaAdaptadorLista adaptador;

    private SwipeRefreshLayout refreshLayout;
    private SearchView search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cita_main);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("cita_main", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Singleton.getInstance().setOrigen("cita");

        Log.e("cita_main", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_nav_menu);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(true);

        collapsingToolbar.setTitle(getString(R.string.citas));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navView = (NavigationView) findViewById(R.id.navview);

        View nav = navView.getHeaderView(0);

        ivCircle = (ImageView)nav.findViewById(R.id.ivCircle);
        txtUsername = (TextView) nav.findViewById(R.id.txtUsername);

        txtUsername.setText(Singleton.getInstance().getUsuario().getEmail());
        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .into(ivCircle);

        datos = new ArrayList<>();
        filtro = new ArrayList<>();

        adaptador = new CitaAdaptadorLista(context, datos);

        search = (SearchView) findViewById(R.id.search);
        search.setIconified(false);
        search.clearFocus();

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

        recView = (RecyclerView) findViewById(R.id.cita_listar_recyclerView);
        recView.setHasFixedSize(true);

        recView.setAdapter(adaptador);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recView.setLayoutManager(linearLayoutManager);


        showDialog(RC_DIALOG_ALERT);

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        showDialog(RC_DIALOG_ALERT);
                        refreshLayout.setRefreshing(false);
                    }
                }
        );

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();
                filtro.clear();
                for (int i = 0; i < datos.size(); i++) {

                    final String text = datos.get(i).toString().toLowerCase();
                    if (text.contains(query)) {
                        filtro.add(datos.get(i));
                    }
                }

                adaptador = new CitaAdaptadorLista(context, filtro);
                recView.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
                return true;
            }
        });

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.menu_inicio:
                                Intent intentoMain = new Intent(context, MainActivity.class);
                                startActivity(intentoMain);
                                finish();
                                break;
                            case R.id.menu_mis_citas:
                                Intent intentoMiCita = new Intent(context, miCitaMainActivity.class);
                                startActivity(intentoMiCita);
                                finish();
                                break;
                            case R.id.menu_citas:
                                Intent intentoCita = new Intent(context, CitaMainActivity.class);
                                startActivity(intentoCita);
                                finish();
                                break;
                            case R.id.menu_parajes:
                                Intent intentoParajes = new Intent(context, ParajeMainActivity.class);
                                startActivity(intentoParajes);
                                finish();
                                break;
                            case R.id.menu_perfil:
                                Intent intentoPerfil = new Intent(context, UsuarioDetallesActivity.class);
                                startActivity(intentoPerfil);
                                finish();
                                break;
                            case R.id.menu_informacion:
                                Intent intentoInformacion = new Intent(context, InformacionActivity.class);
                                startActivity(intentoInformacion);
                                finish();
                                break;
                            case R.id.menu_ayuda:
                                Intent intentoAyuda = new Intent(context, AyudaActivity.class);
                                startActivity(intentoAyuda);
                                finish();
                                break;
                            case R.id.menu_salir:
                                Util.logout(context, CitaMainActivity.this);
                                finish();
                                break;
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case RC_DIALOG_ALERT:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.dialog_sincronizar));
                builder.setCancelable(false);
                builder.setPositiveButton(getString(R.string.dialog_si), new OkOnClickListener());
                builder.setNegativeButton(getString(R.string.dialog_no), new CancelOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class CancelOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            loadCitasLocales();
        }
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
            loadCitasOnline();
        }
    }

    private void loadCitasLocales() {
        datos.clear();
        datos.addAll(DAOLocal.getAllCitasOnline(context));
        adaptador = new CitaAdaptadorLista(context, datos);
        recView.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    private void loadCitasOnline() {
        //NukeSSLCerts.nuke();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.procesando));
        progressDialog.show();
        datos.clear();
        DAOLocal.delCitasOnline(context);
        Log.e("DAORemoto", "getCitas");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_GET_CITAS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (Objects.equals(obj.getString("error"), "false")) {
                                JSONArray arr = obj.getJSONArray("cita");
                                for(int i=0;i<arr.length();i++){
                                    JSONObject jso = arr.getJSONObject(i);
                                    Cita cita = new Cita();
                                    cita.setUsuario(jso.getString("usuario"));
                                    cita.setEspecie(jso.getString("especie"));
                                    cita.setLatitud(jso.getString("latitud"));
                                    cita.setLongitud(jso.getString("longitud"));
                                    cita.setParaje(jso.getString("paraje"));
                                    cita.setMunicipio(jso.getString("municipio"));
                                    cita.setNumero(jso.getString("numero"));
                                    cita.setEdad(jso.getString("edad"));
                                    cita.setSexo(jso.getString("sexo"));
                                    cita.setInteres(jso.getString("interes"));
                                    cita.setObservaciones(jso.getString("observaciones"));
                                    cita.setPrivacidad(jso.getString("privacidad"));
                                    cita.setFiltro(jso.getString("filtro"));
                                    cita.setFecha(jso.getString("fecha"));

                                    String imagenBlob = jso.getString("imagen");
                                    byte[] imageBase64 = Base64.decode(imagenBlob, Base64.DEFAULT);
                                    Bitmap imagen = Util.getImage(imageBase64);
                                    cita.setImagen(imagen);

                                    cita.setToken(jso.getString("token"));

                                    DAOLocal.addCitaOnline(cita, CitaMainActivity.this);

                                    datos.add(cita);
                                }
                                adaptador = new CitaAdaptadorLista(CitaMainActivity.this, datos);
                                recView.setAdapter(adaptador);
                                adaptador.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onBackPressed() {
        Intent intentoMain = new Intent(context, MainActivity.class);
        startActivity(intentoMain);
        finish();
    }


}


