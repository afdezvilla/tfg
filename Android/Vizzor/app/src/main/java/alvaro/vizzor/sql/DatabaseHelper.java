package alvaro.vizzor.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.List;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.model.Cita;
import alvaro.vizzor.model.Pajaro;
import alvaro.vizzor.model.Paraje;
import alvaro.vizzor.model.Usuario;

import static alvaro.vizzor.helper.Config.RC_NOT_DELETE;
import static alvaro.vizzor.helper.Config.RC_NOT_SYNC;
import static alvaro.vizzor.helper.Config.RC_NOT_UPDATE;
import static alvaro.vizzor.sql.PopulatePajaros.poblarPajaros;
import static alvaro.vizzor.sql.PopulateParajes.poblarParajes;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Vizzor.db";

    private static final String TABLE_USER = "usuarios";
    private static final String TABLE_CITA = "citas";
    private static final String TABLE_PARAJE = "parajes";
    private static final String TABLE_CITA_ONLINE = "citasOnline";
    private static final String TABLE_PAJARO = "pajaros";

    public static final String COLUMN_USER_ID = "id";
    public static final String COLUMN_USER_NOMBRE = "nombre";
    public static final String COLUMN_USER_DOMICILIO = "domicilio";
    public static final String COLUMN_USER_EMAIL = "email";
    public static final String COLUMN_USER_TELEFONO = "telefono";
    public static final String COLUMN_USER_IDENTIFICADOR = "identificador";
    public static final String COLUMN_USER_IMAGEN = "imagen";
    public static final String COLUMN_USER_TOKEN = "token";

    public static final String COLUMN_CITA_ID = "id";
    public static final String COLUMN_CITA_USERNAME = "usuario";
    public static final String COLUMN_CITA_ESPECIE = "especie";
    public static final String COLUMN_CITA_LATITUD = "latitud";
    public static final String COLUMN_CITA_LONGITUD = "longitud";
    public static final String COLUMN_CITA_PARAJE = "paraje";
    public static final String COLUMN_CITA_MUNICIPIO = "municipio";
    public static final String COLUMN_CITA_NUMERO = "numero";
    public static final String COLUMN_CITA_EDAD = "edad";
    public static final String COLUMN_CITA_SEXO = "sexo";
    public static final String COLUMN_CITA_INTERES = "interes";
    public static final String COLUMN_CITA_OBSERVACION = "observaciones";
    public static final String COLUMN_CITA_PRIVACIDAD = "privacidad";
    public static final String COLUMN_CITA_FILTRO = "filtro";
    public static final String COLUMN_CITA_FECHA = "fecha";
    public static final String COLUMN_CITA_IMAGEN = "imagen";
    public static final String COLUMN_CITA_STATUS = "status";
    public static final String COLUMN_CITA_TOKEN = "token";

    public static final String COLUMN_CITA_ONLINE_ID = "id";
    public static final String COLUMN_CITA_ONLINE_USERNAME = "usuario";
    public static final String COLUMN_CITA_ONLINE_ESPECIE = "especie";
    public static final String COLUMN_CITA_ONLINE_LATITUD = "latitud";
    public static final String COLUMN_CITA_ONLINE_LONGITUD = "longitud";
    public static final String COLUMN_CITA_ONLINE_PARAJE = "paraje";
    public static final String COLUMN_CITA_ONLINE_MUNICIPIO = "municipio";
    public static final String COLUMN_CITA_ONLINE_NUMERO = "numero";
    public static final String COLUMN_CITA_ONLINE_EDAD = "edad";
    public static final String COLUMN_CITA_ONLINE_SEXO = "sexo";
    public static final String COLUMN_CITA_ONLINE_INTERES = "interes";
    public static final String COLUMN_CITA_ONLINE_OBSERVACION = "observaciones";
    public static final String COLUMN_CITA_ONLINE_PRIVACIDAD = "privacidad";
    public static final String COLUMN_CITA_ONLINE_FILTRO = "filtro";
    public static final String COLUMN_CITA_ONLINE_FECHA = "fecha";
    public static final String COLUMN_CITA_ONLINE_IMAGEN = "imagen";

    public static final String COLUMN_PARAJE_ID = "id";
    public static final String COLUMN_PARAJE_NOMBRE = "nombre";
    public static final String COLUMN_PARAJE_LATITUD = "latitud";
    public static final String COLUMN_PARAJE_LONGITUD = "longitud";
    public static final String COLUMN_PARAJE_MUNICIPIO = "municipio";
    public static final String COLUMN_PARAJE_INTERES = "interes";
    public static final String COLUMN_PARAJE_RADIO = "radio";
    public static final String COLUMN_PARAJE_IMAGEN = "imagen";

    public static final String COLUMN_PAJARO_ID = "id";
    public static final String COLUMN_PAJARO_NOMBRE_CASTELLANO = "castellano";
    public static final String COLUMN_PAJARO_NOMBRE_CIENTIFICO = "cientifico";


    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NOMBRE + " TEXT,"
            + COLUMN_USER_DOMICILIO + " TEXT,"
            + COLUMN_USER_EMAIL + " TEXT," + COLUMN_USER_TELEFONO + " TEXT,"
            + COLUMN_USER_IDENTIFICADOR + " TEXT," + COLUMN_USER_IMAGEN + " BLOB," + COLUMN_USER_TOKEN + " TEXT" + ")";

    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;

    private String CREATE_CITA_TABLE = "CREATE TABLE " + TABLE_CITA
            + "(" + COLUMN_CITA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_CITA_USERNAME + " TEXT,"
            + COLUMN_CITA_ESPECIE + " TEXT," + COLUMN_CITA_LATITUD + " TEXT,"
            + COLUMN_CITA_LONGITUD + " TEXT," + COLUMN_CITA_PARAJE + " TEXT,"
            + COLUMN_CITA_MUNICIPIO + " TEXT," + COLUMN_CITA_NUMERO + " TEXT,"
            + COLUMN_CITA_EDAD + " TEXT," + COLUMN_CITA_SEXO + " TEXT,"
            + COLUMN_CITA_INTERES + " TEXT," + COLUMN_CITA_OBSERVACION + " TEXT,"
            + COLUMN_CITA_PRIVACIDAD + " TEXT," + COLUMN_CITA_FILTRO + " TEXT,"
            + COLUMN_CITA_FECHA + " TEXT," + COLUMN_CITA_IMAGEN + " BLOB,"
            + COLUMN_CITA_STATUS + " TINYINT," + COLUMN_CITA_TOKEN + " TEXT" + ")";

    private String DROP_CITA_TABLE = "DROP TABLE IF EXISTS " + TABLE_CITA;

    private String CREATE_CITA_ONLINE_TABLE = "CREATE TABLE " + TABLE_CITA_ONLINE
            + "(" + COLUMN_CITA_ONLINE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_CITA_ONLINE_USERNAME + " TEXT,"
            + COLUMN_CITA_ONLINE_ESPECIE + " TEXT," + COLUMN_CITA_ONLINE_LATITUD + " TEXT,"
            + COLUMN_CITA_ONLINE_LONGITUD + " TEXT," + COLUMN_CITA_ONLINE_PARAJE + " TEXT,"
            + COLUMN_CITA_ONLINE_MUNICIPIO + " TEXT," + COLUMN_CITA_ONLINE_NUMERO + " TEXT,"
            + COLUMN_CITA_ONLINE_EDAD + " TEXT," + COLUMN_CITA_ONLINE_SEXO + " TEXT,"
            + COLUMN_CITA_ONLINE_INTERES + " TEXT," + COLUMN_CITA_ONLINE_OBSERVACION + " TEXT,"
            + COLUMN_CITA_ONLINE_PRIVACIDAD + " TEXT," + COLUMN_CITA_ONLINE_FILTRO + " TEXT,"
            + COLUMN_CITA_ONLINE_FECHA + " TEXT," + COLUMN_CITA_ONLINE_IMAGEN + " BLOB" + ")";

    private String DROP_CITA_ONLINE_TABLE = "DROP TABLE IF EXISTS " + TABLE_CITA;

    private String CREATE_PARAJE_TABLE = "CREATE TABLE " + TABLE_PARAJE
            + "(" + COLUMN_PARAJE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_PARAJE_NOMBRE + " TEXT,"
            + COLUMN_PARAJE_LATITUD + " TEXT," + COLUMN_PARAJE_LONGITUD + " TEXT,"
            + COLUMN_PARAJE_MUNICIPIO + " TEXT," + COLUMN_PARAJE_INTERES + " TEXT,"
            + COLUMN_PARAJE_RADIO + " TEXT,"
            + COLUMN_PARAJE_IMAGEN + " TEXT" + ")";

    private String DROP_PARAJE_TABLE = "DROP TABLE IF EXISTS " + TABLE_PARAJE;

    private String CREATE_PAJARO_TABLE = "CREATE TABLE " + TABLE_PAJARO
            + "(" + COLUMN_PAJARO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_PAJARO_NOMBRE_CASTELLANO + " TEXT,"
            + COLUMN_PAJARO_NOMBRE_CIENTIFICO + " TEXT" + ")";

    private String DROP_PAJARO_TABLE = "DROP TABLE IF EXISTS " + TABLE_PAJARO;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_CITA_TABLE);
        db.execSQL(CREATE_CITA_ONLINE_TABLE);
        db.execSQL(CREATE_PARAJE_TABLE);
        db.execSQL(CREATE_PAJARO_TABLE);

        poblarPajaros(db,TABLE_PAJARO,COLUMN_PAJARO_NOMBRE_CASTELLANO,COLUMN_PAJARO_NOMBRE_CIENTIFICO);

        poblarParajes(db,TABLE_PARAJE,COLUMN_PARAJE_NOMBRE,COLUMN_PARAJE_LATITUD,COLUMN_PARAJE_LONGITUD,
                COLUMN_PARAJE_MUNICIPIO,COLUMN_PARAJE_RADIO,COLUMN_PARAJE_INTERES,COLUMN_PARAJE_IMAGEN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_CITA_TABLE);
        db.execSQL(DROP_CITA_ONLINE_TABLE);
        db.execSQL(DROP_PARAJE_TABLE);
        db.execSQL(DROP_PAJARO_TABLE);

        onCreate(db);

    }

    public void addUser(Usuario user) {
        byte[] image ;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NOMBRE, user.getNombre());
        values.put(COLUMN_USER_DOMICILIO, user.getDomicilio());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        values.put(COLUMN_USER_TELEFONO, user.getTelefono());
        values.put(COLUMN_USER_IDENTIFICADOR, user.getIdentificador());
        image = Util.getBytes(user.getImagen());
        values.put(COLUMN_USER_IMAGEN, image);
        values.put(COLUMN_USER_TOKEN, user.getToken());

        db.insert(TABLE_USER, null, values);
        db.close();
    }

    public void delUser(String email) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_USER + " WHERE " + COLUMN_USER_EMAIL + "= '" + email + "'");
        db.close();

    }

    public void updateUser(Usuario user) {
        byte[] image ;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NOMBRE, user.getNombre());
        values.put(COLUMN_USER_DOMICILIO, user.getDomicilio());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        values.put(COLUMN_USER_TELEFONO, user.getTelefono());
        values.put(COLUMN_USER_IDENTIFICADOR, user.getIdentificador());
        image = Util.getBytes(user.getImagen());
        values.put(COLUMN_USER_IMAGEN, image);
        values.put(COLUMN_USER_TOKEN, user.getToken());

        db.update(TABLE_USER, values, COLUMN_USER_EMAIL + " = ?",
                new String[]{String.valueOf(user.getEmail())});
        db.close();
    }

    public Usuario getUser(String username) {
        Bitmap image ;
        Usuario user = new Usuario();

        String[] columns = {
                COLUMN_USER_ID,
                COLUMN_USER_NOMBRE,
                COLUMN_USER_DOMICILIO,
                COLUMN_USER_EMAIL,
                COLUMN_USER_TELEFONO,
                COLUMN_USER_IDENTIFICADOR,
                COLUMN_USER_IMAGEN,
                COLUMN_USER_TOKEN
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = COLUMN_USER_EMAIL + " = ?";

        String[] selectionArgs = {username};
        Cursor cursor = db.query(TABLE_USER,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if (cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID))));
                user.setNombre(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NOMBRE)));
                user.setDomicilio(cursor.getString(cursor.getColumnIndex(COLUMN_USER_DOMICILIO)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
                user.setTelefono(cursor.getString(cursor.getColumnIndex(COLUMN_USER_TELEFONO)));
                user.setIdentificador(cursor.getString(cursor.getColumnIndex(COLUMN_USER_IDENTIFICADOR)));

                image = Util.getImage(cursor.getBlob(cursor.getColumnIndex(COLUMN_USER_IMAGEN)));
                user.setImagen(image);

                user.setToken(cursor.getString(cursor.getColumnIndex(COLUMN_USER_TOKEN)));
            }
        }
        cursor.close();
        db.close();

        return user;
    }

    public boolean checkUser(String email) {

        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = COLUMN_USER_EMAIL + " = ?";

        String[] selectionArgs = {email};
        Cursor cursor = db.query(TABLE_USER,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }
        return false;
    }


    public void addCita(Cita cita) {
        byte[] image ;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITA_USERNAME, cita.getUsuario());
        values.put(COLUMN_CITA_ESPECIE, cita.getEspecie());
        values.put(COLUMN_CITA_LATITUD, cita.getLatitud());
        values.put(COLUMN_CITA_LONGITUD, cita.getLongitud());
        values.put(COLUMN_CITA_PARAJE, cita.getParaje());
        values.put(COLUMN_CITA_MUNICIPIO, cita.getMunicipio());
        values.put(COLUMN_CITA_NUMERO, cita.getNumero());
        values.put(COLUMN_CITA_EDAD, cita.getEdad());
        values.put(COLUMN_CITA_SEXO, cita.getSexo());
        values.put(COLUMN_CITA_INTERES, cita.getInteres());
        values.put(COLUMN_CITA_OBSERVACION, cita.getObservaciones());
        values.put(COLUMN_CITA_PRIVACIDAD, cita.getPrivacidad());
        values.put(COLUMN_CITA_FILTRO, cita.getFiltro());
        values.put(COLUMN_CITA_FECHA, cita.getFecha());
        image = Util.getBytes(cita.getImagen());
        values.put(COLUMN_CITA_IMAGEN, image);
        values.put(COLUMN_CITA_STATUS, cita.getStatus());
        values.put(COLUMN_CITA_TOKEN, cita.getToken());

        db.insert(TABLE_CITA, null, values);
        db.close();
    }

    public void delCita(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_CITA + " WHERE " + COLUMN_CITA_ID + "= '" + id + "'");
        db.close();

    }

    public void updateCita(Cita cita) {
        byte[] image ;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITA_USERNAME, cita.getUsuario());
        values.put(COLUMN_CITA_ESPECIE, cita.getEspecie());
        values.put(COLUMN_CITA_LATITUD, cita.getLatitud());
        values.put(COLUMN_CITA_LONGITUD, cita.getLongitud());
        values.put(COLUMN_CITA_PARAJE, cita.getParaje());
        values.put(COLUMN_CITA_MUNICIPIO, cita.getMunicipio());
        values.put(COLUMN_CITA_NUMERO, cita.getNumero());
        values.put(COLUMN_CITA_EDAD, cita.getEdad());
        values.put(COLUMN_CITA_SEXO, cita.getSexo());
        values.put(COLUMN_CITA_INTERES, cita.getInteres());
        values.put(COLUMN_CITA_OBSERVACION, cita.getObservaciones());
        values.put(COLUMN_CITA_PRIVACIDAD, cita.getPrivacidad());
        values.put(COLUMN_CITA_FILTRO, cita.getFiltro());
        values.put(COLUMN_CITA_FECHA, cita.getFecha());
        image = Util.getBytes(cita.getImagen());
        values.put(COLUMN_CITA_IMAGEN, image);
        values.put(COLUMN_CITA_STATUS, cita.getStatus());
        values.put(COLUMN_CITA_TOKEN, cita.getToken());

        db.update(TABLE_CITA, values, COLUMN_CITA_ID + " = ?",
                new String[]{String.valueOf(cita.getId())});
        db.close();
    }

    public List<Cita> getAllCitas(String username) {
        Bitmap image ;

        String[] columns = {
                COLUMN_CITA_ID,
                COLUMN_CITA_USERNAME,
                COLUMN_CITA_ESPECIE,
                COLUMN_CITA_LATITUD,
                COLUMN_CITA_LONGITUD,
                COLUMN_CITA_PARAJE,
                COLUMN_CITA_MUNICIPIO,
                COLUMN_CITA_NUMERO,
                COLUMN_CITA_EDAD,
                COLUMN_CITA_SEXO,
                COLUMN_CITA_INTERES,
                COLUMN_CITA_OBSERVACION,
                COLUMN_CITA_PRIVACIDAD,
                COLUMN_CITA_FILTRO,
                COLUMN_CITA_FECHA,
                COLUMN_CITA_IMAGEN,
                COLUMN_CITA_STATUS,
                COLUMN_CITA_TOKEN
        };
        String sortOrder =
                COLUMN_CITA_ID + " ASC";
        List<Cita> citaList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selection = COLUMN_CITA_USERNAME + " = ?";
        String[] selectionArgs = {username};
        Cursor cursor = db.query(TABLE_CITA,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Cita cita = new Cita();
                cita.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ID))));
                cita.setUsuario(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_USERNAME)));
                cita.setEspecie(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ESPECIE)));
                cita.setLatitud(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_LATITUD)));
                cita.setLongitud(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_LONGITUD)));
                cita.setParaje(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_PARAJE)));
                cita.setMunicipio(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_MUNICIPIO)));
                cita.setNumero(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_NUMERO)));
                cita.setEdad(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_EDAD)));
                cita.setSexo(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_SEXO)));
                cita.setInteres(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_INTERES)));
                cita.setObservaciones(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_OBSERVACION)));
                cita.setPrivacidad(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_PRIVACIDAD)));
                cita.setFiltro(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_FILTRO)));
                cita.setFecha(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_FECHA)));
                image = Util.getImage(cursor.getBlob(cursor.getColumnIndex(COLUMN_CITA_IMAGEN)));
                cita.setImagen(image);
                cita.setStatus(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_STATUS))));
                cita.setToken(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_TOKEN)));
                citaList.add(cita);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return citaList;
    }

    public Cursor getUnsyncedCita() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_CITA + " WHERE " + COLUMN_CITA_STATUS + " = " + RC_NOT_SYNC +";";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public Cursor getUnupdatedCita() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_CITA + " WHERE " + COLUMN_CITA_STATUS + " = " + RC_NOT_UPDATE +";";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public Cursor getUndeletedCita() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_CITA + " WHERE " + COLUMN_CITA_STATUS + " = " + RC_NOT_DELETE +";";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public void updateCitaStatus(int id, int status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_CITA_STATUS, status);

        db.update(TABLE_CITA, contentValues, COLUMN_CITA_ID + "=" + id, null);
        db.close();
    }


    public void addCitaOnline(Cita cita, Context context) {
        byte[] image ;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITA_ONLINE_USERNAME, cita.getUsuario());
        values.put(COLUMN_CITA_ONLINE_ESPECIE, cita.getEspecie());
        values.put(COLUMN_CITA_ONLINE_LATITUD, cita.getLatitud());
        values.put(COLUMN_CITA_ONLINE_LONGITUD, cita.getLongitud());
        values.put(COLUMN_CITA_ONLINE_PARAJE, cita.getParaje());
        values.put(COLUMN_CITA_ONLINE_MUNICIPIO, cita.getMunicipio());
        values.put(COLUMN_CITA_ONLINE_NUMERO, cita.getNumero());
        values.put(COLUMN_CITA_ONLINE_EDAD, cita.getEdad());
        values.put(COLUMN_CITA_ONLINE_SEXO, cita.getSexo());
        values.put(COLUMN_CITA_ONLINE_INTERES, cita.getInteres());
        values.put(COLUMN_CITA_ONLINE_OBSERVACION, cita.getObservaciones());
        values.put(COLUMN_CITA_ONLINE_PRIVACIDAD, cita.getPrivacidad());
        values.put(COLUMN_CITA_ONLINE_FILTRO, cita.getFiltro());
        values.put(COLUMN_CITA_ONLINE_FECHA, cita.getFecha());
        if(cita.getImagen() != null ) {
            image = Util.getBytes(cita.getImagen());
            values.put(COLUMN_CITA_ONLINE_IMAGEN, image);
        }else{
            image = Util.getBytes(BitmapFactory.decodeResource(context.getResources(), R.drawable.usuario_default));
            values.put(COLUMN_CITA_ONLINE_IMAGEN, image);
        }

        db.insert(TABLE_CITA_ONLINE, null, values);
        db.close();
    }

    public List<Cita> getAllCitasOnlineFiltro(String paraje) {
        Bitmap image ;

        String[] columns = {
                COLUMN_CITA_ONLINE_ID,
                COLUMN_CITA_ONLINE_USERNAME,
                COLUMN_CITA_ONLINE_ESPECIE,
                COLUMN_CITA_ONLINE_LATITUD,
                COLUMN_CITA_ONLINE_LONGITUD,
                COLUMN_CITA_ONLINE_PARAJE,
                COLUMN_CITA_ONLINE_MUNICIPIO,
                COLUMN_CITA_ONLINE_NUMERO,
                COLUMN_CITA_ONLINE_EDAD,
                COLUMN_CITA_ONLINE_SEXO,
                COLUMN_CITA_ONLINE_INTERES,
                COLUMN_CITA_ONLINE_OBSERVACION,
                COLUMN_CITA_ONLINE_PRIVACIDAD,
                COLUMN_CITA_ONLINE_FILTRO,
                COLUMN_CITA_ONLINE_FECHA,
                COLUMN_CITA_ONLINE_IMAGEN,
        };
        String sortOrder =
                COLUMN_CITA_ONLINE_ID + " ASC";
        List<Cita> citaList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selection = COLUMN_CITA_ONLINE_PARAJE + " = ?";
        String[] selectionArgs = {paraje};
        Cursor cursor = db.query(TABLE_CITA_ONLINE,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Cita cita = new Cita();
                cita.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_ID))));
                cita.setUsuario(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_USERNAME)));
                cita.setEspecie(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_ESPECIE)));
                cita.setLatitud(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_LATITUD)));
                cita.setLongitud(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_LONGITUD)));
                cita.setParaje(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_PARAJE)));
                cita.setMunicipio(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_MUNICIPIO)));
                cita.setNumero(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_NUMERO)));
                cita.setEdad(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_EDAD)));
                cita.setSexo(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_SEXO)));
                cita.setInteres(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_INTERES)));
                cita.setObservaciones(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_OBSERVACION)));
                cita.setPrivacidad(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_PRIVACIDAD)));
                cita.setFiltro(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_FILTRO)));
                cita.setFecha(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_FECHA)));
                image = Util.getImage(cursor.getBlob(cursor.getColumnIndex(COLUMN_CITA_ONLINE_IMAGEN)));
                cita.setImagen(image);
                citaList.add(cita);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return citaList;
    }

    public List<Cita> getAllCitasOnline() {
        Bitmap image ;

        String[] columns = {
                COLUMN_CITA_ONLINE_ID,
                COLUMN_CITA_ONLINE_USERNAME,
                COLUMN_CITA_ONLINE_ESPECIE,
                COLUMN_CITA_ONLINE_LATITUD,
                COLUMN_CITA_ONLINE_LONGITUD,
                COLUMN_CITA_ONLINE_PARAJE,
                COLUMN_CITA_ONLINE_MUNICIPIO,
                COLUMN_CITA_ONLINE_NUMERO,
                COLUMN_CITA_ONLINE_EDAD,
                COLUMN_CITA_ONLINE_SEXO,
                COLUMN_CITA_ONLINE_INTERES,
                COLUMN_CITA_ONLINE_OBSERVACION,
                COLUMN_CITA_ONLINE_PRIVACIDAD,
                COLUMN_CITA_ONLINE_FILTRO,
                COLUMN_CITA_ONLINE_FECHA,
                COLUMN_CITA_ONLINE_IMAGEN,
        };
        String sortOrder =
                COLUMN_CITA_ONLINE_ID + " ASC";
        List<Cita> citaList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CITA_ONLINE,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Cita cita = new Cita();
                cita.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_ID))));
                cita.setUsuario(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_USERNAME)));
                cita.setEspecie(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_ESPECIE)));
                cita.setLatitud(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_LATITUD)));
                cita.setLongitud(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_LONGITUD)));
                cita.setParaje(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_PARAJE)));
                cita.setMunicipio(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_MUNICIPIO)));
                cita.setNumero(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_NUMERO)));
                cita.setEdad(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_EDAD)));
                cita.setSexo(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_SEXO)));
                cita.setInteres(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_INTERES)));
                cita.setObservaciones(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_OBSERVACION)));
                cita.setPrivacidad(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_PRIVACIDAD)));
                cita.setFiltro(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_FILTRO)));
                cita.setFecha(cursor.getString(cursor.getColumnIndex(COLUMN_CITA_ONLINE_FECHA)));
                image = Util.getImage(cursor.getBlob(cursor.getColumnIndex(COLUMN_CITA_ONLINE_IMAGEN)));
                cita.setImagen(image);
                citaList.add(cita);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return citaList;
    }

    public void delCitasOnline() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CITA_ONLINE, null, null);
        db.close();
    }


    public List<Paraje> getAllParajes() {
        String[] columns = {
                COLUMN_PARAJE_ID,
                COLUMN_PARAJE_NOMBRE,
                COLUMN_PARAJE_LATITUD,
                COLUMN_PARAJE_LONGITUD,
                COLUMN_PARAJE_INTERES,
                COLUMN_PARAJE_MUNICIPIO,
                COLUMN_PARAJE_RADIO,
                COLUMN_PARAJE_IMAGEN
        };

        String sortOrder =
                COLUMN_PARAJE_NOMBRE + " ASC";
        List<Paraje> parajeList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PARAJE,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Paraje paraje = new Paraje();
                paraje.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_ID))));
                paraje.setNombre(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_NOMBRE)));
                paraje.setLatitud(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_LATITUD)));
                paraje.setLongitud(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_LONGITUD)));
                paraje.setMunicipio(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_MUNICIPIO)));
                paraje.setInteres(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_INTERES)));
                paraje.setRadio(cursor.getString(cursor.getColumnIndex(COLUMN_PARAJE_RADIO)));
                paraje.setImagen(cursor.getString((cursor.getColumnIndex(COLUMN_PARAJE_IMAGEN))));
                parajeList.add(paraje);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return parajeList;
    }

    public List<Pajaro> getAllPajaros() {
        String[] columns = {
                COLUMN_PAJARO_ID,
                COLUMN_PAJARO_NOMBRE_CASTELLANO,
                COLUMN_PAJARO_NOMBRE_CIENTIFICO
        };

        String sortOrder =
                COLUMN_PAJARO_NOMBRE_CASTELLANO + " COLLATE UNICODE ASC";
        List<Pajaro> pajaroList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PAJARO,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                Pajaro pajaro = new Pajaro();
                pajaro.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_PAJARO_ID))));
                pajaro.setNombreCastellano(cursor.getString(cursor.getColumnIndex(COLUMN_PAJARO_NOMBRE_CASTELLANO)));
                pajaro.setNombreCientifico(cursor.getString(cursor.getColumnIndex(COLUMN_PAJARO_NOMBRE_CIENTIFICO)));
                pajaroList.add(pajaro);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return pajaroList;
    }




}
