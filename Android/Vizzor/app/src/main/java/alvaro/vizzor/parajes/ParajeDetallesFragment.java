package alvaro.vizzor.parajes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import alvaro.vizzor.R;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;

import static android.graphics.Color.CYAN;
import static android.graphics.Color.TRANSPARENT;

public class ParajeDetallesFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mGoogleMap;
    private SupportMapFragment mapFrag;

    private TextView txtNombre;
    private TextView txtMunicipio;
    private TextView txtInteres;
    private TextView txtCoordenadas;

    private String latitud;
    private String longitud;
    private String radio;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_paraje_detalles, container, false);

        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtMunicipio = (TextView) view.findViewById(R.id.txtMunicipio);
        txtInteres = (TextView) view.findViewById(R.id.txtInteres);
        txtCoordenadas = (TextView) view.findViewById(R.id.txtCoordenadas);


        txtNombre.setText(Singleton.getInstance().getParaje().getNombre());
        txtMunicipio.setText(Singleton.getInstance().getParaje().getMunicipio());
        txtInteres.setText(Singleton.getInstance().getParaje().getInteres());


        latitud = Singleton.getInstance().getParaje().getLatitud();
        longitud = Singleton.getInstance().getParaje().getLongitud();

        txtCoordenadas.setText(Util.convertCoordenadas(Double.parseDouble(latitud), Double.parseDouble(longitud)));

        radio = Singleton.getInstance().getParaje().getRadio();

        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapa);
        mapFrag.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);

        LatLng latLng = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(Integer.parseInt(radio));
        circleOptions.fillColor(TRANSPARENT);
        circleOptions.strokeColor(CYAN);
        circleOptions.strokeWidth(8);
        mGoogleMap.addCircle(circleOptions);

        /*MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));

        mGoogleMap.addMarker(markerOptions);*/

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(9));

    }

}