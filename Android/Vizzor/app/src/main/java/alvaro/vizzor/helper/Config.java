package alvaro.vizzor.helper;



public class Config {

    public static final int RC_SIGN_IN = 1001;
    public static final int RC_INTERNET = 1005;

    public static final String URL_INSERT_CITA = "https://vizzor.ddns.net/vizzor/bin/insertCita.php";
    public static final String URL_UPDATE_CITA = "https://vizzor.ddns.net/vizzor/bin/updateCita.php";
    public static final String URL_DELETE_CITA = "https://vizzor.ddns.net/vizzor/bin/deleteCita.php";
    public static final String URL_GET_CITAS = "https://vizzor.ddns.net/vizzor/bin/getAllCitas.php";
    public static final String URL_INSERT_AUTH = "https://vizzor.ddns.net/vizzor/bin/insertAuth.php";
    public static final String URL_GET_CITAS_USUARIO = "https://vizzor.ddns.net/vizzor/bin/getAllCitasUsuario.php";

    /*public static final String URL_INSERT_CITA = "http://192.168.31.67/vizzor/bin/insertCita.php";
    public static final String URL_UPDATE_CITA = "http://192.168.31.67/vizzor/bin/updateCita.php";
    public static final String URL_DELETE_CITA = "http://192.168.31.67/vizzor/bin/deleteCita.php";
    public static final String URL_GET_CITAS = "http://192.168.31.67/vizzor/bin/getAllCitas.php";
    public static final String URL_INSERT_AUTH = "http://192.168.31.67/vizzor/bin/insertAuth.php";
    public static final String URL_GET_CITAS_USUARIO = "http://192.168.31.67/vizzor/bin/getAllCitasUsuario.php";*/

    public static final int PX_W = 640;
    public static final int PX_H = 480;

    public static final int RC_MODIFICAR = 95;

    public static final int RC_UBICACION = 33;

    public static final int RC_CAMERA = 48;
    public static final int RC_GALLERY = 49;

    public static final int RC_TERMINOS = 44;
    public static final int RC_CREAR_CITA = 55;
    public static final int RC_MODIFICAR_CITA = 56;
    public static final String RC_BROADCAST = "broadcast";

    public static final int RC_SYNC = 0;
    public static final int RC_NOT_SYNC = 1;
    public static final int RC_NOT_UPDATE = 2;
    public static final int RC_NOT_DELETE = 3;

    public static final int RC_DIALOG_INTERNET = 5;
    public static final int RC_DIALOG_ALERT = 10;
    public static final int RC_DIALOG_INFO = 15;

}
