package alvaro.vizzor.parajes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class ParajeDetallesViewPagerAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] =
            new String[] {"Información", "Citas"};

    public ParajeDetallesViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                ParajeDetallesFragment parajeDetalles = new ParajeDetallesFragment();
                return parajeDetalles;
            case 1:
                ParajeCitasFragment parajeCitas = new ParajeCitasFragment();
                return parajeCitas;
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}