package alvaro.vizzor.parajes;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import alvaro.vizzor.AyudaActivity;
import alvaro.vizzor.InformacionActivity;
import alvaro.vizzor.MainActivity;
import alvaro.vizzor.R;
import alvaro.vizzor.citas.CitaMainActivity;
import alvaro.vizzor.helper.Singleton;
import alvaro.vizzor.helper.Util;
import alvaro.vizzor.misCitas.miCitaMainActivity;
import alvaro.vizzor.usuario.UsuarioDetallesActivity;

public class ParajeDetallesMainActivity extends AppCompatActivity {
    private Context context;

    private DrawerLayout drawerLayout;
    private NavigationView navView;

    private ImageView ivCircle;
    private TextView txtUsername;

    private ImageView ivImagen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paraje_detalles_main);

        context = getApplicationContext();

        if(Singleton.getInstance().getUsuario() == null) {
            Log.e("ParajeDetallesMain", "ControlSingleton = nulo");
            Util.checkSingletonUser(context);
        }

        Singleton.getInstance().setOrigen("paraje");

        Log.e("ParajeDetallesMain", "ControlSingleton= " + Singleton.getInstance().getUsuario().toString());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.parajes));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_nav_menu);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new ParajeDetallesViewPagerAdapter(getSupportFragmentManager()));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(true);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navView = (NavigationView) findViewById(R.id.navview);

        View nav = navView.getHeaderView(0);

        ivCircle = (ImageView) nav.findViewById(R.id.ivCircle);
        txtUsername = (TextView) nav.findViewById(R.id.txtUsername);

        txtUsername.setText(Singleton.getInstance().getUsuario().getEmail());
        Glide.with(context)
                .load(Singleton.getInstance().getUsuario().getImagen())
                .into(ivCircle);

        ivImagen = (ImageView) findViewById(R.id.ivImagen);

        collapsingToolbar.setTitle(Singleton.getInstance().getParaje().getNombre());

        Glide.with(context)
                .load(Uri.parse("file:///android_asset/" + Singleton.getInstance().getParaje().getImagen()))
                .apply(new RequestOptions().override(150, 150).fitCenterTransform())
                .into(ivImagen);

        ivImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog nagDialog = new Dialog(ParajeDetallesMainActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(true);
                nagDialog.setContentView(R.layout.dialog_imagen);
                ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
                Glide.with(context)
                        .load(Uri.parse("file:///android_asset/" + Singleton.getInstance().getParaje().getImagen()))
                        .into(ivPreview);

                ivPreview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();
            }
        });

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.menu_inicio:
                                Intent intentoMain = new Intent(context, MainActivity.class);
                                startActivity(intentoMain);
                                finish();
                                break;
                            case R.id.menu_mis_citas:
                                Intent intentoMiCita = new Intent(context, miCitaMainActivity.class);
                                startActivity(intentoMiCita);
                                finish();
                                break;
                            case R.id.menu_citas:
                                Intent intentoCita = new Intent(context, CitaMainActivity.class);
                                startActivity(intentoCita);
                                finish();
                                break;
                            case R.id.menu_parajes:
                                Intent intentoParajes = new Intent(context, ParajeMainActivity.class);
                                startActivity(intentoParajes);
                                finish();
                                break;
                            case R.id.menu_perfil:
                                Intent intentoPerfil = new Intent(context, UsuarioDetallesActivity.class);
                                startActivity(intentoPerfil);
                                finish();
                                break;
                            case R.id.menu_informacion:
                                Intent intentoInformacion = new Intent(context, InformacionActivity.class);
                                startActivity(intentoInformacion);
                                finish();
                                break;
                            case R.id.menu_ayuda:
                                Intent intentoAyuda = new Intent(context, AyudaActivity.class);
                                startActivity(intentoAyuda);
                                finish();
                                break;
                            case R.id.menu_salir:
                                Util.logout(context, ParajeDetallesMainActivity.this);
                                finish();
                                break;
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intentoparaje = new Intent(context, ParajeMainActivity.class);
        startActivity(intentoparaje);
        finish();
    }

}

