En la presente carpeta, Android, se encuentran aquellos ficheros y recursos que han
formado parte del desarrollo de la aplicación móvil.

	APK: Contiene la aplicación compilada en formato APK para su instalación en 
el dispositivo móvil.

	Código Fuente: Contiene el código fuente de la aplicación móvil.
