En la presente carpeta, Recursos, se encuentran aquellos ficheros y recursos que han
formado parte del desarrollo de la herramienta y la memoria.

	Diagramas: Contiene los diagramas realizados mediante la herramienta Visual Paradigm.

	Firewall IPTABLES: Contiene el firewall del servidor generado mediante Iptables.

	Imágenes Propias: Contiene las imágenes que han sido generadas por el autor del proyecto.

	Prototipos: Contiene los bocetos realizados mediante la herramienta Balsamiq Mockups.

	Scripts: Contiene pequeñas herramientas realizadas en Java para facilitar el trabajo a la 
hora de generar documentos utilizados en el desarrollo.

	Test de evaluación: Contiene el test de evaluación utilizado para conocer la opinión de 
diferentes sujetos de pruebas sobre el sistema implementado.
