En el CD adjunto a la presente memoria se encuentran aquellos ficheros y recursos que han
formado parte del desarrollo del proyecto.

	Android: Compuesto por la carpeta que contiene el código fuente de la aplicación móvil
desarrollada e implementada para el proyecto llevado a cabo y la aplicación compilada en
formato APK para su instalación en el dispositivo móvil.

	Memoria: Compuesto por la memoria en formato PDF.

	Recursos: Compuesto por todos los ficheros y recursos utilizados en la creación de la presente
memoria, así como, diversos recursos que forman parte de la herramienta desarrollada.

	Servidor: Compuesto por la carpeta que contiene todos los ficheros y recursos alojados en el
servidor para el correcto funcionamiento de la comunicación entre la aplicación móvil y el
servidor remoto, así como, la carpeta que contiene todos los ficheros y recursos alojados en el
servidor para el correcto funcionamiento de la página web realizada.