\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.1}Ejemplo de la creaci\IeC {\'o}n de la Base de datos local}{101}{lstlisting.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.2}Ejemplo de la creaci\IeC {\'o}n de la Base de datos remota}{102}{lstlisting.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.3}Ejemplo de implementaci\IeC {\'o}n de la comprobaci\IeC {\'o}n del usuario}{106}{lstlisting.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.4}Ejemplo de implementaci\IeC {\'o}n del registro del usuario}{107}{lstlisting.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.5}Ejemplo de implementaci\IeC {\'o}n del registro del usuario mediante Volley}{107}{lstlisting.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.6}Ejemplo de implementaci\IeC {\'o}n del men\IeC {\'u}}{110}{lstlisting.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.7}Ejemplo de la implementaci\IeC {\'o}n del men\IeC {\'u} lateral}{110}{lstlisting.5.7}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.8}Ejemplo de la implementaci\IeC {\'o}n del autocompletar de las especies}{117}{lstlisting.5.8}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.9}Ejemplo de la implementaci\IeC {\'o}n de la sincronizaci\IeC {\'o}n mediante el BroadcastReceiver}{118}{lstlisting.5.9}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.10}Ejemplo de la implementaci\IeC {\'o}n del guardado de las nuevas citas}{118}{lstlisting.5.10}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.11}Ejemplo de la implementaci\IeC {\'o}n de la actualizaci\IeC {\'o}n de la lista personalizada}{121}{lstlisting.5.11}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.12}Ejemplo de la implementaci\IeC {\'o}n de la b\IeC {\'u}squeda de citas}{122}{lstlisting.5.12}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.13}Ejemplo de la implementaci\IeC {\'o}n del aumento de la imagen}{122}{lstlisting.5.13}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.14}Ejemplo de la implementaci\IeC {\'o}n del bot\IeC {\'o}n de eliminar cita}{127}{lstlisting.5.14}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.15}Ejemplo de la implementaci\IeC {\'o}n del guardado de la cita modificada}{127}{lstlisting.5.15}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.16}Ejemplo de la implementaci\IeC {\'o}n del eliminado de la cita}{127}{lstlisting.5.16}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.17}Ejemplo de la implementaci\IeC {\'o}n del bot\IeC {\'o}n de compartir cita}{128}{lstlisting.5.17}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.18}Ejemplo de la implementaci\IeC {\'o}n del elemento de sincronizaci\IeC {\'o}n}{131}{lstlisting.5.18}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.19}Ejemplo de la implementaci\IeC {\'o}n de la actualizaci\IeC {\'o}n de la lista personalizada}{131}{lstlisting.5.19}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.20}Ejemplo del mapa de los parajes}{135}{lstlisting.5.20}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.21}Ejemplo de la selecci\IeC {\'o}n de citas cotejadas con los parajes}{135}{lstlisting.5.21}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.22}Ejemplo de la selecci\IeC {\'o}n de citas cotejadas con los parajes}{138}{lstlisting.5.22}
