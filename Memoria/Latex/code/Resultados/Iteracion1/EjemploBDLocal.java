// Pajaros table name
	private static final String TABLE_PAJARO = "pajaros";

// Pajaros Table Columns names
    public static final String COLUMN_PAJARO_ID = "id";
    public static final String COLUMN_PAJARO_NOMBRE_CASTELLANO = "castellano";
    public static final String COLUMN_PAJARO_NOMBRE_CIENTIFICO = "cientifico";

// Create table sql query
    private String CREATE_PAJARO_TABLE = "CREATE TABLE " + TABLE_PAJARO
            + "(" + COLUMN_PAJARO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," 
            + COLUMN_PAJARO_NOMBRE_CASTELLANO + " TEXT,"
            + COLUMN_PAJARO_NOMBRE_CIENTIFICO + " TEXT" + ")";

// Drop table sql query
    private String DROP_PAJARO_TABLE = "DROP TABLE IF EXISTS " + TABLE_PAJARO;

// Create bd
  	@Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PAJARO_TABLE);

        poblarPajaros(db,TABLE_PAJARO,COLUMN_PAJARO_NOMBRE_CASTELLANO
            ,COLUMN_PAJARO_NOMBRE_CIENTIFICO);
    }