\chapter{Antecedentes}
\label{cap:Antecedentes}

En este capítulo se detallan los resultados de la investigación teórica realizada sobre el estado de arte de las áreas específicas que se vinculan a la temática del Trabajo Fin de Grado. Se analizarán aquellas aplicaciones con una temática similar que propongan una solución al problema propuesto y mediante un estudio de sus características se obtendrá una información valiosa a tener en cuenta para el correcto desarrollo del proyecto.

\section{Fundamentos teóricos}
\label{sec:Fundamentos teóricos}
\index{Fundamentos teóricos} 

La observación de aves se define como una actividad en la que se observa y se estudia la avifauna silvestre de forma recreativa o científica. Dicha actividad puede ser realizada sin el uso de ningún complemento adicional o mediante binoculares y telescopios. 

Dentro de la observación de aves se incluye la percepción auditiva de los sonidos emitidos por las aves. Este tipo de sonidos pueden ayudar en la identificación de las especies al ser más diferenciadoras entre ellas que el tipo de plumaje que poseen.

Las personas que realizan de manera lúdica la observación de aves también pueden ser ornitólogos que utilizan la información recopilada para estudiar las diferentes especies de aves desde un método más científico. Cabe destacar que aquellas observaciones realizadas por personas que no forman parte de la comunidad científica son utilizadas como referencia y valoradas para estudios en ámbitos de investigación.

La existencia de diferentes organizaciones nacionales y mundiales pone en manifiesto el interés que tiene este tipo de actividades en la población, no solo en el aspecto lúdico, sino que también se hace hincapié en las características que aporta en otros ámbitos más centrados en el estudio. Las organizaciones más importantes son la British Trust for Ornithology y la Royal Society for the Protection of Bird en Reino Unido, la American Birding Association y el Cornell Lab of Ornithology en Estados Unidos y SEO/BirdLife en España.

La proliferación del ecoturismo, que incluye actividades como la observación de aves, es un hecho mediante el cual se pretende revalorizar la economía local y la concienciación de la conservación del medio ambiente.

\textbf{Historia de la observación de las aves}

A finales de siglo XVIII en las obras de Gilbert White , Thomas Bewick , George Montagu y John Clare se empieza a mostrar el interés por la observación de las aves por su belleza. 

Durante la época victoriana se observaban aves para la recolección de huevos y pieles, siendo las personas con grandes sumas de dinero aquellas que utilizaban su influencia para conseguir estos elementos de especies no autóctonas de partes diversas del mundo para su colección personal.

En el siglo XIX se comenzó a fomentar la conservación de las aves mediante la fundación de la primera Sociedad Audubon en América y la Real Sociedad para la Protección de las Aves en Gran Bretaña. Estas sociedades fomentaron la observación de las aves como actividad recreativa y la conservación de numerosas especies.

El término \emph{`observación de aves'} apareció por primera vez en el libro \emph{`Observación de Aves'} de Edmund Selous en 1901 \cite{edmund}. La guía de campo más antigua en los Estados Unidos fue \emph{`Birds through a Opera Glass'} de Florence Bailey en 1889 \cite{florence}. 
En el año 1956 se funda la Sociedad Española de Ornitología con el propósito principal de la conservación y el estudio de las aves y sus hábitats.

En la década de 1980 gracias al transporte aéreo y sus precios asequibles hicieron posible el comienzo de un turismo basado en la observación de aves en su hábitat natural. 

El uso de guías que contuvieran especies de todo el mundo propició el desarrollo del \emph{`Manual de las Aves del Mundo'} escrito por Josep del Hoyo, Jordi Sargatal y Andy Elliott en 1992 \cite{josep}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Antecedentes/mantaking}
	\caption[Personas fotografiando un ave para su estudio]{Personas fotografiando un ave para su estudio \cite{mantaking}}
\end{figure}

\textbf{Actividades relacionadas con la observación de las aves}

Las personas que realizan dicha actividad suelen observar especies locales, pero a su vez pueden viajar a otras zonas del planeta para observar nuevas especies.
Según la época del año o el clima de la zona se pueden observar una variedad de especies en una misma ubicación debido a las migraciones que realizan las aves.
Las actividades que se suelen realizar respecto a la observación de aves son:

\begin{itemize}
	\item[$\blacksquare$] \textbf{Monitoreo:}

	Los eventos basados en el monitoraje de las especies se basan en la generación de censos y patrones migratorios mediante la observación de las aves en ciertas zonas determinadas. Este tipo de actividad ayuda a la comunidad científica ornitológica a especificar posibles amenazas ambientales o evaluar el estado de un hábitat, ya que es considerada como la rama más científica de la observación de las aves \cite{texas}.

	\item[$\blacksquare$] \textbf{Educación Ambiental:}
 
	La observación de las aves es un aliciente para educar y concienciar a la población de los problemas ambientales que suceden hoy en día y como paliarlos. La aportación de valores como el respeto a la naturaleza, la conservación medioambiental o la protección de la fauna autóctona es fácilmente transmisible mediante la realización de estas actividades.

	\item[$\blacksquare$] \textbf{Competiciones:}

	La observación de aves también puede ser usada en un entorno competitivo en el que se concursa por la observación de un número de especies en el menor tiempo posible o en aumentar la lista de aves avistadas a lo largo de la vida de un observador \cite{pete}. 
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Antecedentes/birders}
	\caption[Personas observando aves en plena competición]{Personas observando aves en plena competición \cite{birders}}
\end{figure}

\textbf{Tecnología utilizada en la observación de las aves}

El equipo técnico utilizado por las personas que realizan este tipo de prácticas es de lo más diverso y variado. Normalmente se hace uso de unos binoculares o telescopio, un bloc de notas y una guía de campo. Las torres de observación mejoran las condiciones de las observaciones ya que ocultan a las personas y permiten la observación de una manera anónima a la avifauna presente en el lugar sin que interfieran en sus actividades cotidianas.

Dentro del equipamiento tecnológico se pueden encontrar:
\begin{itemize}
	\item[$\blacksquare$] \textbf{Equipos de sonido:} 

	El reconocimiento de las aves por medio de su canto es una parte importante de la realización de la actividad de la observación ya que, la información que proporciona puede ayudar a la identificación y recogida de información de las aves. 
	
	\vfill
	
	\item[$\blacksquare$] \textbf{Fotografía:} 

	La fotografía se ha convertido en un aspecto relevante debido a la evolución de las cámaras de fotos y sus complementos para ser utilizadas juntos a binoculares y telescopios. Gracias a estos dispositivos es posible la captación de detalles visuales de las aves para su posterior estudio.

	\item[$\blacksquare$] \textbf{Video:} 

	La evolución de las cámaras de vídeo permite la toma de imágenes en movimiento a gran resolución, así como sonidos, haciendo que sea un elemento indispensable para la comunidad al poder captar y reproducir de una forma sencilla no solo las características visuales de un pájaro, sino también sus patrones de movimiento y su sonido.

	\item[$\blacksquare$] \textbf{Observación remota de aves:} 

	Gracias a la utilización de dispositivos inalámbricos instalados en la naturaleza permiten la posibilidad de la observación de aves mediante el uso de internet pudiendo realizar observaciones de un punto remoto desde la comodidad del hogar.
	
	\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Antecedentes/cams}
	\caption[Ejemplo de cámara web remota]{Ejemplo de cámara web remota \cite{cornerCams}}
	\label{fig:cams}
\end{figure}

\end{itemize}

\textbf{Comunicación en el ámbito de la observación de las aves}

La colaboración entre personas aficionadas al avistamiento de aves y la forma en que compartían la información ha ido evolucionando junto con la tecnología del momento.

En los años 50 el modo habitual de informar sobre nuevos avistamientos era mediante el correo postal. En los años 60 se empezó a utilizar el teléfono debido a la operatividad que permitieron ciertos amantes al convertirse en centros de comunicación e información a título personal. En los años 70 se utilizaban las cafeterías como lugares de reunión.

Con el nacimiento de internet estos métodos quedaron en un segundo plano o incluso en desuso. Se cambiaron las llamadas telefónicas o las reuniones en cafeterías por foros, listas de correo electrónico, bases de datos, tableros de anuncios, etc.

Esta nueva forma de comunicación tiene como base los diferentes intereses de los observadores de aves, es decir, existen lugares en internet específicos según la predilección de los usuarios. Un ejemplo es la existencia de foros con una estricta categorización para facilitar la búsqueda e información sobre un tipo de aves concreta\cite{birdforum}. Este nuevo paradigma de comunicación basado en internet también facilita la identificación de especies, la discusión sobre las aves, orientación para realizar correctamente la actividad, recomendaciones para preservar el hábitat, notificaciones de especies raras, \dots{} .

Existen multitud de páginas web dedicadas a este tipo de actividades, algunas de ellas pueden ser encontradas en páginas dedicadas a su recopilación para que sea más fácil su localización y consulta\cite{birdwat}.

Birdchat es la herramienta más longeva y con mayor número de usuarios en este contexto\cite{birdchat}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Antecedentes/birdforum}
	\caption[Ejemplo de foro dedicado al avistamiento de aves]{Ejemplo de foro dedicado al avistamiento de aves}
	\label{fig:forum}
\end{figure}

\section{Fundamentos tecnológicos}
\label{sec:Fundamentos tecnológicos}
\index{Fundamentos tecnológicos} 

\subsection{Dispositivos móviles}
\label{subsec:Dispositivos móviles antecedentes}

Se define dispositivo móvil como dispositivo electrónico con la posibilidad de efectuar actividades propias de un ordenador. Normalmente este tipo de dispositivos cuentan con una amplia variedad de elementos que amplían las funcionalidades ofrecidas por un teléfono móvil convencional. Entre ellas se pueden encontrar acceso a internet, correo electrónico, aplicaciones de terceros, reproducción y visualización de archivos multimedia, geolocalización, etc.

Los dispositivos móviles inteligentes surgieron de la necesidad de unificar en un mismo dispositivo las funcionalidades que ofrecía una PDA y un teléfono móvil. 
Con esta idea IBM desarrollo el IBM Simon en 1993, el terminal precursor de la evolución a la que sería sometida la industria. Contaba con una pantalla táctil y se podían gestionar notas personales, el calendario o el correo electrónico.

Cabe destacar el Ericsson GS88 como el primer dispositivo al que se le acuñó el termino\\
 \emph{`smartphone'}. Fue distribuido en 1997.

En el año 2000 los dispositivos empezaron a tener un sistema operativo propio mediante el cual se gestionaban las funcionalidades con las que contaban. Windows Pocket PC fue la revolución y el sistema pionero mediante el cual se produjo un cambio en el paradigma de estos dispositivos marcando el camino evolutivo hasta nuestros días.

El auge llegó de la mano de Apple y Google en 2007, mientras que Apple presentaba su terminal Iphone y era un éxito en la industria, Google presentaba su sistema operativo Android OS.

Desde ese momento el impacto de los dispositivos móviles en la vida cotidiana ha sido excepcional. Gracias a ello las personas viven conectadas a un amplio entorno desde su realidad mediante el uso de internet y los sensores de los que disponen los dispositivos móviles. Es fácil consultar una información en el acto o saber dónde se encuentra algo al instante, incluso poder comunicarte con una persona que vive en otro continente.

\newpage

Hoy en día, un usuario de este tipo de tecnología pasa una media de 170 minutos al día utilizándolo lo que implica el uso de multitud de características, funcionalidades y aplicaciones \cite{ditr}. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Antecedentes/tiempo}
	\caption[Tiempo medio de conexión diario]{Tiempo medio de conexión diario}
	\label{fig:tiempoCon}
\end{figure}


\subsection{Android}
\label{subsec:Android}

Android es un sistema operativo para dispositivos móviles basado en el núcleo Linux y desarrollado por Google. Este sistema operativo se encuentra presente en multitud de dispositivos como teléfonos, tablets, relojes, televisiones, domótica, ordenadores, automóviles y un largo etcétera relacionado con la tecnología.

Según Kantar Worldpanel en un estudio realizado sobre la cuota de mercado de los principales sistemas operativos para dispositivos móviles se observa que Android lidera las ventas con un porcentaje del 87.1\% en diciembre de 2017\cite{cuota}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Antecedentes/cuota}
	\caption[Cuota de mercado de Android]{Cuota de mercado de Android}
	\label{fig:cuotaAndroid}
\end{figure}

El sistema operativo Android fue fundado en 2003 por Andy Rubin, Rich Miner, Nick Sears y Chris White en su empresa Android Inc. El objetivo principal era desarrollar un sistema operativo acorde a las preferencias del usuario que pudiera rivalizar con los sistemas del momento. Tras la compra de la empresa por Google en 2005 comenzó el desarrollo de lo que sería el sistema operativo basándose en el núcleo del sistema operativo Linux.

El 23 de septiembre de 2008 se lanzó el primer dispositivo con Android, el HTC Dream.

\textbf{Evolución}

Desde sus inicios hasta la actualidad Android ha evolucionado sacando nuevas versiones que añadían funcionalidades y se adaptaban al mercado del momento.
La primera versión se publicó el 23 de septiembre de 2008 incluyendo mucho de los bloques fundamentales de Android que han evolucionado junto con el sistema operativo.

El 27 de abril de 2009 se reveló Android 1.5 Cupcake y empezó la tradición de los nombres de postres.

Tras esta versión se han ido sucediendo muchas otras hasta un total de 13 versiones importantes las cuales incluían en algunos casos subversiones con mejoras relevantes \cite{versiones}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Antecedentes/versiones}
	\caption[Versiones actuales de Android]{Versiones actuales de Android}
	\label{fig:versionesAndroid}
\end{figure}

\textbf{Arquitectura}

Es importante hablar de los principales componentes de este sistema. En él se encuentran:
\begin{itemize}
	\item[$\blacksquare$] \textbf{Aplicaciones:} 

	Las aplicaciones están escritas mediante el lenguaje Java y cumplen funcionalidades de lo más diversas. Por defecto se incluyen aplicaciones para gestionar el correo, gestor de mensajes de texto, calendario, mapas, navegador de internet, gestor de contactos, etc. Además, pueden ser instaladas muchas otras aplicaciones de terceros para añadir funcionalidades o características al dispositivo.
	
	\vfill

	\item[$\blacksquare$] \textbf{Marco de trabajo:} 

	Los desarrolladores tienen un acceso completo a las herramientas utilizadas en el desarrollo de las aplicaciones nativas del sistema, es decir, las APIs y el framework son públicas y accesibles para desarrollar nuevas aplicaciones. También se dispone de unas bibliotecas utilizadas por diferentes elementos a las cuales se puede acceder y hacer uso de ellas.
	
	\item[$\blacksquare$] \textbf{Runtime:}

	Android facilita una serie de funciones disponibles en las bibliotecas para el lenguaje de Java, de este modo las clases son compiladas y ejecutadas.
Desde la versión 5.0 de Android se utiliza ART en vez de la máquina virtual Dalvik.

	\item[$\blacksquare$] \textbf{Kernel Linux:}

	El núcleo de Linux interactúa entre el hardware del dispositivo y el resto del software del sistema, esto implica que Android depende del sistema Linux para los servicios base.

	\item[$\blacksquare$] \textbf{Treble:}

	En la nueva versión de Android, Android Oreo, se incluye esta característica que permite particionar el sistema operativo en módulos fácilmente actualizables para que los fabricantes solo tengan que actualizar su módulo y de esta forma las actualizaciones del sistema sean más rápidas y fiables \cite{treble}.
	
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Antecedentes/treble}
	\caption[Mejoras que suponen la implementación de Treble]{Mejoras que suponen la implementación de Treble}
	\label{fig:treble}
\end{figure}

\end{itemize}

\textbf{Modelo Vista Controlador}

El patrón de arquitectura Modelo Vista Controlador utiliza tres tipos de componentes diferenciados para establecer una separación entre la lógica de la aplicación de la lógica de la vista, es decir, permite separar los componentes que forman la aplicación en base a la responsabilidad que tienen dentro del sistema respetando el principio de responsabilidad única presente en diferentes patrones de arquitectura\cite{mvc}.
Los tres componentes que usa este patrón son:

\begin{itemize}
	\item[$\blacksquare$] \textbf{Modelo:}

	Es el componente encargado de los datos y la información con la cual se realizarán las diferentes funcionalidades de la aplicación. En el presente Trabajo Fin de Grado este componente está formado por una base de datos SQLite presente en el dispositivo móvil y una base de datos MySQL presente en la Raspberry.
	
	\item[$\blacksquare$] \textbf{Controlador:}


	Es el componente encargado de proporcionar las diferentes funcionalidades a la aplicación y ser un intermediario entre los datos del modelo y su representación en la vista. En el presente Trabajo Fin de Grado este componente está formado por las clases java que conforman el sistema.
	
	\item[$\blacksquare$] \textbf{Vista:}


	Es el componente encargado de la representación visual de los datos en la aplicación. En el presente Trabajo Fin de Grado este componente está formado por los diferentes archivos XML encargados de definir la interfaz de usuario de la aplicación, así como, los elementos predefinidos para facilitar la visualización de información.
\end{itemize}

\begin{figure}[H]
	\centering
	\subfigure[Evolución de los S.O en dispositivos móviles]{
		\includegraphics[width= 0.45 \linewidth]{Antecedentes/mvc}
		\label{fig:evolucionMoviles}
	} 
	\subfigure[Evolución de las apps en Android]{
		\includegraphics[width= 0.45 \linewidth]{Antecedentes/mvc_arquitectura}
		\label{fig:evolucionApps}
	}
	\caption[Evolución de los S.O y las apps en dispositivos móviles]{Evolución de los S.O y las apps en dispositivos móviles}
	\label{fig:evolucionMovilesApps}
\end{figure}

\subsection{Posicionamiento GPS}
\label{subsec:Posicionamiento GPS}

El GPS (Global Position System), es una tecnología que permite determinar la posición de un elemento en cualquier lugar del mundo en tiempo real. Está basado en la aplicación del principio matemático de triangulación mediante el uso de esferas virtuales que gracias a diferentes satélites pueden definir la posición de un elemento. 

J.W. Jones desarrolló el primer GPS en 1909 mediante un disco de papel con una ruta entre dos ciudades unido a un odómetro. 
Gracias al programa TRANSIT en 1957, desplegado por el ejército de los EEUU, el GPS comenzó a funcionar. En 1964 se lanzaron 10 satélites y en 1967 fue liberado para ser usado por la población civil. 

En 1973 nació el NAVSTAR-GPS formado por 24 satélites en órbita, de uso militar. Tras el accidente del vuelo Korean Airlines KAL007 la tecnología se puso a disposición para el uso civil. En 1991 se desarrolló el GPS diferencial, lo que supuso una gran mejora en la precisión del sistema de posicionamiento\cite{gpshistoria}. 

\textbf{Funcionamiento}

Los satélites están situados en una órbita circular intermedia, entre 2.000 Km y 36.000 Km de distancia a la superficie terrestre, y se encargan de enviar la información necesaria para realizar la triangulación y conocer la posición de un elemento mediante señales de radio que viajan a la velocidad de la luz. Los receptores que se encuentran en la tierra son los encargados de recibir la información de los satélites y operar con ella para realizar los cálculos pertinentes y establecer la posición donde se encuentran. 

Estas operaciones se basan en la distancia entre satélite y receptor y el tiempo que tarda en llegar la señal del satélite al receptor. Gracias al conocimiento de la distancia entre satélite y receptor se establece una región plausible donde puede ubicarse el receptor. Para utilizar la variable temporal se necesita un código pseudo-aleatorio para sincronizar los relojes de los que disponen los satélites y los receptores \cite{gpsfunc}.

Tiempo de retardo * Velocidad de la luz = Distancia

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\linewidth]{Antecedentes/triangulacion}
	\caption[Ejemplo de triangulación usando el GPS]{Ejemplo de triangulación usando el GPS}
	\label{fig:triangulacion}
\end{figure}

\textbf{Uso en dispositivos móviles}

Hoy en día la mayoría de dispositivos móviles cuentan con una integración total del sistema de posicionamiento global mediante la integración de los sensores pertinentes. Esto ha producido que la funcionalidad proporcionada por esta tecnología sea utilizada en una gran variedad de aplicaciones útiles para los usuarios de estos dispositivos. El contexto de dichas aplicaciones es de los más variado, desde navegación hasta el uso de servicios basados en la localización (Located Based System).

\textbf{Google Maps}

Google Maps es un servicio de aplicaciones de mapas y navegación. Se caracteriza por mostrar mapas desplazables, fotografías satelitales, rutas entre dos puntos o fotografías a pie de calle.

Fue desarrollado en 2005 ofreciendo un servicio web de visualización de mapas junto con una API del servicio. La herramienta Street View fue implementada en 2007 añadiendo valor al proyecto inicial. Esta herramienta permitía la visualización de fotografía a pie de calle.

Su evolución ha sido notoria y creciente añadiendo nuevas funcionalidades hasta el día de hoy. Actualmente cuenta con diversas características como puntos de interés, estado del tráfico, información de establecimientos, rutas detalladas, etc., todo en tiempo real\cite{googlemaps}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\linewidth]{Antecedentes/maps}
	\caption[Ejemplo de Google Maps]{Ejemplo de Google Maps}
	\label{fig:googleMaps}
\end{figure}

\subsection{REST}
\label{subsec:REST}

REST (Representational State Transfer) se define como un tipo de arquitectura de desarrollo web. Permite la creación de servicios y/o aplicaciones mediante el uso de HTTP para obtener u operar con datos en multitud de formatos. 

Fue definida por Roy Fielding, coautor de la especificación HTTP y referente de la Arquitectura de Redes, en el año 2000 en su disertación \emph{`Architectural Styles and the Design of Network-based Software Architectures'}\cite{disert}.

\textbf{Características de REST} 
\begin{itemize}
	\item[$\blacksquare$] Las peticiones HTTP contienen toda la información necesaria para ejecutarla permitiendo que no se almacene ningún estado anterior ni en el cliente ni en el servidor estableciendo de esta forma un protocolo cliente-servidor sin estado.
 
	\item[$\blacksquare$] Las operaciones utilizadas son: POST (crear), GET (leer y consultar), PUT (editar) y DELETE (eliminar).

	\item[$\blacksquare$] La manipulación de los objetos REST se realizan mediante la URI, ya que es el identificador único del recurso. 

	\item[$\blacksquare$] La ejecución de las operaciones sobre los recursos solo se dará en elementos identificados mediante una URI. 

	\item[$\blacksquare$] Se utiliza una arquitectura jerárquica entre las capas que llevan a cabo una funcionalidad determinada.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Antecedentes/rest}
	\caption[Ejemplo de API Rest]{Ejemplo de API Rest}
	\label{fig:apiRest}
\end{figure}

\subsection{Servidor HTTP APACHE}
\label{subsec:Servidor HTTP APACHE}

El servidor HTTP Apache es un servidor web de código abierto que utiliza el protocolo HTTP. Puede ser usado en diferentes plataformas como Unix, Microsoft Windows y Macintosh lo que aporta una gran flexibilidad y compatibilidad. Desde el año 1996 el servidor HTTP Apache es el servidor web más utilizado \cite{apache}. 

Fue desarrollado por Robert McCool en 1995 bajo la empresa Apache Software Foundation basándose en el código de NCSA HTTPd 1.3. 

Al ser de código abierto el sistema es mejorado y mantenido por la comunidad de usuarios bajo la supervisión de la empresa.

Las funcionalidades presentes por defecto pueden ser ampliadas mediante módulos proporcionando una configuración de características según el usuario que use el servicio proporcionado por el servidor HTTP Apache.  

\subsection{MariaDB}
\label{subsec:MariaDB}

MariaDB es un sistema de gestión de bases de datos relacional \cite{mariabd}.

Fue desarrollado por Michael Widenius, fundador de MySQL, en el año 2009. 

MariaDB es un fork de MySQL por lo que su compatibilidad está asegurada al compartir los mismos elementos y funcionalidades que MySQL. Este hecho asegura la existencia de un producto con licencia GPL igual que MySQL y su licencia de Oracle.

\subsection{PHP}
\label{subsec:PHP}

PHP es un lenguaje de programación de propósito general. Este lenguaje es utilizado del lado del servidor para la implementación de contenido dinámico en la web \cite{php}. 

Fue desarrollado por Rasmus Lerdorf en el año 1995. 

Una de las principales ventajas es su simplicidad y su gran flexibilidad para realizar multitud de acciones como la generación de contenido dinámico, recopilación de información, acciones referentes a las bases de datos, cifrado de datos, procesamiento de imágenes, validación de credenciales, gestión de cookies, etc.

\subsection{SQLite}
\label{subsec:SQLite}

SQLite es un sistema de gestión de bases de datos relacional almacenada en una biblioteca de reducido tamaño escrita en C \cite{sqlite}.

Fue desarrollado por D. Richard Hipp en el año 2000 siendo un proyecto de dominio público. La principal diferencia con otros sistemas destinados a la gestión de bases de datos relacionales es la integración de SQLite como parte del sistema donde es utilizada, es decir, no actúa como un proceso independiente sino que forma parte del sistema. Esto permite la ejecución de las diferentes funcionalidades proporcionadas por SQLite como procesos propios del sistema.

Los datos y la información perteneciente a la base de datos (tablas, índices y definiciones) son almacenados en un único archivo dentro de sistema donde es ejecutada la aplicación que utiliza las funcionalidades de SQLite.

\subsection{HTML/CSS/JAVASCRIPT/BOOTSTRAP}
\label{subsec:HTML/CSS/JAVASCRIPT/BOOTSTRAP}

HTML es un lenguaje de marcado para el desarrollo de páginas web. 

CSS es un lenguaje de diseño para definir la representación gráfica de un documento escrito en un lenguaje de marcado.  

JavaScript es un lenguaje de programación interpretado y orientado a objetos. 
Estos lenguajes son definidos por el W3C (Wide Web Consortium) y establecen el estándar de las tecnologías web.

Bootstrap es un framework para diseño de páginas y aplicaciones web. 

\section{Trabajos relacionados}
\label{sec:Trabajos relacionados}
\index{Trabajos relacionados} 

\subsection{eBird by Cornell Lab}
\label{subsec:eBird by Cornell Lab}

eBird de Cornell Lab\cite{ebird} es un proyecto para el registro de avistamiento de aves por todo el mundo. Cuenta con mas de 100 millones de observaciones por lo que colabora con multitud de organizaciones, y expertos. Toda la documentación, distribución, uso de hábitat y tendencias de la avifauna puede ser consultada a través de listas de especies dentro de un marco científico.

Los datos de los diferentes avistamientos pueden ser consultados mediante la página web, facilitando la exploración y estudio de los mismos. 

La aplicación móvil permite generar nuevas observaciones de aves de forma sencilla, permitiendo que usuarios no familiarizados con la temática o la tecnología puedan hacer uso de ella.

Dicha aplicación ha sido adoptada por SEO/BirdLife para su utilización.

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/7-2}
	} 
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/7-1}
	}
	\caption[eBird by Cornell Lab para dispositivo móvil]{eBird by Cornell Lab para dispositivo móvil}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.60\linewidth]{Antecedentes/55}
	\caption[eBird by Cornell Lab para sitio web]{eBird by Cornell Lab para sitio web}
\end{figure}


\subsection{Birdtrack}
\label{subsec:Birdtrack}

BirdTrack\cite{birdtrack} es un proyecto para el análisis de las migraciones y distribución de aves en Gran Bretaña e Irlanda. Cuenta con un servicio web y aplicaciones para servicios móviles para que cualquier persona pueda contribuir cuando realiza un avistamiento. 

La información puede ser visualizada por cualquier persona interesada en las aves en la página web del proyecto. Los datos son actualizados cada día renovando los mapas y los gráficos correspondientes mostrando nuevos movimientos de aves o las migraciones que se producen.

El uso de mapas animados proporciona una interesante información sobre la llegada y salida de una especie concreta en un lugar determinado. Además, aquellas especies raras cuentan con un mapa personalizado que muestra su evolución en la zona a lo largo del año.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.30\linewidth]{Antecedentes/6-1}
	\caption[Birdtrack para dispositivo móvil]{Birdtrack para dispositivo móvil}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.60\linewidth]{Antecedentes/22}
	\caption[Birdtrack para sitio web]{Birdtrack para sitio web}
\end{figure}

\subsection{iNaturalist}
\label{subsec:iNaturalist}

iNaturalist\cite{inaturalist} es un proyecto destinado a compartir las observaciones realizadas de plantas, insectos, aves y animales. Cuenta con una página web, una aplicación para Android y otra para iPhone. La comunidad formada que utiliza esta herramienta hace posible la identificación mediante sugerencias automatizadas. 

Aunque es un sistema para la difusión de información está basado en el principio de red social, por lo que los usuarios pueden comunicarse con otros para poder establecer relaciones personales entre personas que comparten una misma pasión.

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/5-1}
	} 
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/5-2}
	}
	\caption[iNaturalist]{iNaturalist}
\end{figure}

\subsection{iBird Pro Birds North America}
\label{subsec:iBird Pro Birds North America}

iBird Pro\cite{ibird} es una aplicación para dispositivos móviles  que permite buscar información sobre especies de aves, actuando como una guía de campo. Esta especializada en la avifauna de América del Norte. Permite la descarga de imágenes y sonidos de los pájaros elegidos para ser consultados más tarde sin disponer de internet.

También ofrece la posibilidad de identificar aves por diferentes características como su color, la forma, el hábitat, el lugar donde ha sido observadas u otros parámetros que pueden ser introducidos para facilitar el reconocimiento.

\subsection{CyberTracker}
\label{subsec:CyberTracker}

CyberTracker\cite{cybertracker} es una aplicación de software para la captura y visualización de datos móviles. Es usada para fomentar la conservación de especies en peligro de extinción. Cuenta con varios proyectos para la protección de diferentes animales como los rinocerontes en África, los gorilas en el Congo, leopardos de las nieves en el Himalaya, mariposas en Suiza, jaguares en Costa Rica, aves en el Amazonas, caballos salvajes en Mongolia, delfines en California, tortugas marinas en el Pacífico y ballenas en la Antártida.

CyberTracker es usada para la investigación científica, así como, la educación ambiental. El proyecto es tan bien valorado que diferentes comunidades indígenas lo han usado para ayudar en la conservación y protección de su hábitat autóctono.

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.45 \linewidth]{Antecedentes/44}
	} 
	\subfigure{
		\includegraphics[width= 0.45 \linewidth]{Antecedentes/11}
	}
	\caption[CyberTracker]{CyberTracker}
\end{figure}

\subsection{Identificador de aves}
\label{subsec:Identificador de aves}

Identificador de aves\cite{identificador} es una aplicación web que permite identificar especies de aves y consultar su información. Mediante las diferentes pestañas se pueden seleccionar los diferentes elementos para ir acotando dichas especies hasta dar con aquella que se busca.
 
También posibilita la visualización de información referente a la especie seleccionada.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.60\linewidth]{Antecedentes/33}
	\caption[Identificador de aves]{Identificador de aves}
\end{figure}

\subsection{Aves Acuáticas de SEO/BirdLife}
\label{subsec:Aves Acuáticas de SEO/BirdLife}

Aves Acuáticas de SEO/BirdLife\cite{acuaticas} es una aplicación para dispositivos móviles para conocer el tamaño, distribución y evolución de las poblaciones de aves acuáticas en España. Permite que los usuarios puedan incluir nuevos censos en los humedales actualizando la información de las especies de un lugar. 

\newpage

También posibilita la identificación de aquellos humedales más cercanos al usuario de la aplicación mediante el uso del GPS.

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/4-1}
	} 
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/4-2}
	}
	\caption[Aves Acuáticas de SEO/BirdLife]{Aves Acuáticas de SEO/BirdLife}
\end{figure}

\subsection{Merlin Bird ID de Cornell Lab}
\label{subsec:Merlin Bird ID de Cornell Lab}

Merlin Bird ID de Cornell Lab\cite{merlin} es una aplicación para dispositivos móviles que permite la identificación de la especie de un pájaro en concreto. Mediante una serie de preguntas o una imagen es capaz de establecer una lista de aves que se adapten a la descripción dada.

También ofrece la consulta de información relevante como fotos, sonidos o consejos sobre diferentes especies de avifauna.

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/3-1}
	} 
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/3-2}
	}
	\caption[Merlin Bird ID de Cornell Lab]{Merlin Bird ID de Cornell Lab}
\end{figure}

\subsection{Ornithopedia Europe}
\label{subsec:Ornithopedia Europe}

Ornithopedia\cite{ornithopedia} es una aplicación para la plataforma Android que permite la búsqueda de información sobre 1144 aves de Europa y Rusia. Proporciona imágenes, archivos de sonido y la posibilidad de visualizar videos de la plataforma Youtube en streaming.

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/2-1}
	} 
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/2-2}
	}
	\caption[Ornithopedia Europe]{Ornithopedia Europe}
\end{figure}

\subsection{Bird Counter}
\label{subsec:Bird Counter}

Bird Counter\cite{birdcounter} es una aplicación para la plataforma Android que permite el conteo de la cantidad de aves observadas de una manera sencilla. Mediante la asignación de especies a botones virtuales en el dispositivo móvil es posible numerar las especies sin dejar de realizar el avistamiento. 

\begin{figure}[H]
	\centering
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/1-1}
	} 
	\subfigure{
		\includegraphics[width= 0.30 \linewidth]{Antecedentes/1-2}
	}
	\caption[Bird Counter]{Bird Counter}
\end{figure}