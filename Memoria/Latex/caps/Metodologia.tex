\chapter{Metodología}
\label{cap:Metodologia}

En este capítulo se procederá a especificar la forma mediante la cual se ha desarrollado el proyecto del presente Trabajo Fin de Grado. Mediante el estudio y análisis correspondiente se justificará la elección de la metodología elegida para llevar a cabo el trabajo, se mostrarán sus características principales y las modificaciones pertinentes para su completo ajuste al desarrollo.

\section{Metodología de desarrollo}
\label{sec:Metodología de desarrollo}
\index{Metodología de desarrollo} 
Primero se deberá contextualizar el término metodología de desarrollo. Una metodología de desarrollo es un entorno de trabajo preestablecido para planificar y gestionar los procesos durante el desarrollo de un proyecto. Usualmente estas metodologías están compuestas por una filosofía de trabajo que establezca un enfoque determinado para llevar a cabo los procesos del desarrollo y una serie de herramientas para que ayuden durante estos procesos.

Para el correcto desarrollo e implementación del Trabajo Fin de Grado propuesto se deberá utilizar una metodología afín al mismo. Para esta elección se ha realizado un análisis y estudio comparativo de las metodologías vigentes llegando a ciertas conclusiones que determinan la elección de la misma.

Durante el estudio se ha podido determinar la existencia de dos tipos de corrientes metodológicas en el desarrollo de software, las metodologías tradicionales y las metodologías ágiles. Aunque ambas metodologías tienen diversas características en común, como lo son los objetivos del proyecto, el tiempo de desarrollo y los costes asociados, su filosofía de trabajo es completamente diferente.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Metodologia/comparativamet}
	\caption[Comparativa de los tipos de metodología]{Comparativa de los tipos de metodología}
	\label{fig:compmet}
\end{figure}

Las metodologías ágiles se basan en un enfoque adaptativo y orientado a personas mediante la interacción del usuario final del proyecto. El objetivo principal de este tipo de metodologías es ir desarrollando prototipos que puedan ser evaluados por el usuario, de este modo, mediante la continua comunicación permitir añadir cambios y modificaciones del proyecto para lograr una completa adaptabilidad a los requisitos finales del usuario \cite{raul}.

Tras el análisis y estudio y teniendo en cuenta las conclusiones resumidas expuestas anteriormente, se ha llegado a la conclusión de la utilización de una metodología de desarrollo ágil denominada eXtreme Programming (XP). Esto es debido al contexto del propio proyecto anteriormente propuesto, la naturaleza del mismo establece una fuerte adaptabilidad a posibles modificaciones de requisitos durante el desarrollo y requiere una continua comunicación con el usuario para llegar a un producto final totalmente acorde a sus deseos.

Conociendo el método de trabajo y el carácter académico de los Trabajos Fin de Grado resulta difícil la implementación completa de dicha metodología por lo que se realizará una implantación adaptada a las particularidades del presente Trabajo Fin de Grado. 

\section{Metodologías ágiles}
\label{sec:Metodologías ágiles}
\index{Metodologías ágiles} 

Las metodologías ágiles son aquellas metodologías que permiten la flexibilidad y capacidad de adaptación de la forma de trabajo al contexto del proyecto teniendo en cuenta en todo momento el cliente. Están basadas en un desarrollo iterativo e incremental donde los requisitos son modificados y evolucionan a lo largo del desarrollo del proyecto, por lo que la generación de prototipos totalmente funcionales al acabar cada iteración para que el cliente pruebe las funcionalidades implementadas es una práctica común para ir validando el proyecto.

Una de las primeras referencias que se tienen sobre el origen de las metodologías ágiles se encuentra en un artículo publicado Takeuchi \& Nona en 1986 donde se mostraban ejemplos de empresas que habían implementado metodologías contrarias a las metodologías que se usaban por decreto y estaban obteniendo mejores resultados \cite{take}. Pero no es hasta 2001 cuando se establece el término \emph{`Métodos Ágiles'} y surgen los principios que forman el \emph{`Manifiesto Ágil'} \cite{agilem}. Este manifiesto es la base de todas las metodologías ágiles existentes. 

\subsection{Justificación de elección}
\label{Justificación de elección}

Teniendo en cuenta todo lo expuesto anteriormente se puede justificar la elección de una metodología ágil frente a una metodología tradicional. La flexibilidad implícita en este tipo de metodologías y la posibilidad de evolución en los requisitos y soluciones las hacen totalmente imprescindibles en un desarrollo donde la toma de decisiones es incremental para satisfacer los requisitos del usuario final. De este modo, se determina un objetivo principal y una línea de trabajo a seguir. Mediante periodos de trabajo cortos en los que la planificación se realiza sobre elementos concretos del proyecto se permite ir evolucionando el producto a la vez que el cliente va validando el trabajo realizado hasta el momento. La adaptabilidad surgida por la continua comunicación es relevante en la elección de este tipo de metodologías ya que, de esta forma, mediante la validación y las decisiones tomadas tras ella es posible lograr el objetivo final del proyecto.

\section{eXtreme Programming}
\label{sec:eXtreme Programming}
\index{eXtreme Programming} 

Como se ha comentado anteriormente la metodología ágil seleccionada es eXtreme Programming, primero se contextualizará dicha metodología, después se especificarán los cambios realizados para la adaptación al presente TFG y por último se explicará la elección de dicha metodología.

eXtreme Programming nació de la mano de Kent Beck en 1996 como una metodología basada en la flexibilidad y agilidad. Se consolidó en 1999 con la publicación de \emph{`Programming Explained: Embrace Change'} \cite{kent}. En este libro se encuentra la definición de eXtreme Programming asi como los valores, principios básicos y prácticas que lo componen. En 2004 se publicó una segunda versión donde se ponían de manifiesto la continua evolución de la metodología añadiendo nuevos elementos a la metodología \cite{extreme}.

La característica de eXtreme Programming es la entrega al cliente de un producto funcional cuando ellos lo necesiten. Permite que los requisitos y soluciones evolucionen a medida que lo requiera el cliente independientemente de la fase en la que se encuentre el proyecto. El trabajo en equipo es uno de los pilares en los cuales se sustenta incluyendo personas de diferentes ámbitos, pero con el mismo objetivo común, la entrega de un software de calidad y que cumpla con los requisitos del cliente. Al tratarse de una metodología ágil se enfatiza la adaptabilidad a la previsibilidad.

Se especifica la existencia de una comunicación constante entre el cliente y los desarrolladores para que la colaboración entre ambos sea un elemento determinante en el éxito del proyecto.

\subsection{Ciclo de vida}
\label{Ciclo de vida}

eXtreme Programming propone la utilización de un ciclo de vida dinámico mediante la utilización de ciclos cortos de trabajo, conocidos como iteraciones, con entregables funcionales al finalizar cada periodo. Cada iteración está compuesta de un ciclo de diseño, desarrollo y pruebas que, junto con un análisis trasversal a todas aquellas iteraciones necesarias para la realización del proyecto componen en su conjunto el ciclo de vida de un proyecto. 

Esta metodología establece 6 fases diferenciadas:

\begin{itemize}
	\item[$\blacksquare$] \textbf{Fase de exploración:} 
	
	Es la fase inicial del proyecto donde es definido el alcance global del mismo. El cliente relata las necesidades del sistema mediante las historias de usuario que serán utilizadas por el equipo de trabajo de desarrollo para estimar el tiempo que necesitarán para completar los requisitos del proyecto.
Tras esta fase se obtendrá una visión global del proyecto, así como, una estimación de los plazos necesarios para su desarrollo.

	\item[$\blacksquare$] \textbf{Fase de planificación:} 
	
	En esta fase se establece, mediante reuniones entre todas las partes integrantes del grupo de trabajo del proyecto (clientes y desarrolladores), el orden en el que serán desarrolladas e implementadas las historias de usuario y las entregas relacionadas con estas.
Tras esta fase se obtendrá un plan de entregas en función de las reuniones y la importancia de las historias de usuario establecidas en estas.

	\item[$\blacksquare$] \textbf{Fase de iteraciones:} 
	
	En esta fase se lleva a cabo el desarrollo de las funcionalidades descritas en las historias de usuario. Al finalizar cada iteración es entregado un prototipo funcional para la validación por parte del cliente. Usualmente las historias de usuario carecen de detalles necesarios en la implementación por lo que es necesaria la continua comunicación entre cliente y grupo de trabajo.
Tras esta fase se obtendrá un producto completo y funcional del sistema propuesto por el cliente.

	\item[$\blacksquare$] \textbf{Fase de producción:} 
	
	En esta fase se realizan ajustes del sistema o se toman las decisiones pertinentes para la inclusión de nuevas características debido a cambios producidos durante la ejecución de esta fase. 
	
	\item[$\blacksquare$] \textbf{Fase de mantenimiento:} 
	
	En esta fase se debe mantener el sistema desplegado y funcionando correctamente.
	
	\item[$\blacksquare$] \textbf{Fase de muerte del proyecto:} 
	
	En esta fase el cliente deja de incorporar nuevas funcionalidades al proyecto y ha validado todas aquellas que han sido incluidas anteriormente a la vez que se comprueba que otros elementos inherentes como la calidad, el rendimiento, la seguridad, la confiabilidad, la usabilidad, … están presentes y complacen a los objetivos que pretende conseguir el cliente. 
Tras esta fase se obtendrá una documentación final del proyecto.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Metodologia/ciclodevida}
	\caption[Ciclo de vida en eXtreme Programming]{Ciclo de vida en eXtreme Programming}
	\label{fig:ciclo}
\end{figure}

\subsection{Valores}
\label{Valores}

eXtreme Programming está basado en un conjunto de valores que determinarán el éxito del proyecto.
\begin{itemize}
	\item[$\blacksquare$] \textbf{Simplicidad:}

	La máxima de esta metodología está basada en hacer solo aquello que sea necesario para llegar al objetivo del proyecto, la sencillez en el código, en el diseño y en los procesos que conforman el conjunto del proyecto. El desarrollo de aquellos elementos imprescindibles hace que no se pierda tiempo en los detalles que aún no han sido implementados por el grupo de trabajo. 


	\item[$\blacksquare$] \textbf{Comunicación:}
	La comunicación entre los componentes del grupo de trabajo implicados en el proyecto debe ser constante desde el inicio del mismo hasta su finalización para establecer una serie de valores que facilitarán el desarrollo del software, así como la resolución de los posibles errores que surjan o una toma de decisiones adecuada.

	\item[$\blacksquare$] \textbf{Retroalimentación:}

	El feedback se considera como pieza imprescindible en esta metodología ya que proporciona información sobre la evolución que está tomando el proyecto. La retroalimentación debe ser recíproca entre todos los componentes que conforman el grupo de trabajo para que todos los implicados puedan expresar puntos de vista y obtener así un mejor resultado.

	\item[$\blacksquare$] \textbf{Coraje:}

	El equipo de trabajo debe tener el coraje necesario para tomar decisiones en los momentos críticos que se dan en todos los proyectos, de esta forma, hacer frente a los posibles errores con una mayor resolución sin importar el coste ni la dificultad. El coraje también debe presentarse en la sinceridad que debe existir entre los miembros implicados para establecer relaciones basadas en la sinceridad y trabajar sobre la verdad y no en estimaciones.

	\item[$\blacksquare$] \textbf{Respeto:}

	Debe existir un respeto en el equipo de trabajo, ya que todos aportan un valor en el proyecto que se tiene que considerar. Ninguno de ellos es más imprescindible que otro puesto que cada uno integrará un elemento diferente y con la unión de todos se llegará a la conclusión del proyecto generando un producto de calidad, cumpliendo todos los requisitos del cliente. 
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{Metodologia/valores}
	\caption[Valores de eXtreme Programming]{Valores de eXtreme Programming}
	\label{fig:valores}
\end{figure}

\subsection{Principios}
\label{Principios}

Anteriormente se han definido los valores de esta metodología, pero son conceptos genéricos de los cuales no se puede determinar su implicación y validez dentro de un proyecto determinado, es decir, el pensamiento de un individuo puede diferir de otro denostando la importancia de un valor en favor de otro mientras que en otro caso sería al contrario.

A partir de esos valores se forman los principios básicos para que el desarrollo del proyecto pueda ser comparado con dichos valores y comprobar su total cumplimiento o no.

Los principios incorporan diferentes valores concretando y determinando si son usados como corresponde sin lugar a interpretaciones personales.
Estos principios son:

\begin{itemize}
	\item[$\blacksquare$] \textbf{Rápido feedback:}

	Una retroalimentación rápida y eficaz hace que el grupo de trabajo pueda desarrollar y validar el sistema en el menor tiempo posible con lo que la comprensión de los procesos y el aprendizaje de los mismos es más fluido. 
	
	\vfill
	
	\item[$\blacksquare$] \textbf{Asumir simplicidad:}

	Asumir una simplicidad latente en todo el proyecto conlleva tratar de dar soluciones simples a los problemas que van surgiendo a lo largo del ciclo de vida. Este principio no implica realizar un trabajo vago y sin calidad, se basa en la resolución de problemas centrándose en ellos para que en un futuro se puedan incluir nuevas funcionalidades y añadir complejidad al conjunto. Para llevar a cabo este principio se deben tener en cuenta dos filosofías de trabajo:
	\begin{itemize}
		\item[*] No vas a necesitarlo o YAGNI (en inglés 'You Aren't Gonna Need It').
		\item[*] No te repitas o DRY (en inglés ‘Don't Repeat Yourself’ ).

	\end{itemize}

	\item[$\blacksquare$] \textbf{Cambio incremental:}

	Los cambios realizados deben darse paulatinamente para que puedan ser integrados de una forma correcta. 

	\item[$\blacksquare$] \textbf{Aceptar el cambio:}

	La aceptación de cambios debe ser algo a tener en cuenta puesto que una mayor cantidad de opciones para resolver un mismo problema mejora la calidad y eficacia del sistema desarrollado a la vez que se puede discernir la mejor opción para cada caso.

	\item[$\blacksquare$] \textbf{Trabajo de calidad:}

	Un grupo de trabajo quiere realizar un trabajo del que sentirse orgulloso una vez finalizado por lo que añadirán la calidad necesaria para que ello funcione.
\end{itemize}

\subsection{Variables}
\label{Variables}

eXtreme Programming define cuatro variables, presentes en cualquier proyecto, que son: el coste, el tiempo, la calidad y el alcance. Tres de ellas pueden ser definidas por personas ajenas al grupo de trabajo dedicado al desarrollo del proyecto mientras que el valor de la cuarta será fijado por este grupo en función de las otras variables.
\begin{itemize}
	\item[$\blacksquare$] \textbf{Coste:}

	El coste hace referencia al dinero que costaría el proyecto, por lo que, un coste demasiado alto podría generar problemas durante el transcurso debido al malgasto de recursos o un coste demasiado bajo generaría un proyecto sin recursos para ser completado con total satisfacción. El coste debe ser acorde al proyecto.

	\item[$\blacksquare$] \textbf{Tiempo:}

	El tiempo hace referencia a la duración total del proyecto, es decir, a las restricciones temporales que determinaría el cliente para obtener el producto final. Un tiempo no acorde al proyecto podría derivar a problemas que se darían en las otras variables, mientras que un tiempo superior al necesario otorgaría una mayor calidad y alcance.

	\item[$\blacksquare$] \textbf{Calidad:}

	La calidad hace referencia a los requisitos del cliente y la mejor forma de implementarlas. Es una variable que se presupone que se tendrá en cuenta en todas las fases del proyecto, aunque no se especifique en ningún punto del mismo y debe afectar tanto al código, como a las funcionalidades y al diseño. El coste y el tiempo están fuertemente relacionados con la calidad ya que según ambos la calidad se verá incrementada o excrementada en valor de ellos.

	\vfill 
	
	\item[$\blacksquare$] \textbf{Alcance:}

	El alcance hace referencia a la dimensión que tendrá el proyecto completo. Teniendo en cuenta el ámbito y la naturaleza junto con los valores que conforman las otras variables se podría modificar el alcance a favor de la calidad o el tiempo siempre y cuando el cliente lo pretendiese si su objetivo cambiase a lo largo del proyecto.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{Metodologia/triangulo}
	\caption[Variables de eXtreme Programming]{Variables de eXtreme Programming}
	\label{fig:triangulo}
\end{figure}

\subsection{Roles}
\label{Roles}

eXtreme Programming establece diferentes roles para diferentes procesos del proyecto, de esta forma la responsabilidad es compartida entre el grupo de trabajo y por consiguiente la toma de decisiones.

\begin{itemize}
	\item[$\blacksquare$] \textbf{Programador (Programmer):}

	Es el responsable del diseño, implementación y la realización de pruebas del sistema. Tendrá que tomar decisiones técnicas para que la construcción del sistema sea correcto.

	\item[$\blacksquare$] \textbf{Cliente (Customer):}

	Es el responsable de definir los requisitos del sistema. Determina el alcance, tiempo y costo del proyecto. Forma parte activa del grupo de trabajo. 

	\item[$\blacksquare$] \textbf{Entrenador (Coach):}

	Es el responsable de liderar el equipo de trabajo tomando las decisiones de mayor repercusión ya que es el responsable de que todo funcione correctamente. 

	\item[$\blacksquare$] \textbf{Rastreador (Tracker):}

	Es el responsable de la recopilación y análisis de datos referentes al proyecto para conocer el estado en el que se encuentra o si se van cumplimentando correctamente las funcionalidades requeridas por el cliente. 

	\item[$\blacksquare$] \textbf{Probador (Tester):}

	Es el responsable de asistir al cliente en la creación, ejecución y documentación de pruebas en el sistema.
\end{itemize}

\subsection{Artefactos}
\label{Artefactos}

eXtreme Programming determina una serie de artefactos que ayudan en la descripción de funcionalidades del sistema.

\begin{itemize}
	\item[$\blacksquare$] \textbf{Historias de Usuario:}

	Están destinadas a representar mediante una pequeña descripción el comportamiento que deberá presentar el sistema desarrollado. La descripción debe proveer la información necesaria para establecer una estimación temporal necesaria para la implementación de la funcionalidad en el sistema. El lenguaje utilizado no contiene tecnicismos que no entienda el cliente, es decir, se utiliza un lenguaje común y entendible por el grupo de trabajo ya que son escritas por el cliente. Existe una historia de usuario por funcionalidad del sistema y son utilizadas para gestionar el plan de lanzamientos según la importancia que le otorgue el cliente. 

	\item[$\blacksquare$] \textbf{Task Card:}

	Están destinadas a representar las tareas que se realizan en el proyecto. Deben tener relación con una Historia de Usuario. Especifican la fecha de inicio y de finalización de tarea y al responsable de llevar a cabo dicha tarea.

	\item[$\blacksquare$] \textbf{Tarjetas CRC (Clase - Responsabilidad – Colaborador):}

	Están destinadas a representar la forma en que interactúa el grupo de trabajo con los procesos necesarios para implementar el sistema facilitando el análisis y la toma de decisiones.
Contienen la información referente a la clase (considerando que una clase es una persona, concepto, evento, documento, …), la responsabilidad de esta (las funcionalidades que presenta, los atributos, …) y los colaboradores que están implicados en su implementación.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Metodologia/hdu}
	\caption[Ejemplo de Historia de Usuario]{Ejemplo de Historia de Usuario}
	\label{fig:hdumet}
\end{figure}

\newpage

\subsection{Prácticas}
\label{Prácticas}

eXtreme Programming establece una serie de prácticas que se han de tener en cuenta durante el desarrollo del proyecto para la aplicación de dicha metodología. 

\begin{itemize}
	\item[$\blacksquare$] \textbf{Principio de pruebas:}
	
	Se deben fijar lo que debe hacer el sistema estableciendo las entradas y las salidas para establecer un periodo de pruebas de aceptación.
Planificación: El cliente del sistema definirá las funcionalidades del sistema mediante el uso de los artefactos establecidos por la metodología para ello (Historias de usuario). A partir de esas historias de usuario se establecerá el plan de entregas.

	\item[$\blacksquare$] \textbf{Cliente in-situ:}

	El cliente debe formar parte del grupo de trabajo para que pueda determinar más fácilmente los requisitos, las funcionalidades y prioridades de los diferentes procesos que se realizan. La comunicación con el grupo de desarrolladores debe ser fluida, lo que permita un desempeño del trabajo más fluido y minimizando la documentación del proyecto. 

	\item[$\blacksquare$] \textbf{Pair-programming:}

	El método para desarrollar e implementar el sistema se realiza mediante la programación en parejas, es decir, el código es escrito en la misma máquina entre dos personas. La experiencia proporcionada por este método favorece la calidad y la consistencia del producto con un coste similar o menor que el destinado si no se utilizase este método. 

	\item[$\blacksquare$] \textbf{Integración continua:}

	La inclusión de nuevos elementos al sistema es progesiva y constante, de esta forma el grupo de trabajo puede reconstruir el proyecto varias veces sin que ello produzca ningún error en el sistema.

	\item[$\blacksquare$] \textbf{Refactorización:}

	Se analiza el código para eliminar aquel sobrante o duplicado que pueda ocasionar un bajo rendimiento del sistema, mejorando de este modo la calidad del producto final. 

	\item[$\blacksquare$] \textbf{Entregas pequeñas:}

	El sistema es probado y validado por el cliente en un ámbito real para comprobar el avance del sistema y que todas las funcionalidades implementadas funcionan como debieran. 

	\item[$\blacksquare$] \textbf{Diseño simple:}

	El proyecto debe satisfacer todos los requisitos del cliente de la forma más sencilla posible, sin incluir florituras no requeridas que incrementen la complejidad del sistema y puedan ocasionar errores en el mismo.

	\item[$\blacksquare$] \textbf{Metáfora:}

	Los objetivos del proyecto y la evolución del mismo se definen mediante una historia narrada. 

	\item[$\blacksquare$] \textbf{Propiedad colectiva del código:}

	El código no es propiedad de un único miembro del equipo de trabajo, es decir, todos son propietarios de aquello que se ha desarrollado hasta el momento. 

	\item[$\blacksquare$] \textbf{Estándar de programación:}

	Se establecen unas reglas y prácticas para definir una guía que debe seguirse. Gracias a esto la comunicación y el entendimiento entre las diferentes partes que componen el grupo de trabajo se entenderán mejor y comprenderán el trabajo realizado. 
\end{itemize}

\subsection{Justificación de elección}
\label{Justificación de elección}

Tras la exposición de la metodologia eXtreme Programming se procederá a explicar por qué ha sido seleccionada para realizar una aproximación para el Trabajo Fin de Grado presente.

En la publicación \emph{`Framework for Agile Methods Classification'} \cite{iac} se encuentra una comparación y clasificación de las metodologías ágiles más utilizadas en el desarrollo de proyectos en base a un enfoque a través de cuatro puntos de vista comunes entre todas las metodologías para establecer cuál sería la óptima a utilizar teniendo en cuenta el ámbito del trabajo que se realizará. Cabe destacar que cada uno de los puntos de vista integran diferentes atributos.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Metodologia/vistas}
	\caption[Puntos de vista utilizados en la comparativa]{Puntos de vista utilizados en la comparativa \cite{iac}}
	\label{fig:vistas}
\end{figure}

Teniendo en cuenta dichos puntos de vista, sus atributos y la tabla comparativa podemos justificar la elección de la metodología viendo qué atributos son los más acordes con el tipo de proyecto a realizar. Tal como se observa, la fecha de entrega no es algo crítico, el proyecto puede evolucionar con el tiempo, predisposición a la realización de iteraciones cortas, trabajo en equipo centrado en las personas, no en los procesos, unas políticas inherentes de refactorización y de pruebas, una predisposición a los cambios, un equipo de trabajo pequeño, el riesgo y la complejidad del proyecto son bajos. 

\newpage

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\linewidth]{Metodologia/tabla_comparativa}
	\caption[Tabla comparativa de las metodologías]{Tabla comparativa de las metodologías \cite{iac}}
	\label{fig:tablacomparativa}
\end{figure}

\subsection{Aplicación de la metodología}
\label{Aplicación de la metodología}

Establecidos los conceptos de la metodología y dadas las justificaciones de su elección se procede a fijar aquellos cambios para poder adaptarla al proyecto Fin de Grado presente.
\begin{itemize}
	\item[$\blacksquare$] \textbf{Pair-Programming:}

	Teniendo en cuenta el carácter del Trabajo Fin de Grado, la programación por parejas no puede ser llevado a cabo debido a que el desarrollo del sistema recae en una única persona.

	\item[$\blacksquare$] \textbf{Roles:}

Los roles especificados en la metodología se han simplificado para estar en concordancia con el proyecto a realizar, es decir, una misma persona tendrá asignados diferentes roles y responsabilidades. 

	\item[$\blacksquare$] \textbf{Refactorizacion:}

	Debido a la restricción del personal, la refactorización del código será realizada por una única persona. 
	
	\item[$\blacksquare$] \textbf{Pruebas:}
	
	Uno de los factores de la metodología elegida es la programación basada en pruebas, pero por cuestiones de alcance se ha omitido este principio. Aun así, las pruebas se han realizado tras el desarrollo del código para validar el sistema.

	\vfill
	
	\item[$\blacksquare$] \textbf{Iteraciones:}

	En cada una de ellas se ha implementado una o varias historias de usuario, de esta forma, tras la realización de las diferentes iteraciones que se han efectuado se logra generar un sistema acorde a los requisitos del cliente.
\end{itemize}