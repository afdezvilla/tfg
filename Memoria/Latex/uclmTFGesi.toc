\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\babel@toc {spanish}{}
\babel@toc {spanish}{}
\babel@toc {english}{}
\babel@toc {spanish}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{31}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Contexto}{31}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Ornitolog\IeC {\'\i }a}{31}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}BirdWatching}{32}{subsection.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.3}Turismo ornitol\IeC {\'o}gico}{32}{subsection.1.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.4}Dispositivos m\IeC {\'o}viles}{33}{subsection.1.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Motivaci\IeC {\'o}n}{34}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Soluci\IeC {\'o}n propuesta}{35}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Estructura del documento}{36}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Objetivo y herramientas de desarrollo}{39}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Objetivo Principal}{39}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Objetivos Secundarios}{39}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Estudio del \IeC {\'a}mbito, tecnolog\IeC {\'\i }as y estado del arte}{39}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Instalaci\IeC {\'o}n y configuraci\IeC {\'o}n del servidor}{39}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Dise\IeC {\~n}ar y desarrollar la APIRest}{40}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.4}Dise\IeC {\~n}ar y desarrollar la aplicaci\IeC {\'o}n}{40}{subsection.2.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.5}Dise\IeC {\~n}ar y desarrollar la web}{40}{subsection.2.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.6}Evaluar y validar el sistema}{40}{subsection.2.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Herramientas}{41}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Medios Software}{41}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Lenguajes}{41}{section*.26}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sistemas Operativos}{43}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Software de desarrollo}{44}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Modelado y dise\IeC {\~n}o de software}{45}{section*.40}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Gesti\IeC {\'o}n remota del servidor}{45}{section*.43}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Gesti\IeC {\'o}n del proyecto}{46}{section*.47}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Control de versiones}{47}{section*.50}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Grafias}{47}{section*.52}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Documentaci\IeC {\'o}n}{48}{section*.55}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Librer\IeC {\'\i }as externas de Android}{49}{section*.59}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Utilidades}{51}{section*.64}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Medios Hardware}{51}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Antecedentes}{53}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Fundamentos te\IeC {\'o}ricos}{53}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Fundamentos tecnol\IeC {\'o}gicos}{57}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Dispositivos m\IeC {\'o}viles}{57}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Android}{58}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Posicionamiento GPS}{61}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}REST}{63}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.5}Servidor HTTP APACHE}{63}{subsection.3.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.6}MariaDB}{64}{subsection.3.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.7}PHP}{64}{subsection.3.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.8}SQLite}{64}{subsection.3.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.9}HTML/CSS/JAVASCRIPT/BOOTSTRAP}{64}{subsection.3.2.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Trabajos relacionados}{65}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}eBird by Cornell Lab}{65}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Birdtrack}{66}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}iNaturalist}{67}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4}iBird Pro Birds North America}{67}{subsection.3.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.5}CyberTracker}{67}{subsection.3.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.6}Identificador de aves}{68}{subsection.3.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.7}Aves Acu\IeC {\'a}ticas de SEO/BirdLife}{68}{subsection.3.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.8}Merlin Bird ID de Cornell Lab}{69}{subsection.3.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.9}Ornithopedia Europe}{70}{subsection.3.3.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.10}Bird Counter}{70}{subsection.3.3.10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Metodolog\IeC {\'\i }a}{71}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Metodolog\IeC {\'\i }a de desarrollo}{71}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Metodolog\IeC {\'\i }as \IeC {\'a}giles}{72}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Justificaci\IeC {\'o}n de elecci\IeC {\'o}n}{72}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}eXtreme Programming}{72}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Ciclo de vida}{73}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Valores}{74}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Principios}{75}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Variables}{76}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.5}Roles}{77}{subsection.4.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.6}Artefactos}{78}{subsection.4.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.7}Pr\IeC {\'a}cticas}{79}{subsection.4.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.8}Justificaci\IeC {\'o}n de elecci\IeC {\'o}n}{80}{subsection.4.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.9}Aplicaci\IeC {\'o}n de la metodolog\IeC {\'\i }a}{81}{subsection.4.3.9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Resultados}{83}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Iteraci\IeC {\'o}n 0}{83}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Distribuci\IeC {\'o}n del proyecto}{83}{subsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Historias de usuario}{84}{subsection.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Plan de iteraciones}{92}{subsection.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Alcance del proyecto}{92}{subsection.5.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.5}Gesti\IeC {\'o}n de procesos}{93}{subsection.5.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.6}Gesti\IeC {\'o}n de recursos}{93}{subsection.5.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.7}Gesti\IeC {\'o}n de riesgos}{93}{subsection.5.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.8}Gesti\IeC {\'o}n de costes}{97}{subsection.5.1.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.9}Prototipos del sistema}{98}{subsection.5.1.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Iteraci\IeC {\'o}n 1}{100}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Historia de Usuario 1: Configuraci\IeC {\'o}n del servidor remoto}{100}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Historia de Usuario 2: Configuraci\IeC {\'o}n del entorno de desarrollo}{100}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Historia de Usuario 3: Creaci\IeC {\'o}n de las bases de datos}{100}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Base de datos local}{100}{section*.126}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Base de datos remota}{102}{section*.129}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.4}Historia de Historia de Usuario 4: Creaci\IeC {\'o}n y configuraci\IeC {\'o}n de la APIRest}{103}{subsection.5.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Iteraci\IeC {\'o}n 2}{104}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Historia de Usuario 5: Acceso a la aplicaci\IeC {\'o}n Android}{104}{subsection.5.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{104}{section*.132}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{104}{section*.134}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{106}{section*.138}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{108}{section*.142}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}Versi\IeC {\'o}n obtenida}{108}{subsection.5.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Iteraci\IeC {\'o}n 3}{109}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.1}Historia de Usuario 6: Navegaci\IeC {\'o}n por la aplicaci\IeC {\'o}n Android}{109}{subsection.5.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{109}{section*.143}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{109}{section*.145}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{110}{section*.147}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{111}{section*.150}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.2}Versi\IeC {\'o}n obtenida}{111}{subsection.5.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Iteraci\IeC {\'o}n 4}{112}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.5.1}Historia de Usuario 7: Creaci\IeC {\'o}n de nuevas citas}{112}{subsection.5.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{112}{section*.152}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{112}{section*.154}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{116}{section*.162}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{118}{section*.166}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.5.2}Versi\IeC {\'o}n obtenida}{119}{subsection.5.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Iteraci\IeC {\'o}n 5}{120}{section.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.6.1}Historia de Usuario 8: Visualizaci\IeC {\'o}n de las citas}{120}{subsection.5.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{120}{section*.168}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{120}{section*.170}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{121}{section*.172}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{123}{section*.176}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.6.2}Versi\IeC {\'o}n obtenida}{123}{subsection.5.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.7}Iteraci\IeC {\'o}n 6}{124}{section.5.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.7.1}Historia de Usuario 9: Gesti\IeC {\'o}n de las citas}{124}{subsection.5.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{124}{section*.178}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{124}{section*.180}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{126}{section*.184}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{128}{section*.189}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.7.2}Versi\IeC {\'o}n obtenida}{128}{subsection.5.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.8}Iteraci\IeC {\'o}n 7}{129}{section.5.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.8.1}Historia de Usuario 10: Visualizaci\IeC {\'o}n de las citas del servidor remoto}{129}{subsection.5.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{129}{section*.191}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{129}{section*.193}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{131}{section*.196}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{132}{section*.199}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.8.2}Versi\IeC {\'o}n obtenida}{132}{subsection.5.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.9}Iteraci\IeC {\'o}n 8}{133}{section.5.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.9.1}Historia de Usuario 11: Visualizaci\IeC {\'o}n de los parajes}{133}{subsection.5.9.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{133}{section*.201}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{133}{section*.203}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{135}{section*.206}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{136}{section*.209}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.9.2}Versi\IeC {\'o}n obtenida}{136}{subsection.5.9.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.10}Iteraci\IeC {\'o}n 9}{137}{section.5.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.10.1}Historia de Usuario 12: Gesti\IeC {\'o}n del perfil de usuario}{137}{subsection.5.10.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{An\IeC {\'a}lisis}{137}{section*.211}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{137}{section*.213}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{138}{section*.215}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{138}{section*.217}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.10.2}Versi\IeC {\'o}n obtenida}{139}{subsection.5.10.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.11}Iteraci\IeC {\'o}n 10}{140}{section.5.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.11.1}Historia de Usuario 13: Creaci\IeC {\'o}n de la p\IeC {\'a}gina web}{140}{subsection.5.11.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Dise\IeC {\~n}o}{140}{section*.219}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implementaci\IeC {\'o}n}{144}{section*.227}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pruebas}{145}{section*.228}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.11.2}Versi\IeC {\'o}n obtenida}{145}{subsection.5.11.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.12}An\IeC {\'a}lisis de la usabilidad del proyecto}{146}{section.5.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.12.1}Justificaci\IeC {\'o}n de la aplicaci\IeC {\'o}n de la usabilidad al proyecto}{146}{subsection.5.12.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.13}Versi\IeC {\'o}n final obtenida del proyecto}{150}{section.5.13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.14}Manual de usuario y ayuda}{152}{section.5.14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.15}Documentaci\IeC {\'o}n}{153}{section.5.15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.16}Esfuerzo dedicado a la realizaci\IeC {\'o}n del proyecto}{153}{section.5.16}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusiones}{155}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}An\IeC {\'a}lisis de los objetivos conseguidos}{155}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Dificultades halladas}{156}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Propuestas de trabajos futuro}{157}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Conclusi\IeC {\'o}n}{158}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Instalaci\IeC {\'o}n y configuraci\IeC {\'o}n del servidor remoto}{159}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Instalar el SO Raspbian en la Raspberry}{159}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.1}Descargar la imagen del sistema operativo}{159}{subsection.A.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.2}Formatear la tarjeta de memoria}{159}{subsection.A.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.3}Instalaci\IeC {\'o}n del sistema operativo}{159}{subsection.A.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.4}Configuraci\IeC {\'o}n del sistema operativo}{159}{subsection.A.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}Configuraci\IeC {\'o}n del SO Raspbian en la Raspberry}{160}{section.A.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.2.1}Creaci\IeC {\'o}n de un nuevo usuario}{160}{subsection.A.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.2.2}Modificaci\IeC {\'o}n del autologin}{160}{subsection.A.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.2.3}Eliminaci\IeC {\'o}n del usuario por defecto}{160}{subsection.A.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}Instalar el control remoto en la Raspberry}{160}{section.A.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.4}Instalar los servicios web en la Raspberry}{161}{section.A.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.4.1}Instalaci\IeC {\'o}n de Apache y php}{161}{subsection.A.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.4.2}Instalaci\IeC {\'o}n y configuraci\IeC {\'o}n de MySQL y PHPMyAdmin}{161}{subsection.A.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.5}Configurando la seguridad en la Raspberry}{162}{section.A.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.5.1}Firewall}{162}{subsection.A.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.5.2}Fail2Ban}{163}{subsection.A.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.5.3}Certificados SSL y HTTPS}{163}{subsection.A.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Configuraci\IeC {\'o}n de las APIs de Google}{165}{appendix.B}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.1}Autenticando el cliente}{165}{section.B.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.2}Configuraci\IeC {\'o}n del proyecto}{165}{section.B.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.3}Utilizaci\IeC {\'o}n de las credenciales}{168}{section.B.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {C}Aspectos Legales}{169}{appendix.C}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C.1}Recursos Gr\IeC {\'a}ficos}{169}{section.C.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C.2}Medidas de seguridad}{172}{section.C.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C.3}T\IeC {\'e}rminos y condiciones de la aplicaci\IeC {\'o}n m\IeC {\'o}vil}{174}{section.C.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {D}Diagramas de secuencia}{183}{appendix.D}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {E}Mapa de navegaci\IeC {\'o}n}{191}{appendix.E}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {F}Cuestionario de usabilidad}{193}{appendix.F}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {G}Manual de usuario}{197}{appendix.G}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G.1}Acceso a la aplicaci\IeC {\'o}n}{197}{section.G.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G.2}Men\IeC {\'u} Principal}{198}{section.G.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G.3}Mis Citas}{199}{section.G.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G.4}Citas}{203}{section.G.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G.5}Parajes}{204}{section.G.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {G.6}Perfil}{206}{section.G.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {H}Contenido del CD}{207}{appendix.H}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{209}{appendix.I}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\'{I}ndice alfab\'{e}tico}{213}{appendix.J}
